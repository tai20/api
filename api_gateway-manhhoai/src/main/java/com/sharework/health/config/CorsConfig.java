package com.sharework.health.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CorsConfig {

    @Value("${allowed.origin}")
    private String allowedOrigin;

    @Value("${allowed.server}")
    private String allowedOriginServer;

    @Value("${allowed.origin.private}")
    private String allowedOriginProduct;

    @Value("${allowed.origin.public}")
    private String allowedOriginProductPublic;

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/api/**")
                        .allowedOrigins(allowedOrigin)
                        //.allowedOrigins(allowedOriginServer)
                        //.allowedOrigins(allowedOriginProduct)
                        //.allowedOrigins(allowedOriginProductPublic)
                        .allowedMethods("*")
                        .allowedHeaders("*")
                        .allowCredentials(true);
//                registry.addMapping("/api/**")
//                        //.allowedOrigins(allowedOrigin)
//                        .allowedOrigins(allowedOriginServer)
//                        //.allowedOrigins(allowedOriginProduct)
//                        //.allowedOrigins(allowedOriginProductPublic)
//                        .allowedMethods("*")
//                        .allowedHeaders("*")
//                        .allowCredentials(true);
            }
        };
    }
}
