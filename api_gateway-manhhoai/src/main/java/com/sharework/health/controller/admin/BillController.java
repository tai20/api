package com.sharework.health.controller.admin;

import com.sharework.health.dto.BillDto;
import com.sharework.health.service.BillService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/api/admin/bill")
@AllArgsConstructor
public class BillController {
    private BillService billService;

    @GetMapping("")
    public Object getAllBill(){
        List<BillDto> billDtos = billService.findAll();
        if(billDtos.isEmpty()){
            return new ResponseEntity<>("Khong tim thay", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(billDtos, HttpStatus.OK);
    }

    @GetMapping("getBillByOrder/{order_id}")
    public Object getBillByOrder(@PathVariable("order_id") Integer orderId){
        List<BillDto> billDtos = billService.findBillByOrder(orderId);
        if(billDtos == null){
            return new ResponseEntity<>("Không có lịch sử hóa đơn", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(billDtos, HttpStatus.OK);
    }
}
