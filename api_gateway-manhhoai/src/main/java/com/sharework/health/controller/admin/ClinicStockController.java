package com.sharework.health.controller.admin;

import com.sharework.health.dto.ClinicDto;
import com.sharework.health.dto.ClinicStockDto;
import com.sharework.health.dto.ProductDto;
import com.sharework.health.dto.ResponseObject;
import com.sharework.health.service.ClinicStockService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/clinicstocks")
@AllArgsConstructor
public class ClinicStockController {

    private ClinicStockService clinicStockService;

    @GetMapping("")
    public Object getAllClinicStock() {
        List<ClinicStockDto> clinicStockDtos = clinicStockService.findAll();
        if (clinicStockDtos.isEmpty()) {
            return new ResponseEntity<>("Không có dữ liệu", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(clinicStockDtos,HttpStatus.OK);
    }

    @GetMapping("/{clinicStock_id}")
    public Object getClinicStockById(@PathVariable("clinicStock_id") Integer id) {
        ClinicStockDto dto = clinicStockService.findById(id);
        if (dto == null) {
            return new ResponseEntity<>("Không tìm thấy", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }

    @PutMapping("/{clinicStock_id}")
    public ResponseEntity<ClinicStockDto> updateClinicStock(@PathVariable("clinicStock_id") Integer id , @RequestBody ClinicStockDto dto,
                                                    BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        clinicStockService.update(id, dto);
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }

    @GetMapping("clinic/{clinic_id}")
    public  ResponseEntity<ResponseObject> getClinicStockByClinicID(@PathVariable("clinic_id") Integer id){
        List<ClinicStockDto> dto = clinicStockService.getClinicStockByClinicId(id);
        if (dto.isEmpty())
        {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject(404, "Không tìm thấy", "")
            );
        } else
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Truy vấn thành công", dto)
            );
    }



}
