package com.sharework.health.controller.admin;

import com.sharework.health.dto.HistoryReturnProductAfterInsertDto;
import com.sharework.health.dto.HistoryReturnProductDetailDto;
import com.sharework.health.dto.HistoryReturnProductDto;
import com.sharework.health.service.HistoryReturnProductService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/historyreturnproducts")
@AllArgsConstructor
public class HistoryReturnProductController {

    private HistoryReturnProductService historyReturnProductService;

    @GetMapping("")
    public Object getAllHistoryReturnProduct() {
        List<HistoryReturnProductDto> historyReturnProductDtos = historyReturnProductService.findAll();
        if (historyReturnProductDtos.isEmpty()) {
            return new ResponseEntity<>("Không có lịch sử trả hàng", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(historyReturnProductDtos, HttpStatus.OK);
    }

    @GetMapping("getHistoryReturnProductDetail/{historyreturnproduct_id}")
    public Object getHistoryReturnProductDetail(@PathVariable("historyreturnproduct_id") Integer historyReturnProductId) {
        List<HistoryReturnProductDetailDto> historyReturnProductDetailDtos = historyReturnProductService.findAllHistoryReturnProductDetail(historyReturnProductId);
        if (historyReturnProductDetailDtos.isEmpty()) {
            return new ResponseEntity<>("Không có chi tiết lịch sử trả hàng", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(historyReturnProductDetailDtos, HttpStatus.OK);
    }

    @PostMapping("")
    public Object addHistoryReturnProduct(@RequestBody HistoryReturnProductDto dto) {
        if (dto == null){
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = historyReturnProductService.insert(dto);
        if (!result) {
            return new ResponseEntity<>("Không thêm được lịch sử trả hàng", HttpStatus.BAD_REQUEST);
        }
        HistoryReturnProductDto historyReturnProductDto = historyReturnProductService.findHistoryReturnProductAfterInsert();
        HistoryReturnProductAfterInsertDto historyReturnProductAfterInsertDto = new HistoryReturnProductAfterInsertDto()
                .setMessage("Thêm lịch sử phiếu khám thành công")
                .setHistoryReturnProductDto(historyReturnProductDto);
        return new ResponseEntity<>(historyReturnProductAfterInsertDto, HttpStatus.CREATED);
    }

    @PostMapping("addHistoryReturnProductDetail")
    public Object addHistoryReturnProductDetail(@RequestBody HistoryReturnProductDetailDto dto) {
        if (dto == null){
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = historyReturnProductService.addHistoryReturnProductDetail(dto);
        if (!result) {
            return new ResponseEntity<>("Không thêm được", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm thành công", HttpStatus.CREATED);
    }

    @GetMapping("getHistoryReturnProductByCustomer/{customer_id}")
    public Object getHistoryReturnProductByCustomer(@PathVariable("customer_id") Integer customerId) {
        List<HistoryReturnProductDto> historyReturnProductDtos = historyReturnProductService.findAllByCustomerId(customerId);
        if (historyReturnProductDtos.isEmpty() || historyReturnProductDtos == null) {
            return new ResponseEntity<>("Khách hàng không có lịch sử trả hàng", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(historyReturnProductDtos, HttpStatus.OK);
    }
}
