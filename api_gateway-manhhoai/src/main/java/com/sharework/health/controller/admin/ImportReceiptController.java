package com.sharework.health.controller.admin;

import com.sharework.health.dto.ImportReceiptDetailsDto;
import com.sharework.health.dto.ImportReceiptDetailsQueryDto;
import com.sharework.health.dto.ImportReceiptDto;
import com.sharework.health.dto.ResponseObject;
import com.sharework.health.repository.ImportReceiptRepository;
import com.sharework.health.service.ImportReceiptService;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/importreceipts")
@AllArgsConstructor
public class ImportReceiptController {

    private ImportReceiptService importReceiptService;
    private ImportReceiptRepository repository;

    @GetMapping("")
    public ResponseEntity<ResponseObject> getAllImportReceipt(){
        List<ImportReceiptDto> importReceiptDtos = importReceiptService.findAll();
        if(importReceiptDtos.isEmpty()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không có dữ liệu", "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200,  "Truy vấn thành công", importReceiptDtos)
        );
    }

    @GetMapping("/{importReceipt_id}")
    public ResponseEntity<ResponseObject> getImportReceiptById(@PathVariable("importReceipt_id") Integer id){
       ImportReceiptDto dto = importReceiptService.findById(id);
        if (dto!=null)
        {
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Truy vấn thành công", dto)
            );

        } else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject(404, "Không tìm thấy", "")
            );
    }

    @PostMapping("")
    public ResponseEntity<ResponseObject> addImportReceipt(@RequestBody ImportReceiptDto dto ){
        if (dto == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không có dữ liệu", "")
            );
        }
        ImportReceiptDto result = importReceiptService.insertImportReceipt(dto);
        if(result == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Thêm không thành công", "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200,  "Thêm phiếu thành công", result)
        );
    }

    @GetMapping("/detail/{receipt_id}")
    public ResponseEntity<ResponseObject> getImportReceiptDetails(@PathVariable("receipt_id") Integer id){
        List<ImportReceiptDetailsQueryDto> dto = importReceiptService.getAllImportReceiptDetailByReceiptid(id);
        if (dto.isEmpty())
        {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject(404, "Không tìm thấy", "")
            );
        } else
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Truy vấn thành công", dto)
            );

    }


    @PostMapping("/detail")
    public ResponseEntity<ResponseObject> addImportReceiptDetail(@RequestBody ImportReceiptDetailsDto dto){
        if (dto == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không có dữ liệu", "")
            );
        }
        ImportReceiptDetailsDto result = importReceiptService.insertImportReceiptDetail(dto);
        if(result == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Thêm không thành công", "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200,  "Thêm phiếu thành công", result)
        );
    }

    @DeleteMapping("{receipt_id}")
    public ResponseEntity<ResponseObject> deleteImportReceipt(@PathVariable("receipt_id") Integer id){
        boolean result = importReceiptService.deleteImportReceipt(id);
        if (result){
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Đã xóa thành công", "")
            );
        } else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không được xóa", "")
            );
        }
    }

    @DeleteMapping("/detail/{receipt_id}/{clinic_stock_id}")
    public ResponseEntity<ResponseObject> deleteImportReceiptDetail(@PathVariable("receipt_id") Integer receipt_id, @PathVariable("clinic_stock_id") Integer clinic_stock_id){
        boolean result = importReceiptService.deleteImportReceiptDetail(receipt_id,clinic_stock_id);
        if (result){
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Đã xóa thành công", "")
            );
        } else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không được xóa", "")
            );
        }
    }


    @PutMapping("/detail")
    public ResponseEntity<ResponseObject> updateImportReceiptDetail(@RequestBody ImportReceiptDetailsQueryDto dto){
        ImportReceiptDetailsQueryDto result = importReceiptService.updateImportReceiptDetail(dto);
        if(result == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Sửa không thành công", "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject(200,  "Sửa thành công", result)
        );

    }

    @PutMapping("/restore/{receipt_id}")
    public ResponseEntity<ResponseObject> restoreImportReceipt(@PathVariable("receipt_id") Integer id){
        boolean result = importReceiptService.restoreImportReceipt(id);
        if (result){
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Đã khôi phục thành công", "")
            );
        } else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Không khôi phục được. Hoặc đã có sẵn", "")
            );
        }
    }

    @PutMapping("/lock/{receipt_id}")
    public ResponseEntity<ResponseObject> lockImportReceipt(@PathVariable("receipt_id") Integer id){
        boolean result = importReceiptService.lockImportReceipt(id);
        if (result){
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject(200,  "Đã khóa phiếu", "")
            );
        } else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ResponseObject(400, "Phiếu đã được khóa hoặc đã xóa", "")
            );
        }
    }

    }


