package com.sharework.health.controller.admin;

import com.sharework.health.dto.*;
import com.sharework.health.service.IndicationCardService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/admin/indicationcards")
@AllArgsConstructor
public class IndicationCardController {

    private IndicationCardService indicationCardService;

    @GetMapping("")
    public Object getAllIndicationCard() {
        List<IndicationCardDto> dtos = indicationCardService.findAll();
        if (dtos.isEmpty() || dtos == null) {
            return new ResponseEntity<>("Không có chỉ định", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("getIndicationCardDetailByIndicationCard/{indicationCardId}")
    public Object getIndicationCardDetailByIndicationCard(@PathVariable("indicationCardId") Integer indicationCardId) {
        List<IndicationCardDetailDto> dtos = indicationCardService.getIndicationCardDetailByIndicationCard(indicationCardId);
        if (dtos.isEmpty() || dtos == null) {
            return new ResponseEntity<>("Không có chi tiết chỉ định", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("getIndicationCardLatelyByCustomer")
    public Object getIndicationCardLatelyByCustomer(@RequestParam("customerId") Integer customerId) {
        IndicationCardDto dto = indicationCardService.getIndicationCardLatelyByCustomer(customerId);
        if (dto == null) {
            return new ResponseEntity<>("Không có phiếu chỉ định", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping("")
    public Object addIndicationCard(@RequestBody IndicationCardDto dto, @RequestParam("medicalId") Integer medicalCardId,
                                    BindingResult error) throws IOException {
        if (dto == null) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = indicationCardService.insert(dto);
        if (!result) {
            return new ResponseEntity<>("Thêm phiếu chỉ định thất bại", HttpStatus.BAD_REQUEST);
        }
        boolean resultUpdateIndicationInMedicard = indicationCardService.updateIndicationCardInMedicalCard(medicalCardId, indicationCardService.getIndicationCardAfterInsert());
        IndicationCardAfterInsertDto dto1 = new IndicationCardAfterInsertDto()
                .setMessage("Thêm phiếu chỉ định thành công")
                .setIndicationCardDto(indicationCardService.getIndicationCardAfterInsert());
        return new ResponseEntity<>(dto1, HttpStatus.CREATED);
    }

    @PutMapping("{indicationcard_id}")
    public Object updateIndicationCard(@PathVariable("indicationcard_id") Integer indicationcardId, @RequestBody IndicationCardDto dto,
                                       BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = indicationCardService.update(indicationcardId, dto);
        if (!result) {
            return new ResponseEntity<>("Cập nhật thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhật thành công", HttpStatus.OK);
    }

//    @PostMapping("addIndicationCardDetail")
//    public Object addIndicationCardDetail(@RequestBody IndicationCardDetailDto dto) {
//        if (dto == null) {
//            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
//        }
//        boolean result = indicationCardService.addIndicationCardDetail(dto);
//        if (!result) {
//            return new ResponseEntity<>("Thêm chi tiết phiếu chỉ định thất bại", HttpStatus.BAD_REQUEST);
//        }
//
//        return new ResponseEntity<>("Thêm chi tiết phiếu chỉ định thành công", HttpStatus.CREATED);
//    }

    ////////////////

    @PostMapping("addIndicationCardDetail")
    public Object addIndicationCardDetail(@RequestBody IndicationCardDetailDto dto, @RequestParam("medical_id") Integer medicalCardId) {
        if (dto == null) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = indicationCardService.addIndicationCardDetail(dto, medicalCardId);
        if (!result) {
            return new ResponseEntity<>("Thêm chi tiết phiếu chỉ định thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm chi tiết phiếu chỉ định thành công", HttpStatus.CREATED);
    }
    ////////////
    @DeleteMapping("deleteIndicationCardDetail")
    public Object deleteIndicationCardDetail(@RequestBody IndicationCardDetailDto dto) {
        if (dto == null) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = indicationCardService.deleteIndicationCardDetail(dto);
        if (!result) {
            return new ResponseEntity<>("Xóa chi tiết chỉ định thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Xóa chi tiết chỉ định thành công", HttpStatus.CREATED);
    }



    @PutMapping("updateIndicationCardDetailABoutUtralsoud")
    public Object updateIndicationCardDetailABoutUtralsoud(@RequestParam(name = "indicationCardId") String indicationCardId,@RequestParam(name = "categoryIndicationCardId") String categoryIndicationCardId,@RequestParam(name = "liverandkidney", required = false) String liverandkidney,@RequestParam(name = "bilebuctsAndgallbladderandbladder", required = false) String bilebuctsAndgallbladderandbladder,@RequestParam(name = "abdominalfluid", required = false) String abdominalfluid,
                                                           @RequestParam(name = "uterus", required = false) String uterus,@RequestParam(name = "dkts", required = false) String dkts,@RequestParam(name = "density", required = false) String density,@RequestParam(name = "ovary", required = false) String ovary,@RequestParam(name = "endometrium", required = false) String endometrium,@RequestParam(name = "cervical", required = false) String cervical,
                                                           @RequestParam(name = "fornixfluid", required = false) String fornixfluid,@RequestParam(name = "smallfetusultrasoundinformation1", required = false) String smallfetusultrasoundinformation1,@RequestParam(name = "smallfetusultrasoundinformation2", required = false) String smallfetusultrasoundinformation2,@RequestParam(name = "blackandwhitefetalultrasoundinformation", required = false) String blackandwhitefetalultrasoundinformation,
                                                           @RequestParam(name = "colordoppleroffetalbloodvesselsandnuchaltranslucency", required = false) String colordoppleroffetalbloodvesselsandnuchaltranslucency,@RequestParam(name = "fetalAnatomy", required = false) String fetalAnatomy,@RequestParam(name = "pidmuterus", required = false) String pidmuterus,@RequestParam(name = "unbt", required = false) String unbt,
                                                           @RequestParam(name = "interstitial", required = false) String interstitial,@RequestParam(name = "tntc", required = false) String tntc,@RequestParam(name = "colordopplerofFetalbloodvessels", required = false) String colordopplerofFetalbloodvessels,@RequestParam(name = "pregnancyultrasound4d", required = false) String pregnancyultrasound4d,
                                                           @RequestParam(name = "fetalanatomyheadfaceneck", required = false) String fetalanatomyheadfaceneck,@RequestParam(name = "fetalanatomychest", required = false) String fetalanatomychest,@RequestParam(name = "fetalanatomystomach", required = false) String fetalanatomystomach,@RequestParam(name = "fetalanatomythefourlimbs", required = false) String fetalanatomythefourlimbs,
                                                           @RequestParam(name = "mlt", required = false) String mlt,@RequestParam(name = "bigblackandwhitefetalultrasoundinformation", required = false) String bigblackandwhitefetalultrasoundinformation,@RequestParam(name = "conclude", required = false) String conclude,@RequestParam(name = "ctc_dk", required = false) String ctc_dk,
                                                           @RequestParam(name = "bldb", required = false) String bldb,@RequestParam(name = "blda", required = false) String blda,@RequestParam(name = "thewomb", required = false) String thewomb, @RequestParam("listImages") MultipartFile[] listImages) throws IOException{
        if (listImages==null || listImages.length<= 0) {
            return new ResponseEntity<>("File ảnh siêu âm không có. Vui lòng chọn ảnh", HttpStatus.BAD_REQUEST);
        }
        boolean result = indicationCardService.updateIndicationCardDetailABoutUtralsoud(Integer.parseInt(indicationCardId) ,Integer.parseInt(categoryIndicationCardId) ,liverandkidney,bilebuctsAndgallbladderandbladder,abdominalfluid,
                uterus,dkts,density,ovary,endometrium,cervical,
                fornixfluid,smallfetusultrasoundinformation1,smallfetusultrasoundinformation2,blackandwhitefetalultrasoundinformation,
                colordoppleroffetalbloodvesselsandnuchaltranslucency,  pidmuterus, unbt,
                interstitial,tntc, colordopplerofFetalbloodvessels, pregnancyultrasound4d,
                fetalanatomyheadfaceneck, fetalanatomychest, fetalanatomystomach,fetalanatomythefourlimbs,
                mlt, bigblackandwhitefetalultrasoundinformation, conclude, ctc_dk,
                bldb, blda,thewomb, listImages);
        if (!result) {
            return new ResponseEntity<>("Cập nhật thất bại", HttpStatus.BAD_REQUEST);
        }
        IndicationCardDetailDto indicationCardDetailDto = indicationCardService.getIndicationCardDetailByAfterUpdate(Integer.parseInt(indicationCardId) ,Integer.parseInt(categoryIndicationCardId) );
        return new ResponseEntity<>(indicationCardDetailDto, HttpStatus.OK);
    }

    @GetMapping("getIndicationCardByCustomer")
    public Object getIndicationCardByCustomer(@RequestParam("customerId") String customerId) {
        List<IndicationCardDto> dtos = indicationCardService.getIndicationCardByCustomer(Integer.parseInt(customerId));
        if (dtos.isEmpty() || dtos == null) {
            return new ResponseEntity<>("Không có chỉ định", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("getIndicationCardByUser")
    public Object getIndicationCardByUser(@RequestParam("userId") String userId) {
        List<IndicationCardDto> dtos = indicationCardService.getIndicationCardByUser(Integer.parseInt(userId));
        if (dtos.isEmpty() || dtos == null) {
            return new ResponseEntity<>("Không có chỉ định", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PutMapping("importFileIndicationCard")
    public Object importFileIndicationCard(@RequestParam("indicationCardId") String indicationCardId,
                                           @RequestParam("categoryIndicationId") String categoryIndicationId,
                                           @RequestParam("file") MultipartFile file) throws IOException {
        if (!file.isEmpty() || file != null) {

            boolean result = indicationCardService.importFileIndicationCard(Integer.parseInt(indicationCardId),
                    Integer.parseInt(categoryIndicationId),
                    file);
            if (!result) {
                return new ResponseEntity<>("Import phiếu khám chỉ định thành công", HttpStatus.OK);
            }
            return new ResponseEntity<>("Import phiếu khám chỉ định không thành công", HttpStatus.OK);
        }
        return new ResponseEntity<>("Lỗi không có file phiếu khám chỉ định", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("getIndicationCardByMedicalCardId/{medicalCardId}")
    public Object getIndicationCardByMedicalCardId(@PathVariable("medicalCardId") Integer medicalCardId) {
        IndicationCardDto dto = indicationCardService.getIndicationCardByMedicalCardId(medicalCardId);
        if (dto == null) {
            return new ResponseEntity<>("Không có phiếu chỉ định", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping("getIndicationCardDetailByMedicalCardId/{medicalCardId}")
    public Object getIndicationCardDetailByMedicalCardId(@PathVariable("medicalCardId") Integer medicalCardId) {
        List<IndicationCardDetailDto> dtos = indicationCardService.getIndicationCardDetailByMedicalCardId(medicalCardId);
        if (dtos.isEmpty() || dtos == null) {
            return new ResponseEntity<>("Không có chi tiết chỉ định", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PutMapping("updateStatusIndicationCardDetail")
    public Object updateMedicalCardByIndicationCardId(@RequestParam("medical_id") Integer medicalCardId, @RequestParam("indicationcard_id") Integer indicationCardId
            ,IndicationCardDetailDto dto, BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = indicationCardService.updateStatusIndicationCardDetail(medicalCardId, indicationCardId,dto);
        if (!result) {
            return new ResponseEntity<>("Cập nhật thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhật thành công", HttpStatus.CREATED);
    }
}
