package com.sharework.health.controller.admin;


import com.sharework.health.dto.ClinicDto;
import com.sharework.health.dto.LevelDto;
import com.sharework.health.service.LevelService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/levels")
@AllArgsConstructor
public class LevelController {
    private LevelService levelService;

    @GetMapping("")
    public Object getAllLevel() {
        List<LevelDto> levelDtos = levelService.findAll();
        if (levelDtos.isEmpty()) {
            return new ResponseEntity<>("Không có dữ liệu", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(levelDtos,HttpStatus.OK);
    }

    @PostMapping("")
    public Object addLevel(@RequestBody LevelDto dto, BindingResult error){
        if (error.hasErrors()) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }

        boolean result = levelService.insert(dto);
        if (!result){
            return new ResponseEntity<>("Thêm hạng khách hàng thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm hạng khách hàng thành công",HttpStatus.CREATED);
    }

    @PutMapping("/{level_id}")
    public Object updateLevel(@PathVariable("level_id") Integer id , @RequestBody LevelDto dto,BindingResult error){

        if (error.hasErrors()) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = levelService.update(id, dto);
        if (!result){
            return new ResponseEntity<>("Cập nhật thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhật thành công", HttpStatus.OK);
    }

}
