package com.sharework.health.controller.admin;

import com.sharework.health.dto.CategoryIndicationCardDto;
import com.sharework.health.dto.MedicalCardAfterInsertDto;
import com.sharework.health.dto.MedicalCardDto;
import com.sharework.health.dto.MedicalCardQueryDto;
import com.sharework.health.service.MedicalCardService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/medicalcards")
@AllArgsConstructor
public class MedicalCardController {

    private MedicalCardService medicalCardService;

    @GetMapping("")
    public Object getAllMedicalCard() {
        List<MedicalCardDto> dtos = medicalCardService.findAll();
        if (dtos.isEmpty() || dtos == null) {
            return new ResponseEntity<>("Không có phiếu khám nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
        /*List<MedicalCardQueryDto> dtos = medicalCardService.getAllMedicalCard();
        if (dtos.isEmpty() || dtos == null){
            return new ResponseEntity<>("Không có phiếu khám nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);*/
    }

    @GetMapping("getMedicalCardByScheduleExaminationId/{scheduleexamination_id}")
    public Object getMedicalCardByScheduleExaminationId(@PathVariable("scheduleexamination_id") Integer scheduleexamination_id) {
        MedicalCardDto dto = medicalCardService.getMedicalCardByScheduleExaminationId(scheduleexamination_id);
        if (dto == null) {
            return new ResponseEntity<>("Không có phiếu khám nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping("getMedicalCardByCustomerId/{customer_id}")
    public Object getMedicalCardByCustomerId(@PathVariable("customer_id") Integer customer_id) {
        List<MedicalCardDto> dto = medicalCardService.getMedicalCardByCustomerId(customer_id);
        if (dto == null || dto.isEmpty()) {
            return new ResponseEntity<>("Không có phiếu khám nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping("getMedicalCardLatelyByCustomerId/{customer_id}")
    public Object getMedicalCardLatelyByCustomerId(@PathVariable("customer_id") Integer customer_id) {
        MedicalCardDto dto = medicalCardService.getMedicalCardLatelyByCustomerId(customer_id);
        if (dto == null) {
            return new ResponseEntity<>("Không có phiếu khám nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping("{medical_id}")
    public Object getMedicalCardById(@PathVariable("medical_id") Integer medicalId) {
        MedicalCardDto dto = medicalCardService.findById(medicalId);
        if (dto == null) {
            return new ResponseEntity<>("Không có phiếu khám nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping("")
    public Object addMedicalCard(@RequestBody MedicalCardDto dto, @RequestParam("scheduleId") Integer id ) {
        if (dto == null) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = medicalCardService.insert(dto);
        if (!result) {
            return new ResponseEntity<>("Thêm phiếu khám thất bại", HttpStatus.BAD_REQUEST);
        }

        MedicalCardDto medicalCardDto = medicalCardService.getMedicalCardAfterInsert();
        boolean resultUpdateMedicalCardInScheduleExamination = medicalCardService.updateMedicalCardInScheduleExamination(id, medicalCardDto );
        if (!resultUpdateMedicalCardInScheduleExamination){
            return new ResponseEntity<>("Thêm phiếu khám thất bại", HttpStatus.BAD_REQUEST);
        }

        MedicalCardAfterInsertDto dto1 = new MedicalCardAfterInsertDto()
                .setMessage("Thêm phiếu khám thành công")
                .setMedicalCardDto(medicalCardService.getMedicalCardAfterInsert());
        return new ResponseEntity<>(dto1, HttpStatus.OK);
    }

    @PutMapping("/{medical_id}")
    public Object updateMedicalCard(@PathVariable("medical_id") Integer id, @RequestBody MedicalCardDto dto,
                                    BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = medicalCardService.update(id, dto);
        if (!result) {
            return new ResponseEntity<>("Cập nhật thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhật thành công", HttpStatus.CREATED);
    }

//    @PutMapping("updateMedicalCardByIndicationCardId/{medical_id}")
//    public Object updateMedicalCardByIndicationCardId(@PathVariable("medical_id") Integer medicalCardId, @RequestBody MedicalCardDto dto
//           ,@RequestParam("indicationcard_id") Integer indicationCardId , BindingResult error) {
//        if (error.hasErrors()) {
//            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
//        }
//        boolean result = medicalCardService.updateMedicalCardByIndicationCardId(medicalCardId, dto, indicationCardId);
//        if (!result) {
//            return new ResponseEntity<>("Cập nhật thất bại", HttpStatus.BAD_REQUEST);
//        }
//        return new ResponseEntity<>("Cập nhật thành công", HttpStatus.CREATED);
//    }
}
