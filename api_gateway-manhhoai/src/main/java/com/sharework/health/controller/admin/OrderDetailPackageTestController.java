package com.sharework.health.controller.admin;

import com.sharework.health.dto.OrderDetailPackageTestDto;
import com.sharework.health.service.OrderDetailPackageTestService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/orderdetailpackagetest")
@AllArgsConstructor
public class OrderDetailPackageTestController {

    private OrderDetailPackageTestService orderDetailPackageTestService;

    @GetMapping("")
    public Object getAllOrderDetailPackageTest() {
        List<OrderDetailPackageTestDto> orderDetailPackageTestDtos = orderDetailPackageTestService.findAll();
        if (orderDetailPackageTestDtos.isEmpty() || orderDetailPackageTestDtos == null) {
            return new ResponseEntity<>("Không có dữ liệu", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(orderDetailPackageTestDtos, HttpStatus.OK);
    }

    @GetMapping("{order_Id}")
    public Object getAllOrderDetailPackageTestByOrders(@PathVariable("order_Id") Integer customerId) {
        List<OrderDetailPackageTestDto> orderDetailPackageTestDtos = orderDetailPackageTestService.getOrderDetailPackageTestByOrderId(customerId);
        if (orderDetailPackageTestDtos.isEmpty() || orderDetailPackageTestDtos == null) {
            return new ResponseEntity<>("Khách hàng này không có đặt hàng nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(orderDetailPackageTestDtos, HttpStatus.OK);
    }

    @PostMapping("")
    public Object addOrderDetailPackageTest(@RequestBody OrderDetailPackageTestDto dto) {
        if (dto == null) {
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = orderDetailPackageTestService.insert(dto);
        if (!result) {
            return new ResponseEntity<>("Thêm thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm thành công", HttpStatus.CREATED);
    }

}
