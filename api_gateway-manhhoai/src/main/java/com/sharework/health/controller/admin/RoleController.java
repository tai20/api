package com.sharework.health.controller.admin;

import com.sharework.health.dto.CustomerDto;
import com.sharework.health.dto.RoleDto;
import com.sharework.health.service.RoleService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/roles")
@AllArgsConstructor
public class RoleController {

    private RoleService roleService;

    @GetMapping("")
    public ResponseEntity<List<RoleDto>> getAllRole() {
        List<RoleDto> roleDtos = roleService.findAll();
        if (roleDtos.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(roleDtos,HttpStatus.OK);
    }

    @GetMapping("/{role_id}")
    public ResponseEntity<RoleDto> getRoleById(@PathVariable("role_id") Integer id) {
        System.out.println(id);
        RoleDto roleDto = roleService.findById(id);
        if (roleDto == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(roleDto,HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<RoleDto> addRole(@RequestBody RoleDto dto,
                                                   BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        roleService.insert(dto);
        return new ResponseEntity<>(dto,HttpStatus.CREATED);
    }

    @PutMapping("/{role_id}")
    public ResponseEntity<RoleDto> updateRole(@PathVariable("role_id") Integer id , @RequestBody RoleDto dto,
                                                      BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        roleService.update(id, dto);
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }

    @DeleteMapping("/{role_id}")
    public ResponseEntity<CustomerDto> deleteRole(@PathVariable("role_id") Integer id) {
        roleService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
