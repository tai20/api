package com.sharework.health.controller.admin;

import com.sharework.health.dto.ServiceCategoryDto;
import com.sharework.health.service.ServiceCategoryService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/servicecategories")
@AllArgsConstructor
public class ServiceCategoryController {

    private ServiceCategoryService serviceCategoryService;

    @GetMapping("")
    public Object getAllServiceCategory(){
        List<ServiceCategoryDto> serviceCategories = serviceCategoryService.findAll();
        if (serviceCategories.isEmpty() || serviceCategories == null){
            return new ResponseEntity<>("Không có dữ liệu", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(serviceCategories, HttpStatus.OK);
    }

    @PostMapping("")
    public Object addServiceCategory(@RequestBody ServiceCategoryDto dto){
        if (dto == null){
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = serviceCategoryService.insert(dto);
        if (!result){
            return new ResponseEntity<>("Thêm loại dịch vụ thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Thêm loại dịch vụ thành công", HttpStatus.CREATED);
    }

    @PutMapping("{service_category_id}")
    public Object updateServiceCategory(@PathVariable("service_category_id") Integer serviceCategoryId, @RequestBody ServiceCategoryDto dto){
        if (dto == null){
            return new ResponseEntity<>("Thiếu thông tin", HttpStatus.BAD_REQUEST);
        }
        boolean result = serviceCategoryService.update(serviceCategoryId, dto);
        if (!result){
            return new ResponseEntity<>("Cập nhật loại dịch vụ thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhật loại dịch vụ thành công", HttpStatus.CREATED);
    }

    @DeleteMapping("{service_category_id}")
    public Object deleteServiceCategory(@PathVariable("service_category_id") Integer serviceCategoryId){
        boolean result = serviceCategoryService.delete(serviceCategoryId);
        if (!result){
            return new ResponseEntity<>("Xóa loại dịch vụ thất bại", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Xóa loại dịch vụ thành công", HttpStatus.CREATED);
    }
}
