package com.sharework.health.controller.admin;

import com.sharework.health.dto.ServiceListDto;
import com.sharework.health.service.ServiceListService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/api/admin/servicelist")
@AllArgsConstructor
public class ServiceListController {
    private ServiceListService serviceListService;

    @GetMapping("getServiceListByCustomer/{customer_id}")
    public Object getServiceListByCustomer(@PathVariable("customer_id") Integer customerId){
        List<ServiceListDto> serviceListDtos = serviceListService.findAllServiceListByCustomer(customerId);
        if(serviceListDtos.isEmpty()){
            return new ResponseEntity<>("Không có danh sách dịch vụ", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(serviceListDtos, HttpStatus.OK);
    }
}
