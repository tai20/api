package com.sharework.health.controller.admin;

import com.sharework.health.dto.SlipReturnProductDetailDto;
import com.sharework.health.dto.SlipReturnProductDto;
import com.sharework.health.service.SlipReturnProductService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/admin/slipreturnproducts")
@AllArgsConstructor
public class SlipReturnProductController {

    private SlipReturnProductService slipReturnProductService;

    @GetMapping("")
    public Object getAllSlipReturnProduct() {
        List<SlipReturnProductDto> slipReturnProductDtos = slipReturnProductService.findAll();
        if (slipReturnProductDtos.isEmpty()) {
            return new ResponseEntity<>("Không tìm thấy", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(slipReturnProductDtos, HttpStatus.OK);
    }

    @GetMapping("{slipreturnproduct_id}")
    public Object getSlipReturnProductById(@PathVariable("slipreturnproduct_id") Integer slipUseReturnProductId) {
        SlipReturnProductDto slipReturnProductDto = slipReturnProductService.findById(slipUseReturnProductId);
        if (slipReturnProductDto == null) {
            return new ResponseEntity<>("Không tìm thấy", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(slipReturnProductDto, HttpStatus.OK);
    }

    @GetMapping("getSlipReturnProductDetail/{slipreturnproduct_id}")
    public Object getSlipReturnProductDetail(@PathVariable("slipreturnproduct_id") Integer slipReturnProductId) {
        List<SlipReturnProductDetailDto> slipReturnProductDetailDtos = slipReturnProductService.findAllSlipReturnProductDetailBySlipReturnProduct(slipReturnProductId);
        if (slipReturnProductDetailDtos.isEmpty()) {
            return new ResponseEntity<>("Không có chi tiết phiếu trả", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(slipReturnProductDetailDtos, HttpStatus.OK);
    }

    @GetMapping("getSlipReturnProductByCustomer/{customer_id}")
    public Object getSlipReturnProductByCustomer(@PathVariable("customer_id") Integer customerId) {
        List<SlipReturnProductDto> slipReturnProductDtos = slipReturnProductService.findAllSlipReturnProductByCustomer(customerId);
        if (slipReturnProductDtos.isEmpty()) {
            return new ResponseEntity<>("Khách hàng này không có phiếu trả nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(slipReturnProductDtos, HttpStatus.OK);
    }
}
