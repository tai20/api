package com.sharework.health.controller.admin;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sharework.health.dto.*;
import com.sharework.health.service.StatisticalService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/admin/statisticals")
@AllArgsConstructor
public class StatisticalController {

    private StatisticalService statisticalService;

    @PostMapping("statisticalCustomerByCreatedDate")
    public Object statisticalCustomerByCreatedDate(@RequestParam("startDate") String startDate,
                                                   @RequestParam("endDate") String endDate){

        List<CustomerDto> customerDtos = statisticalService.statisticalCustomerByCreatedDate(LocalDateTime.parse(startDate),
                LocalDateTime.parse(endDate));
        if (customerDtos.isEmpty() || customerDtos == null){
            return new ResponseEntity<>("Không tìm thấy", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(customerDtos, HttpStatus.NOT_FOUND);
    }

    @PostMapping("statisticalAdvisoryByCreatedDate")
    public Object statisticalAdvisoryByCreatedDate(@RequestParam("startDate") String startDate,
                                                   @RequestParam("endDate") String endDate){

        List<AdvisoryDto> advisoryDtos = statisticalService.statisticalAdvisoryByCreatedDate(LocalDateTime.parse(startDate),
                LocalDateTime.parse(endDate));
        if (advisoryDtos.isEmpty() || advisoryDtos == null){
            return new ResponseEntity<>("Không tìm thấy", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(advisoryDtos, HttpStatus.OK);
    }

    @PostMapping("statisticalExaminationCardByDateOfExamiantion")
    public Object statisticalExaminationCardByCreatedDate(@RequestParam("startDate") String startDate,
                                                   @RequestParam("endDate") String endDate){

        List<ExaminationCardDto> examinationCardDtos = statisticalService.statisticalExaminationCardByDateOfExamination(LocalDateTime.parse(startDate),
                LocalDateTime.parse(endDate));
        if (examinationCardDtos.isEmpty() || examinationCardDtos == null){
            return new ResponseEntity<>("Không tìm thấy", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(examinationCardDtos, HttpStatus.OK);
    }

    @PostMapping("statisticalOrderByOrderDate")
    public Object statisticalOrderByOrderDate(@RequestParam("startDate") String startDate,
                                                          @RequestParam("endDate") String endDate){

        List<OrderDto> orderDtos = statisticalService.statisticalOrderByOrderDate(LocalDateTime.parse(startDate), LocalDateTime.parse(endDate));
        if (orderDtos.isEmpty() || orderDtos == null){
            return new ResponseEntity<>("Không tìm thấy", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(orderDtos, HttpStatus.OK);
    }

    @PostMapping("statisticalCustomerHaveSumDebt")
    public Object statisticalCustomerHaveSumDebt(@RequestParam("month") String month, @RequestParam("year") String year){

        List<SelectCustomerIdDebtMappingDto> dtos = statisticalService.statisticalCustomerHaveSumDebtByMonth(Integer.parseInt(month), Integer.parseInt(year));
        if (dtos.isEmpty() || dtos == null){
            return new ResponseEntity<>("Không có khách hàng nào nợ trong tháng này", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping("statisticalCustomerHaveSumTotal")
    public Object statisticalCustomerHaveSumTotal(@RequestParam("month") String month, @RequestParam("year") String year){

        List<SelectCustomerIdTotalMappingDto> dtos = statisticalService.statisticalCustomerHaveSumTotalByMonth(Integer.parseInt(month), Integer.parseInt(year));
        if (dtos.isEmpty() || dtos == null){
            return new ResponseEntity<>("Không có khách hàng nào mua hàng trong tháng này", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping("statisticalUserWithRoleDoctorAndCountExamination")
    public Object statisticalUserWithRoleDoctorAndCountExamination(@RequestParam("month") String month, @RequestParam("year") String year){

        List<SelectCountExaminationQueryMappingDto> dtos = statisticalService.statisticalUserWithRoleDoctorAndCountExaminationByMonthAndYear(Integer.parseInt(month), Integer.parseInt(year));
        if (dtos.isEmpty() || dtos == null){
            return new ResponseEntity<>("Không có dữ liệu", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping("statisticalUserWithRoleTechnicianAndCountExamination")
    public Object statisticalUserWithRoleTechnicianAndCountExamination(@RequestParam("month") String month, @RequestParam("year") String year){

        List<SelectCountExaminationQueryMappingDto> dtos = statisticalService.statisticalUserWithRoleTechnicianAndCountExaminationByMonthAndYear(Integer.parseInt(month), Integer.parseInt(year));
        if (dtos.isEmpty() || dtos == null){
            return new ResponseEntity<>("Không có dữ liệu", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping("statisticalCustomerAndCountExamination")
    public Object statisticalCustomerAndCountExamination(@RequestParam("month") String month, @RequestParam("year") String year){

        List<SelectCountExaminationQueryMappingDto> dtos = statisticalService.statisticalCustomerAndCountExaminationByMonthAndYear(Integer.parseInt(month), Integer.parseInt(year));
        if (dtos.isEmpty() || dtos == null){
            return new ResponseEntity<>("Không tìm thấy", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping("statisticalCustomerHaveBirthDateByMonth")
    public Object statisticalCustomerHaveBirthDateByMonth(@RequestParam("month") String month){

        List<CustomerDto> dtos = statisticalService.statisticalCustomerHaveBirthDateByMonth(Integer.parseInt(month));
        if (dtos.isEmpty() || dtos == null){
            return new ResponseEntity<>("Không có khách hàng nào có sinh nhật trong tháng", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping("statisticalCountProductSoldByMonth")
    public Object statisticalCountProductSoldByMonth(@RequestParam("month") String month, @RequestParam("year") String year) {

        List<CountProductSoldByMonthDto> dtos = statisticalService.statisticalCountProductSoldByMonth(Integer.parseInt(month),Integer.parseInt(year));
        if (dtos.isEmpty() || dtos == null){
            return new ResponseEntity<>("Không có sản phẩm nào được bán trong tháng này", HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping("statisticalCountExaminationOfDoctorByMonthAndYear")
    public Object statisticalCountExaminationOfDoctorByMonthAndYear(@RequestParam("month") String month, @RequestParam("year") String year) {
        List<CountExaminationOfDoctorByMonthAndYearDto> dtos = statisticalService.statisticalCountExaminationOfDoctorByMonthAndYear(Integer.parseInt(month),Integer.parseInt(year));
        try {
            if (dtos.isEmpty()){
                return new ResponseEntity<>("Chưa có bác sĩ nào khám trong tháng này", HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(dtos, HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>("Lỗi", HttpStatus.BAD_GATEWAY);

    }

    @PostMapping("statisticalCountExaminationOfTechnicianByMonthAndYear")
        public Object statisticalCountExaminationOfTechnicianByMonthAndYear(@RequestParam("month") String month, @RequestParam("year") String year) throws JsonProcessingException {

        List<CountExaminationOfTechnicianByMonthAndYearDto> dtos = statisticalService.statisticalCountExaminationOfTechnicianByMonthAndYear(Integer.parseInt(month),Integer.parseInt(year));
        try {
            if (dtos.isEmpty() || dtos == null){
                return new ResponseEntity<>("Chưa có kĩ thuật viên nào khám trong tháng này", HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(dtos, HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>("Lỗi", HttpStatus.BAD_GATEWAY);
    }

    @PostMapping("statisticalCountExaminationOfServiceByMonthAndYearAndClinic")
    public Object statisticalCountExaminationOfServiceByMonthAndYearAndClinic(@RequestParam("month") String month, @RequestParam("year") String year, @RequestParam("clinic_id") String clinicId) {

        List<CountExaminationOfServiceByClinicDto> dtos = statisticalService.statisticalCountExaminationOfServiceByMonthAndYearAndClinic(Integer.parseInt(month),Integer.parseInt(year), Integer.parseInt(clinicId));
        try {
            if (dtos.isEmpty() || dtos == null){
                return new ResponseEntity<>("Chưa có dịch vụ nào được khám trong tháng này", HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(dtos, HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>("Lỗi", HttpStatus.BAD_GATEWAY);
    }

    @PostMapping("statisticalCountConsumableProductByMonthAndYear")
    public Object statisticalCountConsumableProductByMonthAndYear(@RequestParam("month") String month, @RequestParam("year") String year) throws JsonProcessingException {

        List<CountConsumableProductByMonthAndYearDto> dtos = statisticalService.statisticalCountConsumableProductByMonthAndYear(Integer.parseInt(month),Integer.parseInt(year));
        try {
            if (dtos.isEmpty() || dtos == null){
                return new ResponseEntity<>("Chưa có sản phẩm tiêu hao nào được khám trong tháng này", HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(dtos, HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>("Lỗi", HttpStatus.BAD_GATEWAY);
    }

    @PostMapping("statisticalCountCustomerOfServiceByMonthAndYear")
    public Object statisticalCountCustomerOfServiceByMonthAndYear(@RequestParam("month") String month, @RequestParam("year") String year) {

        List<CountCustomerOfServiceByMonthAndYearDto> dtos = statisticalService.statisticalCountCustomerOfServiceByMonthAndYear(Integer.parseInt(month),Integer.parseInt(year));
        try {
            if (dtos.isEmpty() || dtos == null){
                return new ResponseEntity<>("Chưa có dịch vụ nào được khám trong tháng này", HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(dtos, HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>("Lỗi", HttpStatus.BAD_GATEWAY);
    }

    @PostMapping("statisticalCountServiceExaminationOfCustomerByMonthAndYear")
    public Object statisticalCountServiceExaminationOfCustomerByMonthAndYear(@RequestParam("month") String month, @RequestParam("year") String year) {

        List<CountServiceExaminationOfCustomerByMonthAndYearDto> dtos = statisticalService.statisticalCountServiceExaminationOfCustomerByMonthAndYear(Integer.parseInt(month),Integer.parseInt(year));
        try {
            if (dtos.isEmpty() || dtos == null){
                return new ResponseEntity<>("Chưa có khách hàng nào được khám trong tháng này", HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(dtos, HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>("Lỗi", HttpStatus.BAD_GATEWAY);
    }

    @GetMapping("findAllProductImportFromTo")
    public Object findAllProductImportFromTo(@RequestParam("from") String from, @RequestParam("to") String to) {

        List<ProductDto> dtos = statisticalService.findAllProductImportFromTo(LocalDate.parse(from),LocalDate.parse(to));
        if (dtos.isEmpty() || dtos == null){
            return new ResponseEntity<>("Không có sản phẩm nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("findAllProductExportFromTo")
    public Object findAllProductExportFromTo(@RequestParam("from") String from, @RequestParam("to") String to) {

        List<ProductDto> dtos = statisticalService.findAllProductExportFromTo(LocalDate.parse(from),LocalDate.parse(to));
        if (dtos.isEmpty() || dtos == null){
            return new ResponseEntity<>("Không có sản phẩm nào", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);

    }
}
