package com.sharework.health.controller.admin;

import com.sharework.health.dto.LoginDto;
import com.sharework.health.dto.UserDto;
import com.sharework.health.service.ClinicService;
import com.sharework.health.service.RoleService;
import com.sharework.health.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/users")
@AllArgsConstructor
public class UserController {

    private UserService userService;

    private RoleService roleService;

    private ClinicService clinicService;

    @GetMapping("")
    public Object getAllUser() {
        List<UserDto> userDtos = userService.findAll();
        if (userDtos.isEmpty()) {
            return new ResponseEntity<>("Không có dữ liệu", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(userDtos,HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public Object getUserById(@PathVariable("id") Integer id) {
        UserDto userDto = userService.findById(id);
        if (userDto == null) {
            return new ResponseEntity<>("Không có dữ liệu", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(userDto,HttpStatus.OK);
    }

//    @PostMapping("")
//    public Object addUser(@RequestBody UserDto dto,BindingResult error) {
//        boolean result = userService.insert(dto);
//        if (!result){
//            return new ResponseEntity<>("Thêm thất bại",HttpStatus.BAD_REQUEST);
//        }
//        return new ResponseEntity<>(dto,HttpStatus.CREATED);
//    }
@PostMapping("")
public Object addUser(@RequestBody UserDto dto,BindingResult error) {
    LoginDto loginDto = userService.showLoginWhenAddUser(dto);
    if (loginDto == null){
        return new ResponseEntity<>("Thêm tài khoản thất bại",HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(loginDto,HttpStatus.CREATED);
}

    @PutMapping("/{user_id}")
    public Object updateUser(@PathVariable("user_id") Integer id , @RequestBody UserDto dto,
                                       BindingResult error) {
        if (error.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        boolean result = userService.update(id, dto);
        if (!result){
            return new ResponseEntity<>("Cập nhật thông tin nhân viên không thành công",HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Cập nhật thông tin nhân viên thành công",HttpStatus.OK);
    }

    @DeleteMapping("/{user_id}")
    public ResponseEntity<UserDto> deleteUser(@PathVariable("user_id") Integer id) {
        userService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("searchName")
    public Object searchUserByUserName (@RequestParam("username") String name){
        List<UserDto> userDtos = userService.searchUserByUsername(name);
        if (userDtos.isEmpty() || userDtos == null){
            return new ResponseEntity<>("Không tìm thấy", HttpStatus.BAD_REQUEST);
        }
        return  new ResponseEntity<>(userDtos, HttpStatus.OK);
    }
}
