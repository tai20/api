package com.sharework.health.controller.customer;

import lombok.AllArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class PublicController {

    @GetMapping("images/getImageSlipUseUpload/{image}")
    public Object getImageSlipUseUpload(@PathVariable("image") String image) throws IOException {
        if (!image.equals("") || image != null) {
            try {
//                Path staticPath = Paths.get("static");
//                Path imagePath = Paths.get("images");
                Path fileName = Paths.get("static/images", image);
                byte[] buffer = Files.readAllBytes(fileName);
                ByteArrayResource byteArrayResource = new ByteArrayResource(buffer);
                return ResponseEntity.ok()
                        .contentLength(buffer.length)
                        .contentType(MediaType.parseMediaType("image/png"))
                        .body(byteArrayResource);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new ResponseEntity<>("Không có ảnh", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("signatures/getSignatureExaminationCardUpload/{signature}")
    public Object getSignatureExaminationCardUpload(@PathVariable("signature") String signature) throws IOException {
        if (!signature.equals("") || signature != null) {
            try {

                Path fileName = Paths.get("static/signatures", signature);
                byte[] buffer = Files.readAllBytes(fileName);
                ByteArrayResource byteArrayResource = new ByteArrayResource(buffer);
                return ResponseEntity.ok()
                        .contentLength(buffer.length)
                        .contentType(MediaType.parseMediaType("image/png"))
                        .body(byteArrayResource);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new ResponseEntity<>("Không có chữ kí", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("getFiles/getFileGeneralExamination/{file}")
    public Object getFileGeneralExamination(@PathVariable("file") String file) throws IOException {
        if (!file.equals("") || file != null) {
            try {

                Path fileName = Paths.get("static/storedFileExaminationDetails", file);
                byte[] buffer = Files.readAllBytes(fileName);
                ByteArrayResource byteArrayResource = new ByteArrayResource(buffer);
                return ResponseEntity.ok()
                        .contentLength(buffer.length)
                        .contentType(MediaType.parseMediaType(Files.probeContentType(fileName)))
                        .body(byteArrayResource);
//                String mimeType = Files.probeContentType(fileName);
//                return mimeType;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new ResponseEntity<>("Không có file", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("getFiles/getFileIndicationCard/{file}")
    public Object getFileIndicationCard(@PathVariable("file") String file) throws IOException {
        if (!file.equals("") || file != null) {
            try {
                Path fileName = Paths.get("static/fileIndicationCards", file);
                byte[] buffer = Files.readAllBytes(fileName);
                ByteArrayResource byteArrayResource = new ByteArrayResource(buffer);
                return ResponseEntity.ok()
                        .contentLength(buffer.length)
                        .contentType(MediaType.parseMediaType(Files.probeContentType(fileName)))
                        .body(byteArrayResource);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new ResponseEntity<>("Không có file", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("images/getAvartar/{image}")
    public Object getAvartar(@PathVariable("image") String image) throws IOException {
        if (!image.equals("") || image != null) {
            try {
//                Path staticPath = Paths.get("static");
//                Path imagePath = Paths.get("images");
                Path fileName = Paths.get("static/avatarCustomer", image);
                byte[] buffer = Files.readAllBytes(fileName);
                ByteArrayResource byteArrayResource = new ByteArrayResource(buffer);
                return ResponseEntity.ok()
                        .contentLength(buffer.length)
                        .contentType(MediaType.parseMediaType("image/png"))
                        .body(byteArrayResource);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new ResponseEntity<>("Không có ảnh", HttpStatus.BAD_REQUEST);
    }
}
