package com.sharework.health.convert;

public interface BaseConvert<E,D> {

    D entityToDto (E entity);
    E dtoToEntity (D dto);

}
