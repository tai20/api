package com.sharework.health.convert;

import com.sharework.health.dto.SubCategoryIndicationCardDto;
import com.sharework.health.dto.CategoryIndicationCardDto;
import com.sharework.health.entity.CategoryIndicationCard;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class CategoryIndicationCardConvert implements BaseConvert<CategoryIndicationCard, CategoryIndicationCardDto> {

    @Override
    public CategoryIndicationCardDto entityToDto(CategoryIndicationCard entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, CategoryIndicationCardDto.class)
        		 .setSubCategoryIndicationCardDto(modelMapper.map(entity.getSubCategoryIndicationCard(), SubCategoryIndicationCardDto.class));
    }

    @Override
    public CategoryIndicationCard dtoToEntity(CategoryIndicationCardDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, CategoryIndicationCard.class);
    }
}
