package com.sharework.health.convert;

import com.sharework.health.dto.ClinicDto;
import com.sharework.health.entity.Clinic;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ClinicConvert implements BaseConvert<Clinic, ClinicDto> {

    @Override
    public ClinicDto entityToDto(Clinic entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, ClinicDto.class);
    }

    @Override
    public Clinic dtoToEntity(ClinicDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, Clinic.class);
    }

}
