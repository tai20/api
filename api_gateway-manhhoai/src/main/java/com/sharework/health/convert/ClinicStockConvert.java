package com.sharework.health.convert;

import com.sharework.health.dto.ClinicDto;
import com.sharework.health.dto.ClinicStockDto;
import com.sharework.health.dto.ProductDto;
import com.sharework.health.entity.ClinicStock;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ClinicStockConvert implements BaseConvert<ClinicStock, ClinicStockDto>{
    @Override
    public ClinicStockDto entityToDto(ClinicStock entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, ClinicStockDto.class)
                .setProductDto(modelMapper.map(entity.getProduct(), ProductDto.class))
                .setClinicDto(modelMapper.map(entity.getClinic(), ClinicDto.class));
    }

    @Override
    public ClinicStock dtoToEntity(ClinicStockDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, ClinicStock.class);
    }

}
