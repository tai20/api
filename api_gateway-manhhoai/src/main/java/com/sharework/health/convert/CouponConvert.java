package com.sharework.health.convert;

import com.sharework.health.dto.CouponDto;
import com.sharework.health.dto.ServiceCategoryDto;
import com.sharework.health.entity.Coupon;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class CouponConvert implements BaseConvert<Coupon, CouponDto> {

    @Override
    public CouponDto entityToDto(Coupon entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, CouponDto.class)
                .setServiceCategoryDto(modelMapper.map(entity.getServiceCategory(), ServiceCategoryDto.class));
    }

    @Override
    public Coupon dtoToEntity(CouponDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, Coupon.class);
    }
}
