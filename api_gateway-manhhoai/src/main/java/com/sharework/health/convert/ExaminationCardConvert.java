package com.sharework.health.convert;

import com.sharework.health.dto.ExaminationCardDto;
import com.sharework.health.dto.SlipUseDto;
import com.sharework.health.entity.ExaminationCard;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ExaminationCardConvert implements BaseConvert<ExaminationCard, ExaminationCardDto> {

    @Override
    public ExaminationCardDto entityToDto(ExaminationCard entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, ExaminationCardDto.class)
                .setSlipUseDto(modelMapper.map(entity.getSlipUse(), SlipUseDto.class));
    }

    @Override
    public ExaminationCard dtoToEntity(ExaminationCardDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, ExaminationCard.class);
    }
}
