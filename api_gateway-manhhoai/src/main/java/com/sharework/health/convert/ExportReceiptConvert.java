package com.sharework.health.convert;


import com.sharework.health.dto.ClinicDto;
import com.sharework.health.dto.ExportReceiptDto;
import com.sharework.health.dto.OrderDto;
import com.sharework.health.dto.UserDto;
import com.sharework.health.entity.ExportReceipt;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ExportReceiptConvert implements BaseConvert<ExportReceipt, ExportReceiptDto> {

    @Override
    public ExportReceiptDto entityToDto(ExportReceipt entity){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, ExportReceiptDto.class)
                .setClinicDto(modelMapper.map(entity.getClinic(), ClinicDto.class))
                .setUserDto(modelMapper.map(entity.getUser(), UserDto.class))
                .setOrderDto(modelMapper.map(entity.getOrder(), OrderDto.class));
    }

    @Override
    public ExportReceipt dtoToEntity(ExportReceiptDto dto){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, ExportReceipt.class);
    }
}
