package com.sharework.health.convert;

import com.sharework.health.dto.CustomerDto;
import com.sharework.health.dto.FreeTrialDto;
import com.sharework.health.dto.ServiceDto;
import com.sharework.health.entity.FreeTrial;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class FreeTrialConvert implements BaseConvert<FreeTrial, FreeTrialDto> {

    @Override
    public FreeTrialDto entityToDto(FreeTrial entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, FreeTrialDto.class)
                .setCustomerDto(modelMapper.map(entity.getCustomer(), CustomerDto.class))
                .setServiceDto(modelMapper.map(entity.getService(), ServiceDto.class));
    }

    @Override
    public FreeTrial dtoToEntity(FreeTrialDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, FreeTrial.class);
    }
}
