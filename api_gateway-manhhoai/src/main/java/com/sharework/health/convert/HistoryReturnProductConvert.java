package com.sharework.health.convert;

import com.sharework.health.dto.HistoryReturnProductDto;
import com.sharework.health.dto.SlipReturnProductDto;
import com.sharework.health.dto.UserDto;
import com.sharework.health.entity.HistoryReturnProduct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class HistoryReturnProductConvert implements BaseConvert<HistoryReturnProduct, HistoryReturnProductDto> {

    @Override
    public HistoryReturnProductDto entityToDto(HistoryReturnProduct entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, HistoryReturnProductDto.class)
                .setSlipReturnProductDto(modelMapper.map(entity.getSlipReturnProduct(), SlipReturnProductDto.class))
                .setUserDto(modelMapper.map(entity.getUser(), UserDto.class));
    }

    @Override
    public HistoryReturnProduct dtoToEntity(HistoryReturnProductDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, HistoryReturnProduct.class);
    }
}
