package com.sharework.health.convert;

import com.sharework.health.dto.ClinicDto;
import com.sharework.health.dto.ImportReceiptDto;
import com.sharework.health.dto.SupplierDto;
import com.sharework.health.dto.UserDto;
import com.sharework.health.entity.ImportReceipt;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ImportReceiptConvert implements BaseConvert<ImportReceipt, ImportReceiptDto>{
    @Override
    public ImportReceiptDto entityToDto(ImportReceipt entity){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, ImportReceiptDto.class)
                .setClinicDto(modelMapper.map(entity.getClinic(), ClinicDto.class))
                .setUserDto(modelMapper.map(entity.getUser(), UserDto.class))
                .setSupplierDto(modelMapper.map(entity.getSupplier(), SupplierDto.class));
    }

    @Override
    public ImportReceipt dtoToEntity(ImportReceiptDto dto){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, ImportReceipt.class);
    }
}
