package com.sharework.health.convert;

import com.sharework.health.dto.ClinicStockDto;
import com.sharework.health.dto.ImportReceiptDetailsDto;
import com.sharework.health.dto.ImportReceiptDto;
import com.sharework.health.entity.ImportReceiptDetails;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ImportReceiptDetailsConvert implements  BaseConvert<ImportReceiptDetails, ImportReceiptDetailsDto>{
    @Override
    public ImportReceiptDetailsDto entityToDto(ImportReceiptDetails entity){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, ImportReceiptDetailsDto.class)
                .setImportReceiptDto(modelMapper.map(entity.getImportReceipt(), ImportReceiptDto.class))
                .setClinicStockDto(modelMapper.map(entity.getClinicStock(), ClinicStockDto.class));
    }

    @Override
    public ImportReceiptDetails dtoToEntity(ImportReceiptDetailsDto dto){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, ImportReceiptDetails.class);
    }
}
