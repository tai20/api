package com.sharework.health.convert;

import com.sharework.health.dto.CustomerDto;
import com.sharework.health.dto.IndicationCardDto;
import com.sharework.health.dto.MedicalCardDto;
import com.sharework.health.dto.UserDto;
import com.sharework.health.entity.IndicationCard;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class IndicationCardConvert implements BaseConvert<IndicationCard, IndicationCardDto> {

    @Override
    public IndicationCardDto entityToDto(IndicationCard entity) {
        ModelMapper modelMapper = new ModelMapper();

        //Convert
        UserConvert userConvert = new UserConvert();

        return modelMapper.map(entity, IndicationCardDto.class)
                .setUserDto(userConvert.entityToDto(entity.getUser()));
    }

    @Override
    public IndicationCard dtoToEntity(IndicationCardDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, IndicationCard.class);
    }
}
