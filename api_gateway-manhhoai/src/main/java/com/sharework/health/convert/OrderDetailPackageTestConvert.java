package com.sharework.health.convert;

import com.sharework.health.dto.OrderDetailPackageTestDto;
import com.sharework.health.dto.OrderDto;
import com.sharework.health.dto.PackageTestDto;
import com.sharework.health.entity.OrderDetailPackageTest;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class OrderDetailPackageTestConvert implements BaseConvert<OrderDetailPackageTest, OrderDetailPackageTestDto> {
    @Override
    public OrderDetailPackageTestDto entityToDto(OrderDetailPackageTest entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, OrderDetailPackageTestDto.class)
                .setOrderDto(modelMapper.map(entity.getOrder(), OrderDto.class))
                .setPackageTestDto(modelMapper.map(entity.getPackageTest(), PackageTestDto.class));
    }

    @Override
    public OrderDetailPackageTest dtoToEntity(OrderDetailPackageTestDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, OrderDetailPackageTest.class);
    }
}
