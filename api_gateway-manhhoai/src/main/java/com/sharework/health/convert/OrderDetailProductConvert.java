package com.sharework.health.convert;

import com.sharework.health.dto.OrderDetailProductDto;
import com.sharework.health.dto.OrderDto;
import com.sharework.health.dto.ProductDto;
import com.sharework.health.entity.OrderDetailProduct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class OrderDetailProductConvert implements BaseConvert<OrderDetailProduct, OrderDetailProductDto> {

    @Override
    public OrderDetailProductDto entityToDto(OrderDetailProduct entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, OrderDetailProductDto.class)
                .setOrderDto(modelMapper.map(entity.getOrder(), OrderDto.class))
                .setProductDto(modelMapper.map(entity.getProduct(), ProductDto.class));
    }

    @Override
    public OrderDetailProduct dtoToEntity(OrderDetailProductDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, OrderDetailProduct.class);
    }
}
