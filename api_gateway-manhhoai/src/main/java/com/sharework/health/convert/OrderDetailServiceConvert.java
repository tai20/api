package com.sharework.health.convert;

import com.sharework.health.dto.OrderDetailServiceDto;
import com.sharework.health.dto.OrderDto;
import com.sharework.health.dto.ServiceDto;
import com.sharework.health.entity.OrderDetailService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class OrderDetailServiceConvert implements BaseConvert<OrderDetailService, OrderDetailServiceDto> {

    @Override
    public OrderDetailServiceDto entityToDto(OrderDetailService entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, OrderDetailServiceDto.class)
                .setOrderDto(modelMapper.map(entity.getOrder(), OrderDto.class))
                .setServiceDto(modelMapper.map(entity.getService(), ServiceDto.class));
    }

    @Override
    public OrderDetailService dtoToEntity(OrderDetailServiceDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, OrderDetailService.class);
    }
}
