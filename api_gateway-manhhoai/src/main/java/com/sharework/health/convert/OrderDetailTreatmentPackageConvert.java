package com.sharework.health.convert;

import com.sharework.health.dto.OrderDetailTreatmentPackageDto;
import com.sharework.health.dto.OrderDto;
import com.sharework.health.dto.TreatmentPackageDto;
import com.sharework.health.entity.OrderDetailTreatmentPackage;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class OrderDetailTreatmentPackageConvert implements BaseConvert<OrderDetailTreatmentPackage, OrderDetailTreatmentPackageDto> {

    @Override
    public OrderDetailTreatmentPackageDto entityToDto(OrderDetailTreatmentPackage entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, OrderDetailTreatmentPackageDto.class)
                .setOrderDto(modelMapper.map(entity.getOrder(), OrderDto.class))
                .setTreatmentPackageDto(modelMapper.map(entity.getTreatmentPackage(), TreatmentPackageDto.class));
    }

    @Override
    public OrderDetailTreatmentPackage dtoToEntity(OrderDetailTreatmentPackageDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, OrderDetailTreatmentPackage.class);
    }
}
