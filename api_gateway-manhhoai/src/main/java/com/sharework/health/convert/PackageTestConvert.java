package com.sharework.health.convert;

import com.sharework.health.dto.PackageTestDto;
import com.sharework.health.dto.TestServiceDto;
import com.sharework.health.entity.PackageTest;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class PackageTestConvert implements BaseConvert<PackageTest, PackageTestDto> {
    @Override
    public PackageTestDto entityToDto(PackageTest entity) {

        TestServiceConvert testServiceConvert = new TestServiceConvert();

        ModelMapper modelMapper = new ModelMapper();

        PackageTestDto packageTestDto = new PackageTestDto();
        packageTestDto = modelMapper.map(entity, PackageTestDto.class);

        if (entity.getTestService() != null) {
            packageTestDto.setTestServiceDto(testServiceConvert.entityToDto(entity.getTestService()));
        } else {
            packageTestDto.setTestServiceDto(null);
        }
        return packageTestDto;
    }

    @Override
    public PackageTest dtoToEntity(PackageTestDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, PackageTest.class);
    }
}
