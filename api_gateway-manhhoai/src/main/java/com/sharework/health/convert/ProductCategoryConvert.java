package com.sharework.health.convert;

import com.sharework.health.dto.ProductCategoryDto;
import com.sharework.health.entity.ProductCategory;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ProductCategoryConvert implements BaseConvert<ProductCategory, ProductCategoryDto> {
    @Override
    public ProductCategoryDto entityToDto(ProductCategory entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, ProductCategoryDto.class)
                ;
    }

    @Override
    public ProductCategory dtoToEntity(ProductCategoryDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, ProductCategory.class);
    }
}
