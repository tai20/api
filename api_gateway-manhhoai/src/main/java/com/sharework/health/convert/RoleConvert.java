package com.sharework.health.convert;

import com.sharework.health.dto.RoleDto;
import com.sharework.health.entity.Role;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class RoleConvert implements BaseConvert<Role, RoleDto> {

    @Override
    public Role dtoToEntity(RoleDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, Role.class);
    }

    @Override
    public RoleDto entityToDto(Role entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, RoleDto.class);
    }
}
