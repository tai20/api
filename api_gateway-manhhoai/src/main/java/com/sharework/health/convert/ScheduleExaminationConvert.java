package com.sharework.health.convert;

import com.sharework.health.dto.*;
import com.sharework.health.entity.ScheduleExamination;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ScheduleExaminationConvert implements BaseConvert<ScheduleExamination, ScheduleExaminationDto> {

    @Override
    public ScheduleExaminationDto entityToDto(ScheduleExamination entity) {
        ModelMapper modelMapper = new ModelMapper();

        CustomerConvert customerConvert = new CustomerConvert();

        UserConvert userConvert = new UserConvert();

        MedicalCardConvert medicalCardConvert = new MedicalCardConvert();


        ScheduleExaminationDto scheduleExaminationDto = new ScheduleExaminationDto();
        scheduleExaminationDto = modelMapper.map(entity, ScheduleExaminationDto.class)
                .setUserDto(userConvert.entityToDto(entity.getUser()))
                .setCustomerDto(customerConvert.entityToDto(entity.getCustomer()));

        if (entity.getMedicalCard() == null) {
            scheduleExaminationDto.setMedicalCardDto(null);
        } else {
            scheduleExaminationDto.setMedicalCardDto(medicalCardConvert.entityToDto(entity.getMedicalCard()));
        }

        return scheduleExaminationDto;
    }

    @Override
    public ScheduleExamination dtoToEntity(ScheduleExaminationDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, ScheduleExamination.class);
    }
}
