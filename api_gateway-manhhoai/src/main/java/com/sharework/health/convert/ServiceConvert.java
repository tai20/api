package com.sharework.health.convert;

import com.sharework.health.dto.ServiceCategoryDto;
import com.sharework.health.dto.ServiceDto;
import com.sharework.health.entity.Service;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ServiceConvert implements BaseConvert<Service, ServiceDto> {

    @Override
    public ServiceDto entityToDto(Service entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, ServiceDto.class)
                .setServiceCategoryDto(modelMapper.map(entity.getServiceCategory(), ServiceCategoryDto.class));
    }

    @Override
    public Service dtoToEntity(ServiceDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, Service.class);
    }
}
