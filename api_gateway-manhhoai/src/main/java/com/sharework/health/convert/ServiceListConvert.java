package com.sharework.health.convert;

import com.sharework.health.dto.CustomerDto;
import com.sharework.health.dto.ServiceDto;
import com.sharework.health.dto.ServiceListDto;
import com.sharework.health.entity.ServiceList;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ServiceListConvert implements BaseConvert<ServiceList, ServiceListDto>{
    @Override
    public ServiceListDto entityToDto(ServiceList entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, ServiceListDto.class)
                .setCustomerDto(modelMapper.map(entity.getCustomer(), CustomerDto.class))
                .setServiceDto(modelMapper.map(entity.getService(), ServiceDto.class));
    }

    @Override
    public ServiceList dtoToEntity(ServiceListDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, ServiceList.class);
    }
}
