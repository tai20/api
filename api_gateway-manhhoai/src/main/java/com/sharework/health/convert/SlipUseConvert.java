package com.sharework.health.convert;

import com.sharework.health.dto.CustomerDto;
import com.sharework.health.dto.SlipUseDto;
import com.sharework.health.entity.SlipUse;
import com.sharework.health.entity.Status;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class SlipUseConvert implements BaseConvert<SlipUse, SlipUseDto> {
    @Override
    public SlipUseDto entityToDto(SlipUse entity) {
        ModelMapper modelMapper = new ModelMapper();
        String status = entity.getStatus() == Status.ACTIVE ? "Còn hiệu lực" :  "Chưa kích hoạt";
        return modelMapper.map(entity, SlipUseDto.class)
                .setCustomerDto(modelMapper.map(entity.getCustomer(), CustomerDto.class));
                //.setStatus(status);
    }

    @Override
    public SlipUse dtoToEntity(SlipUseDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, SlipUse.class);
    }
}
