package com.sharework.health.convert;

import com.sharework.health.dto.CustomerDto;
import com.sharework.health.dto.SlipUsePackageTestDto;
import com.sharework.health.dto.TestServiceDto;
import com.sharework.health.entity.SlipUsePackageTest;
import com.sharework.health.entity.TestService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class SlipUsePackageTestConvert implements BaseConvert<SlipUsePackageTest, SlipUsePackageTestDto> {
    @Override
    public SlipUsePackageTestDto entityToDto(SlipUsePackageTest entity) {

        //Convert
        TestServiceConvert testServiceConvert = new TestServiceConvert();

        CustomerConvert customerConvert = new CustomerConvert();

        ModelMapper modelMapper = new ModelMapper();
        if (entity.getTestService()!=null) {
            return modelMapper.map(entity, SlipUsePackageTestDto.class)
                    .setCustomerDto(modelMapper.map(entity.getCustomer(), CustomerDto.class))
                    .setTestServiceDto(modelMapper.map(entity.getTestService(), TestServiceDto.class));
        } else {
            return modelMapper.map(entity, SlipUsePackageTestDto.class)
                    .setCustomerDto(modelMapper.map(entity.getCustomer(), CustomerDto.class));
        }

    }

    //      .setCustomerDto(customerConvert.entityToDto(entity.getCustomer()))
//            .setTestServiceDto(testServiceConvert.entityToDto(entity.getTestService()));
//
    @Override
    public SlipUsePackageTest dtoToEntity(SlipUsePackageTestDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, SlipUsePackageTest.class);
    }
}
