package com.sharework.health.convert;

import com.sharework.health.dto.CategoryIndicationCardDto;
import com.sharework.health.dto.SlipUsePackageTestDetailDto;
import com.sharework.health.dto.SlipUsePackageTestDto;
import com.sharework.health.entity.SlipUsePackageTestDetail;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class SlipUsepackageTestDetailConvert implements BaseConvert<SlipUsePackageTestDetail, SlipUsePackageTestDetailDto> {
    @Override
    public SlipUsePackageTestDetailDto entityToDto(SlipUsePackageTestDetail entity) {

        //Convert
        CategoryIndicationCardConvert categoryIndicationCardConvert = new CategoryIndicationCardConvert();

        SlipUsePackageTestConvert slipUsePackageTestConvert = new SlipUsePackageTestConvert();

        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, SlipUsePackageTestDetailDto.class)
                .setCategoryIndicationCardDto(categoryIndicationCardConvert.entityToDto(entity.getCategoryIndicationCard()))
                .setSlipusePackageTestDto(slipUsePackageTestConvert.entityToDto(entity.getSlipusePackageTest()));
    }

    @Override
    public SlipUsePackageTestDetail dtoToEntity(SlipUsePackageTestDetailDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, SlipUsePackageTestDetail.class);
    }
}
