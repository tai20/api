package com.sharework.health.convert;

import com.sharework.health.dto.SubCategoryIndicationCardDto;
import com.sharework.health.entity.SubCategoryIndicationCard;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class SubCategoryIndicationCardConvert implements BaseConvert<SubCategoryIndicationCard, SubCategoryIndicationCardDto> {

    @Override
    public SubCategoryIndicationCardDto entityToDto(SubCategoryIndicationCard entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, SubCategoryIndicationCardDto.class);
    }

    @Override
    public SubCategoryIndicationCard dtoToEntity(SubCategoryIndicationCardDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, SubCategoryIndicationCard.class);
    }
}
