package com.sharework.health.convert;

import com.sharework.health.dto.SupplierDto;
import com.sharework.health.entity.Supplier;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class SupplierConvert implements BaseConvert<Supplier, SupplierDto> {

    @Override
    public SupplierDto entityToDto(Supplier entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, SupplierDto.class);
    }

    @Override
    public Supplier dtoToEntity(SupplierDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, Supplier.class);
    }
}
