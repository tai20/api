package com.sharework.health.convert;

import com.sharework.health.dto.ClinicStockDto;
import com.sharework.health.dto.TransferReceiptDetailsDto;
import com.sharework.health.dto.TransferReceiptDto;
import com.sharework.health.entity.TransferReceiptDetails;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class TransferReceiptDetailsConvert implements BaseConvert<TransferReceiptDetails, TransferReceiptDetailsDto>{

    @Override
    public TransferReceiptDetailsDto entityToDto(TransferReceiptDetails entity){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, TransferReceiptDetailsDto.class)
                .setTransferReceiptDto(modelMapper.map(entity.getTransferReceipt(), TransferReceiptDto.class))
                .setClinicStockDto(modelMapper.map(entity.getClinicStock(), ClinicStockDto.class));
    }

    @Override
    public TransferReceiptDetails dtoToEntity(TransferReceiptDetailsDto dto){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, TransferReceiptDetails.class);
    }

}
