package com.sharework.health.convert;

import com.sharework.health.dto.ServiceCategoryDto;
import com.sharework.health.entity.ServiceCategory;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class TreatmentPackageCategoryConvert implements BaseConvert<ServiceCategory, ServiceCategoryDto> {


    @Override
    public ServiceCategoryDto entityToDto(ServiceCategory entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, ServiceCategoryDto.class);
    }

    @Override
    public ServiceCategory dtoToEntity(ServiceCategoryDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, ServiceCategory.class);
    }
}
