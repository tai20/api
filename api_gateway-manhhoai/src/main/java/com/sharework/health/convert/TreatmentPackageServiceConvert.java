package com.sharework.health.convert;

import com.sharework.health.dto.TreatmentPackageServiceDto;
import com.sharework.health.entity.TreatmentPackageService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class TreatmentPackageServiceConvert implements BaseConvert<TreatmentPackageService, TreatmentPackageServiceDto> {

    @Override
    public TreatmentPackageServiceDto entityToDto(TreatmentPackageService entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, TreatmentPackageServiceDto.class);
    }

    @Override
    public TreatmentPackageService dtoToEntity(TreatmentPackageServiceDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, TreatmentPackageService.class);
    }
}
