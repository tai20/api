package com.sharework.health.convert;

import com.sharework.health.dto.ClinicDto;
import com.sharework.health.dto.SupplierDto;
import com.sharework.health.dto.UserDto;
import com.sharework.health.dto.WarehouseReceiptDto;
import com.sharework.health.entity.WarehouseReceipt;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class WarehouseReceiptConvert implements BaseConvert<WarehouseReceipt, WarehouseReceiptDto> {

    @Override
    public WarehouseReceiptDto entityToDto(WarehouseReceipt entity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(entity, WarehouseReceiptDto.class)
                .setClinicDto(modelMapper.map(entity.getClinic(), ClinicDto.class))
                .setSupplierDto(modelMapper.map(entity.getSupplier(), SupplierDto.class))
                .setUserDto(modelMapper.map(entity.getUser(), UserDto.class));
    }

    @Override
    public WarehouseReceipt dtoToEntity(WarehouseReceiptDto dto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(dto, WarehouseReceipt.class);
    }
}
