package com.sharework.health.dto;

import com.sharework.health.entity.Order;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class BillDto {
    private Integer id;

    private OrderDto orderDto;

    private LocalDateTime paymentDate;

    private String paymentMethod;

    private BigDecimal paymentAmount;

}
