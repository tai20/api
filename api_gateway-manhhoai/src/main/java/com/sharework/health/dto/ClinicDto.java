package com.sharework.health.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sharework.health.entity.ClinicType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ClinicDto{

    private Integer id;

    private String name;

    private String address;

    private ClinicType type;

    private String status;

    @JsonIgnore
    private List<UserDto> userDtos;

    @JsonIgnore
    private List<WarehouseReceiptDto> warehouseReceiptDtos;

    //@JsonIgnore

}
