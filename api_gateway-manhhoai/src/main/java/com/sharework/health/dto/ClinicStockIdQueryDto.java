package com.sharework.health.dto;

import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ClinicStockIdQueryDto implements Serializable {
    private Integer clinic_stock_id;
}
