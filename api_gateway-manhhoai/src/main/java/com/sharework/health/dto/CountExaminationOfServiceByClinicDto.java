package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CountExaminationOfServiceByClinicDto {

    private Integer serviceId;

    private String serviceName;

    private Integer clinicId;

    private String clinicName;

    private long countExamination;
}
