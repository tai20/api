package com.sharework.health.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CouponDto {

    private Integer id;

    private String name;

    private String code;

    private LocalDate startDate;

    private LocalDate endDate;

    private int quantity;

    private double discount;

    @JsonIgnore
    private List<CustomerCouponDto> customerCouponDtos;

    private ServiceCategoryDto serviceCategoryDto;

}
