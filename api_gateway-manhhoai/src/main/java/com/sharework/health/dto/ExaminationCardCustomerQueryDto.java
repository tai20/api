package com.sharework.health.dto;

import com.sharework.health.entity.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ExaminationCardCustomerQueryDto {

    private Integer id;

    private LocalDateTime dateOfExamination;

    private Status status;

    private Integer slipUseId;

    private String customerName;

    private String phoneNumber;

    private String signature;

}
