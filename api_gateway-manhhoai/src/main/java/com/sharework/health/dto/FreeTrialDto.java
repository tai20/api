package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class FreeTrialDto {

    private Integer id;

    private LocalDateTime createdDate;

    private CustomerDto customerDto;

    private ServiceDto serviceDto;
}
