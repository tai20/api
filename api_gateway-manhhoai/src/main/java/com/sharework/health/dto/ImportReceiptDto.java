package com.sharework.health.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sharework.health.entity.Clinic;
import com.sharework.health.entity.ImportReceiptDetails;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;


import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ImportReceiptDto {
    private Integer id;

    private ClinicDto clinicDto;

    private UserDto userDto;

    private SupplierDto supplierDto;

    private LocalDateTime dateimport;

    private BigDecimal sumprice;

    private String status;

    @JsonIgnore
    private List<ImportReceiptDetails> importReceiptDetails;

    @JsonIgnore
    private List<Clinic> clinics;

}
