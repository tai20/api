package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.apache.poi.hpsf.Decimal;

import java.math.BigDecimal;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class LevelDto {
    private Integer id;

    private String name;

    private BigDecimal min_value;

    private BigDecimal max_value;
}
