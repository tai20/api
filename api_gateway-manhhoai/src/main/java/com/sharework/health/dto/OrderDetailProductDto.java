package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrderDetailProductDto {

    private OrderDto orderDto;

    private ProductDto productDto;

    private int quantity;

    private BigDecimal price;
}
