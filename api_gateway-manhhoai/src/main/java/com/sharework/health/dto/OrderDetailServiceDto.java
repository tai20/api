package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrderDetailServiceDto {

    private OrderDto orderDto;

    private ServiceDto serviceDto;

    private int quantity;

    private BigDecimal price;
}
