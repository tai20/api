package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;



@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class OtherReceiptDetailsQueryDto {
    private Integer receipt_id;

    private Integer clinic_stock_id;

    private Integer number_product;

    private Integer product_id;

    private String name;
}
