package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class PrescriptionDetailDto {

    private PrescriptionDto prescriptionDto;

    private ProductDto productDto;

    private String tutorial;

    private int quantity;
}
