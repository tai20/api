package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RemainingProductStatisticsQueryDto{

    private Integer product_id;

    private String product_name;

    private String product_category;

    private BigDecimal price;

    private BigDecimal who_sale_price;

    private String unit;

    private String warehouse_name;

    private Integer quantity;


}
