package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDate;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SalaryDto {

    private Integer id;
    private LocalDate createdDate;

    private String status;

    private String paymentMethod;

    private int workingPeriod;
    private int countTimekeeping;
    private BigDecimal totalPayment;

    private UserDto userDto;
}
