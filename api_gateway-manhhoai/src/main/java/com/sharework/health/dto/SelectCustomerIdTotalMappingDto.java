package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SelectCustomerIdTotalMappingDto {

    private String name;

    private String phoneNumber;

    private String address;

    private BigDecimal total;
}
