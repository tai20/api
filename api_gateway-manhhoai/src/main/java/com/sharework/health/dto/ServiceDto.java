package com.sharework.health.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sharework.health.entity.TreatmentPackageService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ServiceDto {

    private Integer id;

    private String name;

    private LocalDate period;

    private BigDecimal price;

    @JsonIgnore
    private List<TreatmentPackageService> treatmentPackageServices;

    private ServiceCategoryDto serviceCategoryDto;
}
