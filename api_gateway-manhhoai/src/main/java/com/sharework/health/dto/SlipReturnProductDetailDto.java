package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SlipReturnProductDetailDto {

    private SlipReturnProductDto slipReturnProductDto;

    private ProductDto productDto;

    private int quantityStock;

    private int quantity;
}
