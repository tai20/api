package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SlipUsePackageTestDetailDto {

    private Integer id;

    private CategoryIndicationCardDto categoryIndicationCardDto;

    private SlipUsePackageTestDto slipusePackageTestDto;

    private Integer numberofuse;
}
