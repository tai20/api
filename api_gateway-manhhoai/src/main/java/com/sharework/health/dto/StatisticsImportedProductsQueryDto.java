package com.sharework.health.dto;


import com.sharework.health.entity.ReceiptStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class StatisticsImportedProductsQueryDto {
    private Integer product_id;

    private String product_name;

    private String product_category;

    private BigDecimal import_price;

    private String unit;

    private Integer quantity_import;

    private Integer warehouse_id;

    private String warehouse_name;


}
