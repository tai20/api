package com.sharework.health.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SubCategoryIndicationCardDto {

	private Integer id;

	private String name;

	private String description;

    @JsonIgnore
    private List<CategoryIndicationCardDto> categoryIndicationCardDtos;
}
