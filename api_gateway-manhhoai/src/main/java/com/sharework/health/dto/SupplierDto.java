package com.sharework.health.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SupplierDto {

    private Integer id;

    private String name;

    private String phoneNumber;

    private String address;

    private String status;

    private String type;

    @JsonIgnore
    private List<WarehouseReceiptDto> warehouseReceiptDtos;
}
