package com.sharework.health.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sharework.health.entity.MedicalCard;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TotalCostDto {

    private Integer id;

    private String paymentmethod;

    private BigDecimal totalpayment;

}
