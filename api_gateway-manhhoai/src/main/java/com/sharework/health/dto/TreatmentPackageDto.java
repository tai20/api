package com.sharework.health.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TreatmentPackageDto {

    private Integer id;

    private String name;

    private String status;

    private BigDecimal total;

    private ServiceCategoryDto serviceCategoryDto;

    @JsonIgnore
    private List<SlipUseDto> slipUseDtos;
}
