package com.sharework.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WarehouseReceiptDetailDto {

    private ProductDto productDto;

    private WarehouseReceiptDto warehouseReceiptDto;

    private int numberProductImport;
}
