package com.sharework.health.entity;

public enum ExaminationStatus {
    Examining, Examined;
}
