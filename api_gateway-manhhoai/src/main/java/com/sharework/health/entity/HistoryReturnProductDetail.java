package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@IdClass(HistoryReturnProductDetail_PK.class)
public class HistoryReturnProductDetail implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "historyreturnproduct_id")
    private HistoryReturnProduct historyReturnProduct;

    @Id
    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    private int quantity;
}
