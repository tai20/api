package com.sharework.health.entity;

import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "importreceiptdetails")
@IdClass(ImportReceiptDetails_PK.class)
public class ImportReceiptDetails implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "receipt_id")
    private ImportReceipt importReceipt;

    @Id
    @ManyToOne
    @JoinColumn(name = "clinic_stock_id")
    private ClinicStock clinicStock;

    @Column
    private Integer number_product_import;

    @Column(precision = 19, scale = 2)
    private BigDecimal import_price;

}
