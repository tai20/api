package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@IdClass(IndicationCardDetail_PK.class)
public class IndicationCardDetail implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "indicationcard_id")
    private IndicationCard indicationCard;

    @Id
    @ManyToOne
    @JoinColumn(name = "categoryindicationcard_id")
    private CategoryIndicationCard categoryIndicationCard;

    private String fileIndication;

    @ElementCollection
    @CollectionTable(name = "ListImage",
            joinColumns = { @JoinColumn(name = "indicationcard_id"), @JoinColumn( name = "categoryindicationcard_id")})
    @LazyCollection(LazyCollectionOption.FALSE)
    @Column(name = "listimage", nullable = false)
    private Set<String> listImages;

    private String status;

    @Column(columnDefinition="TEXT")
    private String liverandkidney;

    @Column(columnDefinition="TEXT")
    private String bilebuctsAndgallbladderandbladder;

    private String abdominalfluid;

    @Column(columnDefinition="TEXT")
    private String uterus;

    private String dkts;

    private String density;

    @Column(columnDefinition="TEXT")
    private String ovary;

    @Column(columnDefinition="TEXT")
    private String endometrium;

    @Column(columnDefinition="TEXT")
    private String uterinemuscle;

    private String cervical;

    private String fornixfluid;

    @Column(columnDefinition="TEXT")
    private String smallfetusultrasoundinformation1;

    @Column(columnDefinition="TEXT")
    private String smallfetusultrasoundinformation2;

    @Column(columnDefinition="TEXT")
    private String blackandwhitefetalultrasoundinformation;

    @Column(columnDefinition="TEXT")
    private String colordoppleroffetalbloodvesselsandnuchaltranslucency;

    @Column(columnDefinition="TEXT")
    private String fetalAnatomy;

    private String pidmuterus;

    @Column(columnDefinition="TEXT")
    private String unbt;

    @Column(columnDefinition="TEXT")
    private String interstitial;

    @Column(columnDefinition="TEXT")
    private String tntc;

    @Column(columnDefinition="TEXT")
    private String colordopplerofFetalbloodvessels;

    @Column(columnDefinition="TEXT")
    private String pregnancyultrasound4d;

    @Column(columnDefinition="TEXT")
    private String fetalanatomyheadfaceneck;

    @Column(columnDefinition="TEXT")
    private String fetalanatomychest;

    @Column(columnDefinition="TEXT")
    private String fetalanatomystomach;

    @Column(columnDefinition="TEXT")
    private String fetalanatomythefourlimbs;

    @Column(columnDefinition="TEXT")
    private String mlt;

    @Column(columnDefinition="TEXT")
    private String bigblackandwhitefetalultrasoundinformation;

    @Column(columnDefinition="TEXT")
    private String conclude;

    @Column(columnDefinition="TEXT")
    private String ctc_dk;

    @Column(columnDefinition="TEXT")
    private String bldb;

    @Column(columnDefinition="TEXT")
    private String blda;

    @Column(columnDefinition="TEXT")
    private String thewomb;

}
