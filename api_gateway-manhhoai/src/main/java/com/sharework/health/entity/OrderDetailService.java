package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@IdClass(OrderDetailService_PK.class)
public class OrderDetailService implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;

    @Id
    @ManyToOne
    @JoinColumn(name = "service_id")
    private Service service;

    private int quantity;

    private BigDecimal price;
}
