package com.sharework.health.entity;

public enum OrderStatus {
    Processing, Confirmed, Paying, Paid, Examining, Examined;
}
