package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "otherswarehousereceiptdetails")
@IdClass(OthersWarehouseReceiptDetails_PK.class)
public class OthersWarehouseReceiptDetails implements Serializable {
    @Id
    @ManyToOne
    @JoinColumn(name = "receipt_id")
    private OthersWarehouseReceipt othersWarehouseReceipt;

    @Id
    @ManyToOne
    @JoinColumn(name = "clinic_stock_id")
    private ClinicStock clinicStock;
    
    private Integer number_product;
}
