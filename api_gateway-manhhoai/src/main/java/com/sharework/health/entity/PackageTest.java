package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
@Entity
@Table(name = "packagetest")
public class PackageTest implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer packagetest_id;

    private String packagetestname;

    @Enumerated(EnumType.STRING)
    private Status status;

    private BigDecimal price;

    private Integer numberofuse;

    @ManyToOne
    @JoinColumn(name = "testService_id")
    private TestService testService;

    @OneToMany(mappedBy = "packageTest", fetch = FetchType.LAZY)
    private List<PackageTestServiceDetail> indicationCardDetails;

    @OneToMany(mappedBy = "packageTest", fetch = FetchType.LAZY)
    private List<OrderDetailPackageTest> orderDetailPackageTests;


}
