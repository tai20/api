package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "slipusedetail")
@IdClass(SlipUseDetail_PK.class)
public class SlipUseDetail implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "slipuse_id")
    private SlipUse slipUse;

    @Id
    @ManyToOne
    @JoinColumn(name = "service_id")
    private Service service;

    private int numberOfStock;

    private int numberOfUse;
}
