package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
@Entity
@Table(name = "slipusepackagetestdetail")
public class SlipUsePackageTestDetail implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "categoryindicationcard_id")
    private CategoryIndicationCard categoryIndicationCard;

    @ManyToOne
    @JoinColumn(name = "slipusepackagetest_id")
    private SlipUsePackageTest slipusePackageTest;

    private Integer numberofuse;


}
