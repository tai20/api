package com.sharework.health.entity;

public enum SupplierType {
    Clinic, Supplier;
}
