package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
@Entity
@Table(name = "testservices")
public class TestService implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer testservice_id;

    private String nameservice;

    private BigDecimal price;

    @OneToMany(mappedBy = "testService", fetch = FetchType.LAZY)
    private List<MedicalCard> testServices;

    @OneToMany(mappedBy = "testService", fetch = FetchType.LAZY)
    private List<SlipUsePackageTest> slipusePackageTests;

    @OneToMany(mappedBy = "testService", fetch = FetchType.LAZY)
    private List<PackageTest> packageTests;


}
