package com.sharework.health.entity;

public enum TransferStatus {
    MOVING, RECEIVED, DELETED
}
