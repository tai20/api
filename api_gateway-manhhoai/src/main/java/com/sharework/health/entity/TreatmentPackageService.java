package com.sharework.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@IdClass(TreatmentPackageService_PK.class)
public class TreatmentPackageService implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "treatmentpackage_id")
    private TreatmentPackage treatmentPackage;

    @Id
    @ManyToOne
    @JoinColumn(name = "service_id")
    private Service service;

    private int numberOfUse;

    private BigDecimal price;
}
