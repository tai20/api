package com.sharework.health.entity;

import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "users")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @Column(unique = true)
    private String email;

    private String password;

    private LocalDate birthDate;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    private String address;

    private String phoneNumber;

    @Enumerated(EnumType.STRING)
    private Status status;

    private String certificate;

    private int workExperience;

    private String cmnd;

    private String token;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

    @ManyToOne
    @JoinColumn(name = "clinic_id")
    private Clinic clinic;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Order> orders;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Advisory> advisories;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<WarehouseReceipt> warehouseReceipts;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<ExaminationCardDetail> examinationCardDetails;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<ScheduleExamination> scheduleExaminations;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<IndicationCard> indicationCards;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<ImportReceipt> importReceipts;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private  List<ExportReceipt> exportReceipts;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<TransferReceipt> transferReceipts;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<OthersWarehouseReceipt> othersWarehouseReceipts;

}
