package com.sharework.health.helper;

import java.util.List;
import java.util.Random;

import com.sharework.health.entity.*;
import com.sharework.health.repository.*;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;

import static javax.swing.UIManager.get;

@RestController
@RequestMapping("database")
@AllArgsConstructor
public class InitDatabase {

    private UserRepository userRepository;

    private ClinicRepository clinicRepository;

    private ClinicStockRepository clinicStockRepository;

    private RoleRepository roleRepository;

    private InitValue initValue;

    private CustomerRepository customerRepository;

    private ProductCategoryRepository productCategoryRepository;

    private ProductRepository productRepository;

    private TreatmentPackageRepository treatmentPackageRepository;

    private ServiceCategoryRepository serviceCategoryRepository;

    private SlipUseRepository slipUseRepository;

    private CouponRepository couponRepository;

    private OrderRepository orderRepository;

    private OrderDetailProductRepository orderDetailProductRepository;

    private OrderDetailTreatmentPackageRepository orderDetailTreatmentPackageRepository;

    private OrderDetailServiceRepository orderDetailServiceRepository;

    private SupplierRepository supplierRepository;

//    private WarehouseReceiptRepository warehouseReceiptRepository;

//    private WarehouseReceiptDetailRepository warehouseReceiptDetailRepository;

    private ServiceRepository serviceRepository;

    private AdvisoryRepository advisoryRepository;

    private CustomerCategoryRepository customerCategoryRepository;

    @GetMapping("")
    public String init() {
        try {
//            Chèn tất cả trường

/*            initCustomerCategories();
            initClinics();
            initRoles();
            initUsers();
            initCustomers();
            initProductCategories();
            initProducts();
            initServiceCategories();
            initServices();
            initTreatmentPackages();
            initCoupons();
            initOrders();
            initOrderDetailProducts();
            initOrderDetailTreatmentPackages();
            initOrderDetailServices();
            initSuppliers();
            initSlipUses();
            initAdvisories();
           initClinicStock();*/
            //
            //Bỏ kho cũ
//            initWarehouseReceipts();
//            initWarehouseReceiptDetails();

            return "test";
        }catch (Exception e){
            e.printStackTrace();
        }
        return "test failed";
    }

    private void initClinics(){
        Clinic clinic = new Clinic();
        for(int i = 0; i < DataValue.CLINIC_NAME.length; i++) {
            ClinicType type;
            type = i == 0 ? ClinicType.DEPOT : ClinicType.BRANCH;
            clinic = initValue.initClinic(i);
            clinic.setType(type);
            clinicRepository.save(clinic);
        }
    }

    private void initClinicStock(){
        List<Clinic> clinics = clinicRepository.findAll();
        List<Product> products = productRepository.findAll();
        Random random = new Random();

        ClinicStock clinicStock= new ClinicStock();
//        for(int i =0; i < 50; i++){
//            clinicStock = initValue.initClinicStock(products.get(i),
//                    clinics.get(random.nextInt(clinics.size())));
//            clinicStockRepository.save(clinicStock);
//        }

        for(int i =0; i < 50; i++){
            for (int j=0; j<clinics.size(); j++){
                clinicStock = initValue.initClinicStock(products.get(i), clinics.get(j));
                clinicStockRepository.save(clinicStock);
            }

        }

    }

//    private void initImportReceipt(){
//        List<Clinic> clinics = clinicRepository.findAll();
//        List<User> users = userRepository.findAll();
//        List<Supplier> suppliers = supplierRepository.findAll();
//        for(int i =0; i < 50; i++){
//
//        }
//    }


    private void initRoles(){
        List<Role> roles = roleRepository.findAll();
        Role role = new Role();
        for (int i=0; i < DataValue.ROLE_NAME.length; i++){
            role = initValue.initRole(i);
            roleRepository.save(role);
        }
    }

    private void initUsers(){
        Random random = new Random();
        List<Role> roles = roleRepository.findAll();
        List<Clinic> clinics = clinicRepository.findAll();

        User user = initValue.initUser(roles.get(0), clinics.get(0));
        user.setEmail("tuanbin007@gmail.com");
        String password = BCrypt.hashpw("tuan", BCrypt.gensalt());
        user.setPassword(password);
        userRepository.save(user);
//
//        try {
//            for(int i = 0; i < 10; i++) {
//                User user1 = initValue.initUser(roles.get(random.nextInt(roles.size())),clinics.get(random.nextInt(clinics.size())));
//                userRepository.save(user1);
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }
    }

    private void initCustomers(){
        List<Role> roles = roleRepository.findAll();
        List<CustomerCategory> customerCategories = customerCategoryRepository.findAll();
        List<Clinic> clinics = clinicRepository.findAll();
        Random random = new Random();
        Customer customer = new Customer();
        try {
            for (int i=0; i < 50; i++){
                customer = initValue.initCustomer(customerCategories.get(0), clinics.get(random.nextInt(clinics.size())));
                customerRepository.save(customer);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void initProductCategories(){

        for (int i=0 ; i < 4; i++){
            ProductCategory productCategory = initValue.initProductCategory(i);
            productCategoryRepository.save(productCategory);
        }
    }

    private void initProducts(){
        List<ProductCategory> productCategories = productCategoryRepository.findAll();
        List<Clinic> clinics = clinicRepository.findAll();
        Random random = new Random();
        for (int i = 0; i< 50; i++){
            Product product = initValue.initProduct(productCategories.get(random.nextInt(productCategories.size())),
                    clinics.get(random.nextInt(clinics.size())));
            productRepository.save(product);
        }
    }


    private void initTreatmentPackages(){
        List<ServiceCategory> serviceCategories = serviceCategoryRepository.findAll();
        TreatmentPackage treatmentPackage = new TreatmentPackage();
        Random random = new Random();
        for (int i = 0; i < 50; i++){
            treatmentPackage = initValue.initTreatmentPackage(serviceCategories.get
                    (random.nextInt(serviceCategories.size())));
            treatmentPackageRepository.save(treatmentPackage);
        }
    }

    private void initSlipUses(){
        List<Customer> customers = customerRepository.findAll();
        List<TreatmentPackage> treatmentPackages = treatmentPackageRepository.findAll();
        Random random = new Random();
        SlipUse slipUse = new SlipUse();
        for (int i=0; i < 50; i++){
            slipUse = initValue.initSlipUse(customers.get(random.nextInt(customers.size())),
                    treatmentPackages.get(random.nextInt(treatmentPackages.size())));
            slipUseRepository.save(slipUse);
        }
    }

    private void initCoupons(){
        Coupon coupon = new Coupon();
        Random random = new Random();
        List<ServiceCategory> serviceCategories = serviceCategoryRepository.findAll();
        for (int i = 0; i < 50; i++){
            coupon = initValue.initCoupon(serviceCategories.get(random.nextInt(serviceCategories.size())));
            couponRepository.save(coupon);
        }
    }

    private void initOrders() {
        Order order = new Order();
        Random random = new Random();
        List<Customer> customers = customerRepository.findAll();
        List<User> users = userRepository.findAll();
        for(int i = 0; i < 100; i++) {
            order = initValue.initOrder(customers.get(random.nextInt(customers.size())),
                    users.get(random.nextInt(users.size())));
            orderRepository.save(order);
        }
    }

    private void initOrderDetailProducts() {

        OrderDetailProduct orderDetailProduct = new OrderDetailProduct();

        Random random = new Random();
        List<Order> orders = orderRepository.findAll();
        List<Product> products = productRepository.findAll();
        for(int i = 0; i < 10; i++) {
            orderDetailProduct = initValue.initOrderDetailProduct(orders.get(i),
                    products.get(random.nextInt(products.size())));
            orderDetailProductRepository.save(orderDetailProduct);
            //orderRepository.save(orders.get(i).setOrderDetailProducts(Arrays.asList(orderDetailProduct)));
        }
    }

    private void initOrderDetailTreatmentPackages() {

        OrderDetailTreatmentPackage orderDetailTreatmentPackage = new OrderDetailTreatmentPackage();

        Random random = new Random();
        List<Order> orders = orderRepository.findAll();
        List<TreatmentPackage> treatmentPackages = treatmentPackageRepository.findAll();
        for(int i = 10; i < 30; i++) {
            orderDetailTreatmentPackage = initValue.initOrderDetailTreatmentPackage(orders.get(i),
                    treatmentPackages.get(random.nextInt(treatmentPackages.size())));
            orderDetailTreatmentPackageRepository.save(orderDetailTreatmentPackage);
            //orderRepository.save(orders.get(i).setOrderDetailProducts(Arrays.asList(orderDetailProduct)));
        }
    }

    private void initOrderDetailServices() {

        OrderDetailService orderDetailService = new OrderDetailService();

        Random random = new Random();
        List<Order> orders = orderRepository.findAll();
        List<Service> services = serviceRepository.findAll();
        for(int i = 30; i < 50; i++) {
            orderDetailService = initValue.initOrderDetailService(orders.get(i),
                    services.get(random.nextInt(services.size())));
            orderDetailServiceRepository.save(orderDetailService);
            //orderRepository.save(orders.get(i).setOrderDetailProducts(Arrays.asList(orderDetailProduct)));
        }
    }

    private void initSuppliers(){
        Supplier supplier = new Supplier();
        Random random = new Random();
        for (int i = 0; i < 50; i++){
            supplier = initValue.initSupplier();
            supplierRepository.save(supplier);
        }
    }

//    private void initWarehouseReceipts(){
//        WarehouseReceipt warehouseReceipt = new WarehouseReceipt();
//        Random random = new Random();
//        List<Supplier> suppliers = supplierRepository.findAll();
//        List<Clinic> clinics = clinicRepository.findAll();
//        List<User> users = userRepository.findAll();
//        for (int i = 0; i < 50; i++){
//            warehouseReceipt = initValue.initWarehouseReceipt(suppliers.get(random.nextInt(suppliers.size())), clinics.get(random.nextInt(clinics.size())),
//                    users.get(random.nextInt(users.size())));
//            warehouseReceiptRepository.save(warehouseReceipt);
//        }
//    }

//    private void initWarehouseReceiptDetails(){
//        WarehouseReceiptDetail warehouseReceiptDetail = new WarehouseReceiptDetail();
//        Random random = new Random();
//        List<WarehouseReceipt> warehouseReceipts = warehouseReceiptRepository.findAll();
//        List<Product> products = productRepository.findAll();
//        for (int i = 0; i < 50; i++){
//            warehouseReceiptDetail = initValue.initWarehouseReceiptDetail(warehouseReceipts.get(i), products.get(random.nextInt(products.size())));
//            warehouseReceiptDetailRepository.save(warehouseReceiptDetail);
//        }
//    }

    private void initServiceCategories(){
        ServiceCategory serviceCategory = new ServiceCategory();
        for (int i=0; i <= 10; i++){
            serviceCategory = initValue.initServiceCategory();
            serviceCategoryRepository.save(serviceCategory);
        }
    }

    private void initServices(){
        Service service = new Service();
        List<ServiceCategory> serviceCategories = serviceCategoryRepository.findAll();
        Random random = new Random();
        for (int i = 0; i < 50; i++){
            service = initValue.initService(serviceCategories.get(random.nextInt(serviceCategories.size())));
            serviceRepository.save(service);
        }
    }

    private void initAdvisories(){
        Advisory advisory = new Advisory();
        List<User> users = userRepository.findAll();
        List<Customer> customers = customerRepository.findAll();
        Random random = new Random();
        for (int i = 0; i < 50; i++){
            advisory = initValue.initAdvisory(users.get(random.nextInt(users.size())), customers.get(random.nextInt(customers.size())));
            advisoryRepository.save(advisory);
        }
    }

    private void initCustomerCategories(){
        CustomerCategory customerCategory = new CustomerCategory();

        for (int i = 0; i < 4; i++){
            customerCategory = initValue.initCustomerCategory(i);
            customerCategoryRepository.save(customerCategory);
        }
    }



}
