package com.sharework.health.helper;

import com.sharework.health.entity.*;
import com.sharework.health.repository.ClinicRepository;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

@Component
public class InitValue {

    private ClinicRepository clinicRepository;

    private static final String PASSWORD = BCrypt.hashpw("12345", BCrypt.gensalt());

    private DataValue dataValue;

    private Random random = new Random();

    public Clinic initClinic(int i){
        //Status status = random.nextInt(100)%2==0 ? Status.ACTIVE : Status.DEACTIVE;
        //String clinicType = random.nextInt(100)%2==0 ? "Kho tổng" : "Kho chi nhánh";
//        ClinicType type = random.nextInt(100)%2==0 ? ClinicType.DEPOT : ClinicType.BRANCH;
        Clinic clinic = new Clinic()
                .setName(DataValue.CLINIC_NAME[i])
                .setAddress(DataValue.ADDRESS[i])
                .setStatus(Status.ACTIVE);
//                .setType(type);
        return clinic;
    }

    public ClinicStock initClinicStock(Product product, Clinic clinic){

        ClinicStock clinicStock = new ClinicStock()
                .setProduct(product)
                .setClinic(clinic)
                .setQuantity(product.getQuantity())
                .setUnit(product.getUnit())
                ;
        return clinicStock;
    }


    public Role initRole(int i){
        Role role = new Role()
                .setName(DataValue.ROLE_NAME[i])
                .setDescription(DataValue.ROLE_DESCRIPTION[i])
                .setStatus(Status.ACTIVE);
        return role;
    }

    public Customer initCustomer(CustomerCategory customerCategory, Clinic clinic){
        Gender gender = random.nextInt(100)%2==0 ? Gender.Nam : Gender.Nữ;

        Customer customer = new Customer()
                .setName(DataValue.USER_NAME[random.nextInt(DataValue.USER_NAME.length)])
                .setEmail(DataValue.USER_EMAIL[random.nextInt(DataValue.USER_EMAIL.length)])
                .setGender(gender)
                .setBirthDate(LocalDate.now().withYear(1999))
                .setPhoneNumber(DataValue.USER_PHONENUMBER[random.nextInt(DataValue.USER_PHONENUMBER.length)])
                .setAddress(DataValue.ADDRESS[random.nextInt(DataValue.ADDRESS.length)])
                .setCmnd(DataValue.USER_PHONENUMBER[random.nextInt(DataValue.USER_PHONENUMBER.length)])
                .setCreatedDate(LocalDateTime.now())
                .setSkinStatus(DataValue.SKIN_STATUS[random.nextInt(DataValue.SKIN_STATUS.length)])
                .setCustomerCategory(customerCategory)
                .setCustomerResource(DataValue.CUSTOMER_RESOURCE[random.nextInt(DataValue.CUSTOMER_RESOURCE.length)])
                .setClinic(clinic)
                .setAvatar("avatar_sample.jpeg");

        return customer;
    }


    public User initUser(Role role, Clinic clinic){
        Gender gender = random.nextInt(100)%2==0 ? Gender.Nam : Gender.Nữ;

        User user = new User()
                .setName(DataValue.USER_NAME[random.nextInt(DataValue.USER_NAME.length)])
                .setEmail(DataValue.USER_EMAIL[random.nextInt(DataValue.USER_EMAIL.length)])
                .setPassword(PASSWORD)
                .setStatus(Status.ACTIVE)
                .setAddress(DataValue.ADDRESS[random.nextInt(DataValue.ADDRESS.length)])
                .setGender(gender)
                .setBirthDate(LocalDate.now().withYear(1999))
                .setPhoneNumber(DataValue.USER_PHONENUMBER[random.nextInt(DataValue.USER_PHONENUMBER.length)])
                .setCmnd(DataValue.USER_PHONENUMBER[random.nextInt(DataValue.USER_PHONENUMBER.length)])
                .setWorkExperience(random.nextInt(40))
                .setRole(role)
                .setClinic(clinic);
        return user;
    }

    public ProductCategory initProductCategory(int i){
        ProductCategory productCategory = new ProductCategory()
                .setName(DataValue.PRODUCT_CATEGORY_NAME[i])
                .setStatus(Status.ACTIVE)
                .setDescription(DataValue.PRODUCT_CATEGORY_DESCRIPTION[i]);
        return productCategory;
    }

    public Product initProduct(ProductCategory productCategory, Clinic clinic){
        Product product = new Product()
                .setName(DataValue.PRODUCT_NAME[random.nextInt(DataValue.PRODUCT_NAME.length)])
                .setStatus(Status.ACTIVE)
                .setQuantity(random.nextInt(1000))
//                .setQuantity(0)
                .setPrice(new BigDecimal(random.nextInt(1000000000)))
                .setWholesalePrice(new BigDecimal(random.nextInt(1000000000)))
                .setUnit(DataValue.PRODUCT_UNIT[random.nextInt(DataValue.PRODUCT_UNIT.length)])
                .setProductCategory(productCategory);
                //.setClinic(clinic);

        return product;
    }

    public ServiceCategory initTreatmentPackageCategory(int i){
        ServiceCategory serviceCategory = new ServiceCategory()
                .setName(DataValue.TREATMENT_PACKAGE_CATEGORY_NAME[i])
                .setStatus(Status.ACTIVE);
        return serviceCategory;
    }

    public TreatmentPackage initTreatmentPackage(ServiceCategory serviceCategory){
        TreatmentPackage treatmentPackage = new TreatmentPackage()
                .setName(DataValue.TREATMENT_PACKAGE_NAME[random.nextInt(DataValue.TREATMENT_PACKAGE_NAME.length)])
                .setStatus(Status.ACTIVE)
                .setTotal(new BigDecimal(random.nextInt(1000000000)));

        return treatmentPackage;

    }

    public SlipUse initSlipUse(Customer customer, TreatmentPackage treatmentPackage){

        SlipUse slipUse = new SlipUse()
                .setStartDate(LocalDate.now())
                .setEndDate(LocalDate.now().plusDays(90))
                .setStatus(Status.ACTIVE)
                .setImageBefore("before.png")
                .setImageAfter("after.png")
                .setCustomer(customer)
                .setTreatmentPackage(treatmentPackage);

        return slipUse;
    }

    public String generateCouponCode(){
        StringBuilder code = new StringBuilder();
        for (int i=0; i<16; i++){
            code.append(DataValue.ALPHABETS.charAt(random.nextInt(DataValue.ALPHABETS.length())));
        }
        return code.toString();
    }

    public Coupon initCoupon(ServiceCategory serviceCategory){
        int numberRandom = random.nextInt(100);
        Coupon coupon = new Coupon()
                .setName(DataValue.COUPON_NAME[random.nextInt(DataValue.COUPON_NAME.length)])
                .setCode(generateCouponCode())
                .setStartDate(LocalDate.now())
                .setEndDate(LocalDate.now().plusDays(30))
                .setQuantity(random.nextInt(100))
                .setDiscount(numberRandom/100.0)
                .setServiceCategory(serviceCategory);
        return coupon;
    }

    public Order initOrder(Customer customer, User user) {
    	
    	//Status status = random.nextInt(100)%2==0 ? Status.ACTIVE : Status.DEACTIVE;
    	
    	Order order = new Order()
    			.setOrderDate(LocalDateTime.now())
    			.setPaymentMethod(DataValue.PAYMENT_METHOD[random.nextInt(DataValue.PAYMENT_METHOD.length)])
    			.setStatus(OrderStatus.Processing)
                .setDebtMoney(new BigDecimal(random.nextInt(1000000000)))
                .setTotal(new BigDecimal(random.nextInt(1000000000)))
    			.setCustomer(customer)
    			.setUser(user)
                .setType(OrderType.Spa);
    	return order;
    	
    }
    
    public OrderDetailProduct initOrderDetailProduct(Order order, Product product) {   	
    	
		OrderDetailProduct orderDetailProduct = new OrderDetailProduct()
				.setOrder(order)
				.setProduct(product)
				.setQuantity(DataValue.USER_WORKEXPERIENCE[random.nextInt(DataValue.USER_WORKEXPERIENCE.length)])
				.setPrice(product.getPrice());
		
		return orderDetailProduct;
	}

    public OrderDetailTreatmentPackage initOrderDetailTreatmentPackage(Order order, TreatmentPackage treatmentPackage) {

        OrderDetailTreatmentPackage orderDetailTreatmentPackage = new OrderDetailTreatmentPackage()
                .setOrder(order)
                .setTreatmentPackage(treatmentPackage)
                .setQuantity(DataValue.USER_WORKEXPERIENCE[random.nextInt(DataValue.USER_WORKEXPERIENCE.length)])
                .setPrice(treatmentPackage.getTotal());

        return orderDetailTreatmentPackage;
    }

    public OrderDetailService initOrderDetailService(Order order, Service service) {

        OrderDetailService orderDetailService = new OrderDetailService()
                .setOrder(order)
                .setService(service)
                .setQuantity(DataValue.USER_WORKEXPERIENCE[random.nextInt(DataValue.USER_WORKEXPERIENCE.length)])
                .setPrice(service.getPrice());

        return orderDetailService;
    }

    public Order initOrderWithExamination(Customer customer, User user) {

        //Status status = random.nextInt(100)%2==0 ? Status.ACTIVE : Status.DEACTIVE;

        Order order = new Order()
                .setOrderDate(LocalDateTime.now())
                .setPaymentMethod(DataValue.PAYMENT_METHOD[random.nextInt(DataValue.PAYMENT_METHOD.length)])
                .setStatus(OrderStatus.Examining)
                .setDebtMoney(new BigDecimal(random.nextInt(1000000000)))
                .setTotal(new BigDecimal(random.nextInt(1000000000)))
                .setCustomer(customer)
                .setUser(user);
        return order;

    }

	public Supplier initSupplier(){

        Status status = random.nextInt(100)%2==0 ? Status.ACTIVE : Status.DEACTIVE;

        Supplier supplier =  new Supplier()
                .setName(DataValue.PRODUCER_PRODUCT[random.nextInt(DataValue.PRODUCER_PRODUCT.length)])
                .setPhoneNumber(DataValue.USER_PHONENUMBER[random.nextInt(DataValue.USER_PHONENUMBER.length)])
                .setStatus(status);

        return supplier;
    }

//    public WarehouseReceipt initWarehouseReceipt(Supplier supplier, Clinic clinic, User user){
//        Status status = random.nextInt(100)%2==0 ? Status.ACTIVE : Status.DEACTIVE;
//
//        WarehouseReceipt warehouseReceipt = new WarehouseReceipt()
//                .setDateimport(LocalDate.now())
//                .setStatus(status)
//                .setReceiptType(DataValue.RECEIPT_TYPE[random.nextInt(DataValue.RECEIPT_TYPE.length)])
//                //.setTotalPayment(new BigDecimal(random.nextInt(1000000000)))
//                .setSupplier(supplier)
//                .setClinic(clinic)
//                .setUser(user);
//
//        return warehouseReceipt;
//    }

//    public WarehouseReceiptDetail initWarehouseReceiptDetail(WarehouseReceipt warehouseReceipt, Product product){
//        WarehouseReceiptDetail warehouseReceiptDetail = new WarehouseReceiptDetail()
//                .setWarehouseReceipt(warehouseReceipt)
//                .setProduct(product)
//                .setNumberProductImport(random.nextInt(1000000));
//        return warehouseReceiptDetail;
//    }

    public ServiceCategory initServiceCategory(){
        ServiceCategory serviceCategory = new ServiceCategory()
                .setName(DataValue.SERVICE_NAME[random.nextInt(DataValue.SERVICE_NAME.length)])
                .setStatus(Status.ACTIVE);
        return serviceCategory;
    }

    public Service initService(ServiceCategory serviceCategory){
        Service service = new Service()
                .setName(DataValue.SERVICE_NAME[random.nextInt(DataValue.SERVICE_NAME.length)])
                .setPeriod(LocalDate.now())
                .setPrice(new BigDecimal(random.nextInt(1000000000)))
                .setStatus(Status.ACTIVE)
                .setServiceCategory(serviceCategory);
        return service;
    }

    public Advisory initAdvisory(User user, Customer customer){
        Advisory advisory = new Advisory()
                .setCustomer(customer)
                .setCreatedDate(LocalDateTime.now())
                .setUser(user)
                .setContent(DataValue.ADVISORY[random.nextInt(DataValue.ADVISORY.length)]);
        return advisory;
    }

    public CustomerCategory initCustomerCategory(int i){
        CustomerCategory customerCategory = new CustomerCategory()
                .setName(DataValue.CUSTOMER_CATEGORY_NAME[i])
                .setDescription(DataValue.CUSTOMER_CATEGORY_DESCRIPTION[i]);
        return customerCategory;
    }

}
