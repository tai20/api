package com.sharework.health.repository;

import com.sharework.health.entity.Bill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BillRepository extends JpaRepository<Bill, Integer> {
    @Query(nativeQuery = true,value = "select * from bill where order_id= :orderId")
    List<Bill> findByOrderId (Integer orderId);
}
