package com.sharework.health.repository;

import com.sharework.health.entity.CategoryIndicationCard;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryIndicationCardRepository extends JpaRepository<CategoryIndicationCard, Integer> {

	  @Query(nativeQuery = true, value = "select p.* from categoryindicationcard p join subcategoryindicationcard c on p.subcategoryindicationcard_id = c.id where c.name= :subCategoryIndicationCardName")
	  List<CategoryIndicationCard> findAllBySubCategoryIndicationCardName(String subCategoryIndicationCardName);
}
