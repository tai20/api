package com.sharework.health.repository;

import com.sharework.health.entity.Clinic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClinicRepository extends JpaRepository<Clinic, Integer> {

    Clinic findByName(String name);

    List<Clinic> findByNameContaining(String name);


}
