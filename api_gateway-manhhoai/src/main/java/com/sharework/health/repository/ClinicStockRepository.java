package com.sharework.health.repository;


import com.sharework.health.entity.ClinicStock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClinicStockRepository extends JpaRepository<ClinicStock, Integer> {

    @Query(nativeQuery = true, value="select  * from clinicstock where clinic_id= :clinic_id")
    List<ClinicStock> getClinicStockByClinicId(Integer clinic_id);

    @Query(nativeQuery = true, value="select  * from clinicstock where product_id= :product_id")
    List<ClinicStock> getClinicStockByProductId(Integer product_id);

    //Kiểm tra xem sp đó có ở phiếu hay không

    @Query(nativeQuery = true, value="select cs.* from clinicstock cs join importreceiptdetails ON importreceiptdetails.clinic_stock_id = cs.id where cs.id= :clinic_stock_id")
    List<ClinicStock> getClinicStockFromImportReceipt(Integer clinic_stock_id);


    @Query(nativeQuery = true, value="select cs.* from clinicstock cs join exportreceiptdetails ON exportreceiptdetails.clinic_stock_id = cs.id where cs.id= :clinic_stock_id")
    List<ClinicStock> getClinicStockFromExportReceipt(Integer clinic_stock_id);


    @Query(nativeQuery = true, value="select cs.* from clinicstock cs join transferreceiptdetails ON transferreceiptdetails.clinic_stock_id = cs.id where cs.id= :clinic_stock_id")
    List<ClinicStock> getClinicStockFromTransferReceipt(Integer clinic_stock_id);

    @Query(nativeQuery = true, value="select cs.* from clinicstock cs join otherswarehousereceiptdetails ON otherswarehousereceiptdetails.clinic_stock_id = cs.id where cs.id= :clinic_stock_id")
    List<ClinicStock> getClinicStockFromOtherReceipt(Integer clinic_stock_id);


    //Tính tổng sản phẩm ở thời điểm hiện tại
    @Query("select sum(cs.quantity) from ClinicStock cs " +
            "where cs.product.id= :product_id and (:clinic_id is null or cs.clinic.id=: clinic_id)")
    Integer stockPresentByProductId(Integer product_id, Integer clinic_id);

}
