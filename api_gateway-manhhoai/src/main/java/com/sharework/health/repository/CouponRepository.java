package com.sharework.health.repository;

import com.sharework.health.entity.Coupon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CouponRepository extends JpaRepository<Coupon, Integer> {

    Coupon findByName(String name);

    Coupon findByCode(String code);

    @Query(value = "select * from customer c join customer_coupon cc on c.id = cc.customer_id where c.id= :customerId",
        nativeQuery = true)
    List<Coupon> findByCustomer(Integer customerId);

    List<Coupon> findByNameContains(String name);
}
