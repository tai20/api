package com.sharework.health.repository;

import com.sharework.health.entity.CustomerCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerCategoryRepository extends JpaRepository<CustomerCategory, Integer> {

    CustomerCategory findByName(String name);
}
