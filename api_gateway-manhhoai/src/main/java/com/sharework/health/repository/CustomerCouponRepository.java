package com.sharework.health.repository;


import com.sharework.health.entity.CustomerCoupon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerCouponRepository extends JpaRepository<CustomerCoupon, Integer> {

    List<CustomerCoupon> findAllByCustomerId(Integer customerId);

    List<CustomerCoupon> findAllByCouponId(Integer couponId);
}
