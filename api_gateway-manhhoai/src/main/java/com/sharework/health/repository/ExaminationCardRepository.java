package com.sharework.health.repository;

import com.sharework.health.dto.*;
import com.sharework.health.entity.ExaminationCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ExaminationCardRepository extends JpaRepository<ExaminationCard, Integer> {



    @Query(nativeQuery = true, value = "select * from examinationcard where dateofexamination between :startDate and :endDate")
    List<ExaminationCard> statisticalExaminationCardByDateOfExaminationIsBetween(LocalDateTime startDate, LocalDateTime endDate);

//    @Query(nativeQuery = true, value = "select e.* from examinationcarddetail e join users u on e.user_id = u.id where e.status like 'Examining' and u.id= :userId")
//    List<ExaminationCardDetail> findAllByUser(Integer userId);

//    @Query("SELECT new com.sharework.com.sharework.health.dto.ExaminationCardQueryDto(e.id, e.dateOfExamination, c.name) from ExaminationCard e join User u on e.user.id = u.id " +
//            "join SlipUse s on e.slipUse.id = s.id join Customer c on s.customer.id = c.id where e.status = 'Examining' and u.id= :userId")
//    List<ExaminationCardQueryDto> findAllByUser(Integer userId);

    @Query(nativeQuery = true, value = "select e.* from examinationcard e join slipuse s on e.slipuse_id = s.id where s.id= :slipUseId")
    List<ExaminationCard> findAllBySlipUse(Integer slipUseId);

    //test
    @Query(nativeQuery = true, value = "Select e.* from examinationcard e join slipuse s  on e.slipuse_id = s.id join examinationcarddetail ed on e.id = ed.examinationcard_id where s.customer_id= :customerId AND ed.status like 'Examined'")
    List<ExaminationCard> findAllByCustomerId(Integer customerId);

    @Query(nativeQuery = true, value = "select * from examinationcard order by id desc limit 1")
    ExaminationCard getExaminationCardAfterInsert();

//    @Query("SELECT new com.sharework.com.sharework.health.dto.ExaminationCardDetailQueryDto(e.id, e.dateOfExamination, u.name, c.name) from ExaminationCard e join ExaminationCardDetail ed on ed.examinationCard.id = e.id join User u on ed.user.id = u.id " +
//            "join SlipUse s on e.slipUse.id = s.id join Customer c on s.customer.id = c.id")
//    List<ExaminationCardDetailQueryDto> findAllExaminationCardByUserAndCustomer();

    @Query("select new com.sharework.health.dto.ExaminationCardCustomerQueryDto(e.id, e.dateOfExamination,e.status, s.id, c.name, c.phoneNumber, e.signature) from ExaminationCard e join SlipUse s on e.slipUse.id = s.id join Customer c on c.id = s.customer.id")
    List<ExaminationCardCustomerQueryDto> getExaminationCardWithCustomer();

//    @Query("SELECT distinct new com.sharework.com.sharework.health.dto.SelectCountExaminationCustomerDto(c.id, count(e.id)) from ExaminationCard e join SlipUse s on e.slipUse.id = s.id join Customer c on s.customer.id = c.id " +
//            " where MONTH(e.dateOfExamination) = :month and YEAR(e.dateOfExamination) = :year  group by c.id having count(e.id) > 0")
//    List<SelectCountExaminationCustomerDto> findAllCustomerAndCountExaminationByMonthAndYear(Integer month, Integer year);

    @Query(nativeQuery = true, value = "select c.id as customerid, c.name as customername, c.phonenumber, c.address,cl.id as clinicid,cl.name as clinicname, count(e.id)\n" +
            "from examinationcard e join slipuse s on e.slipuse_id = s.id join customer c on s.customer_id = c.id\n" +
            "join examinationcarddetail ed on ed.examinationcard_id = e.id\n" +
            "join users u on u.id = ed.user_id\n" +
            "join clinic cl on u.clinic_id = cl.id\n" +
            "where extract(MONTH from e.dateOfExamination) = :month and extract(YEAR from e.dateOfExamination) = :year  \n" +
            "group by c.id,cl.id \n" +
            "having count(e.id) > 0")
    List<?> findAllCustomerAndCountExaminationByMonthAndYear(Integer month, Integer year);

    @Query(nativeQuery = true, value = "select ed.user_id, u.name as username, c.id, c.name as clinicname, ed.service_id, s.name as servicename,cu.id as customerid, cu.name as customername, count(ed.examinationcard_id)\n" +
        "from examinationcarddetail ed join users u on ed.user_id = u.id join role r on u.role_id = r.id join examinationcard e on e.id = ed.examinationcard_id\n" +
        "join slipuse sl on e.slipuse_id=sl.id join customer cu on sl.customer_id=cu.id \n"+
        "join service s on ed.service_id = s.id join clinic c on c.id = u.clinic_id\n" +
        "where extract(MONTH from e.dateofexamination)= :month AND extract(YEAR from e.dateofexamination)= :year AND r.name like 'ROLE_DOCTOR' and ed.status like 'Examined'\n" +
        "group by ed.user_id, u.name, c.id, c.name, ed.service_id, s.name, cu.id, cu.name\n" +
        "having count(ed.examinationcard_id) > 0")
    List<?> statisticalCountExaminationOfDoctorByMonthAndYear(@Param("month") Integer month,@Param("year") Integer year);

    @Query(nativeQuery = true, value = "select ed.user_id, u.name as username, c.id, c.name as clinicname, ed.service_id, s.name as servicename,cu.id as customerid, cu.name as customername, count(ed.examinationcard_id)\n" +
            "from examinationcarddetail ed join users u on ed.user_id = u.id join role r on u.role_id = r.id join examinationcard e on e.id = ed.examinationcard_id\n" +
            "join slipuse sl on e.slipuse_id=sl.id join customer cu on sl.customer_id=cu.id \n"+
            "join service s on ed.service_id = s.id join clinic c on c.id = u.clinic_id\n" +
            "where extract(MONTH from e.dateofexamination)= :month AND extract(YEAR from e.dateofexamination)= :year AND r.name like 'ROLE_TECHNICIAN' and ed.status like 'Examined'\n" +
            "group by ed.user_id, u.name, c.id, c.name, ed.service_id, s.name, cu.id, cu.name\n" +
            "having count(ed.examinationcard_id) > 0")
    List<?> statisticalCountExaminationOfTechnicianByMonthAndYear(@Param("month") Integer month,@Param("year") Integer year);

    @Query(nativeQuery = true, value = "select ed.service_id, s.name as serviceName, c.id, c.name as clinicName, count(ed.examinationCard_id)\n" +
            "from examinationcarddetail ed join users u on ed.user_id = u.id join role r on u.role_id = r.id join examinationcard e on e.id = ed.examinationCard_id\n" +
            "join service s on ed.service_id = s.id join clinic c on c.id = u.clinic_id\n" +
            "where extract(MONTH from e.dateofexamination)= :month AND extract(YEAR from e.dateofexamination)= :year AND c.id= :clinicId AND ed.status like 'Examined'\n" +
            "group by ed.service_id, s.name, c.id, c.name\n" +
            "having count(ed.examinationCard_id) > 0")
    List<?> statisticalCountExaminationOfServiceByMonthAndYearAndClinic(@Param("month") Integer month,@Param("year") Integer year,@Param("clinicId") Integer clinicId);


    @Query(nativeQuery = true, value = "select ed.service_id, psd.product_id, p.name as productname, p.unit, c.id, c.name as clinicname, sum(psd.quantity)\n" +
            "from examinationcard e join examinationcarddetail ed on e.id = ed.examinationcard_id join service s on ed.service_id = s.id\n" +
            "join productservicedetail psd on s.id = psd.service_id join product p on p.id = psd.product_id join clinic c on c.id = p.clinic_id\n" +
            "where extract(MONTH from e.dateofexamination)= :month AND extract(YEAR from e.dateofexamination)= :year AND ed.status like 'Examined'\n" +
            "group by ed.service_id, psd.product_id, p.name, c.id, p.unit, c.name\n" +
            "having sum(psd.quantity) > 0")
    List<?> statisticalCountConsumableProductByMonthAndYear(@Param("month") Integer month,@Param("year") Integer year);


    @Query(nativeQuery = true, value = "select ed.service_id, s.name as service_name, cl.id, cl.name as clinicname, count(c.id)\n" +
            "from examinationcard e join examinationcarddetail ed on e.id = ed.examinationcard_id join service s on ed.service_id = s.id\n" +
            "join slipuse su on su.id = e.slipuse_id join customer c on c.id = su.customer_id join users u on ed.user_id = u.id join clinic cl on cl.id = u.clinic_id\n" +
            "where extract(MONTH from e.dateofexamination)= :month AND extract(YEAR from e.dateofexamination)= :year AND ed.status like 'Examined'\n" +
            "group by ed.service_id, s.name, cl.id, cl.name\n" +
            "having count(c.id) > 0")
    List<?> statisticalCountCustomerOfServiceByMonthAndYear(@Param("month") Integer month,@Param("year") Integer year);

    @Query(nativeQuery = true, value = "select c.id as customer_id, c.name as customer_name, ed.service_id, s.name as service_name, cl.id as clinic_id, cl.name as clinicname, count(ed.service_id)\n" +
            "from examinationcard e join examinationcarddetail ed on e.id = ed.examinationcard_id join service s on ed.service_id = s.id\n" +
            "join slipuse su on su.id = e.slipuse_id join customer c on c.id = su.customer_id join users u on ed.user_id = u.id join clinic cl on cl.id = u.clinic_id\n" +
            "where extract(MONTH from e.dateofexamination)= :month AND extract(YEAR from e.dateofexamination)= :year AND ed.status like 'Examined'\n" +
            "group by c.id, c.name, ed.service_id, s.name, cl.id, cl.name\n" +
            "having count(ed.service_id) > 0")
    List<?> statisticalCountServiceExaminationOfCustomerByMonthAndYear(@Param("month") Integer month,@Param("year") Integer year);


}
