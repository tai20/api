package com.sharework.health.repository;

import com.sharework.health.entity.FreeTrial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FreeTrialRepository extends JpaRepository<FreeTrial, Integer> {
}
