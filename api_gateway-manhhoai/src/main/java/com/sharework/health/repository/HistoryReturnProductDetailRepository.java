package com.sharework.health.repository;

import com.sharework.health.entity.HistoryReturnProductDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HistoryReturnProductDetailRepository extends JpaRepository<HistoryReturnProductDetail, Integer> {

    @Query(nativeQuery = true, value = "select * from historyreturnproductdetail where historyreturnproduct_id= :historyReturnProductId")
    List<HistoryReturnProductDetail> findAllByHistoryReturnProductId(Integer historyReturnProductId);
}
