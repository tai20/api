package com.sharework.health.repository;

import com.sharework.health.dto.ImportReceiptDetailsQueryDto;
import com.sharework.health.entity.ImportReceiptDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImportReceiptDetailsRepository extends JpaRepository<ImportReceiptDetails, Integer> {

    @Query("select new com.sharework.health.dto.ImportReceiptDetailsQueryDto(ird.importReceipt.id, ird.clinicStock.id, ird.number_product_import, cs.product.id, p.name, ird.import_price) " +
            "from ImportReceiptDetails ird join ClinicStock cs on cs.id=ird.clinicStock.id " +
            "join Product p on p.id=cs.product.id where ird.importReceipt.id= :receipt_id")
    List<ImportReceiptDetailsQueryDto> getAllImportReceiptDetailByReceiptid(Integer receipt_id);

    @Query(nativeQuery = true, value = "select ird.* from importreceiptdetails ird " +
            "where ird.receipt_id= :receipt_id and ird.clinic_stock_id= :clinic_stock_id")
    List<ImportReceiptDetails> getImportReceiptByClinicStockIdReceiptId(Integer receipt_id, Integer clinic_stock_id);

}
