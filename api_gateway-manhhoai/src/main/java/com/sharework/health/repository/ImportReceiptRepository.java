package com.sharework.health.repository;

import com.sharework.health.entity.ImportReceipt;
import com.sharework.health.entity.ImportReceiptDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface ImportReceiptRepository extends JpaRepository<ImportReceipt, Integer>{
    @Query(value = "select sum(importreceiptdetails.number_product_import*importreceiptdetails.import_price) from importreceiptdetails where importreceiptdetails.receipt_id= :receipt_id", nativeQuery = true)
    BigDecimal getSumPrice(Integer receipt_id);

    @Query(nativeQuery = true, value = "select ir.* from importreceipt ir order by ir.dateimport DESC")
    List<ImportReceipt> findAllSortDateDESC();
}
