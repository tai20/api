package com.sharework.health.repository;

import com.sharework.health.entity.IndicationCardDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IndicationCardDetailRepository extends JpaRepository<IndicationCardDetail, Integer> {

    @Query(nativeQuery = true, value = "select id.* \n" +
            "from indicationcard i join indicationcarddetail id on i.id= id.indicationcard_id\n" +
            "join categoryindicationcard ci on ci.id = id.categoryindicationcard_id\n" +
            "where i.id= :indicationCardId and ci.id= :categoryIndicationId")
    IndicationCardDetail getIndicationCardDetailByIndicationCardAndCategoryIndicationCard(Integer indicationCardId, Integer categoryIndicationId);

    @Query(nativeQuery = true, value = "select id.* from indicationcard i join indicationcarddetail id on i.id = id.indicationcard_id where i.id= :indicationCardId")
    List<IndicationCardDetail> getIndicationCardDetailByIndicationCard(Integer indicationCardId);

    @Query(nativeQuery = true, value = "select i.* from indicationcarddetail i join medicalcard c on i.indicationcard_id = c.indicationcard_id where c.id= :medicalCardId")
    List<IndicationCardDetail> getIndicationCardDetailByMedicalCardId(Integer medicalCardId);
}
