package com.sharework.health.repository;

import com.sharework.health.dto.MedicalCardQueryDto;
import com.sharework.health.entity.MedicalCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicalCardRepository extends JpaRepository<MedicalCard, Integer> {

    @Query(nativeQuery = true, value = "select m.* from medicalcard m order by m.id desc limit 1 ")
    MedicalCard getMedicalCardAfterInsert();

    @Query(nativeQuery = true, value = "select m.* from medicalcard m join scheduleexamination s on m.id = s.medicalcard_id where s.id= :scheduleExaminationId ")
    MedicalCard getMedicalCardByScheduleExaminationId(Integer scheduleExaminationId);

    @Query(nativeQuery = true, value = "select m.* from medicalcard m join scheduleexamination s ON m.id = s.medicalcard_id where s.customer_id= :customerId")
    List<MedicalCard> getMedicalCardByCustomerId(Integer customerId);

    @Query(nativeQuery = true, value = "select m.* from medicalcard m join scheduleexamination s ON m.id = s.medicalcard_id where s.customer_id= :customerId order by m.id desc limit 1 ")
    MedicalCard getMedicalCardLatelyByCustomerId(Integer customerId);

    /*@Query("select new com.sharework.health.dto.MedicalCardQueryDto(m.id, m.diagnose, m.differentDiagnose, m.management, m.nextAppointment, " +
            "m.note, m.advice, c.id, c.name, u.id, u.name, s.id, s.dateOfExamination, g.packagetest_id, i.testservice_id) from MedicalCard m join ScheduleExamination s on s.id = m.scheduleExamination.id join " +
            "User u on u.id = s.user.id join Customer c on c.id = s.customer.id join PackageTest g on g.packagetest_id = m.packageTest.packagetest_id " +
            "join TestService i on m.testService.testservice_id = i.testservice_id")*/
    /*List<MedicalCardQueryDto> getAllMedicalCard();*/
}
