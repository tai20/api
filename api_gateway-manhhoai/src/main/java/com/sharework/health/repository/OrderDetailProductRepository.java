package com.sharework.health.repository;

import com.sharework.health.entity.OrderDetailProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderDetailProductRepository extends JpaRepository<OrderDetailProduct, Integer> {

    @Query(nativeQuery = true, value = "select od.* from orderdetailproduct od join orders o on od.order_id = o.id where o.id= :id")
    List<OrderDetailProduct> findByOrder(Integer id);

    @Query(nativeQuery = true, value = "select product_id, sum(quantity)\n" +
            "from orders o join orderdetailproduct od on o.id = od.order_id\n" +
            "where extract(month from o.orderdate) = :month and extract(year from o.orderdate) = :year\n" +
            "group by product_id")
    List<Object[]> CountProductSoldByMonth(Integer month, Integer year);
}
