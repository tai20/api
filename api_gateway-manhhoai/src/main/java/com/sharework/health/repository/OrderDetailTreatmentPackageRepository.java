package com.sharework.health.repository;

import com.sharework.health.entity.OrderDetailTreatmentPackage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDetailTreatmentPackageRepository extends JpaRepository<OrderDetailTreatmentPackage, Integer> {

    @Query(nativeQuery = true, value = "select od.* from orderdetailtreatmentpackage od join orders o on od.order_id = o.id where o.id= :orderId")
    List<OrderDetailTreatmentPackage> findAllByOrder(Integer orderId);
}
