package com.sharework.health.repository;

import com.sharework.health.entity.OthersWarehouseReceipt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OthersWarehouseReceiptRepository extends JpaRepository<OthersWarehouseReceipt, Integer> {

    @Query(nativeQuery = true, value = "select owr.* from otherswarehousereceipt owr order by owr.datecreate DESC")
    List<OthersWarehouseReceipt> findAllSortDateDESC();
}
