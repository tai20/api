package com.sharework.health.repository;

import com.sharework.health.entity.MedicalCard;
import com.sharework.health.entity.PackageTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PackageTestRepository extends JpaRepository<PackageTest, Integer> {
    @Query(nativeQuery = true, value = "select m.* from packagetest m order by m.packagetest_id desc limit 1 ")
    PackageTest getPackageTestAfterInsert();
}
