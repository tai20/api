package com.sharework.health.repository;

import com.sharework.health.entity.PackageTestServiceDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PackageTestServiceDetailRepository extends JpaRepository<PackageTestServiceDetail, Integer> {
    @Query(nativeQuery = true, value = "select a.* from packagetestservicedetail a join packagetest c on a.packagetest_id = c.packagetest_id where c.packagetest_id= :packagetest_id")
    List<PackageTestServiceDetail> getPackageTestDetalByPackageId(Integer packagetest_id);
}
