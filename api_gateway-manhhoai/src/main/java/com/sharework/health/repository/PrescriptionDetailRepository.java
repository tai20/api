package com.sharework.health.repository;

import com.sharework.health.entity.IndicationCardDetail;
import com.sharework.health.entity.PrescriptionDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PrescriptionDetailRepository extends JpaRepository<PrescriptionDetail, Integer> {
    @Query(nativeQuery = true, value = "select i.* from prescriptiondetail i join medicalcard c on i.prescription_id = c.prescription_id where c.id= :medicalCardId")
    List<PrescriptionDetail> getPrescriptionDetailByMedicalCard(Integer medicalCardId);
}
