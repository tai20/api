package com.sharework.health.repository;

import com.sharework.health.entity.ServiceCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceCategoryRepository extends JpaRepository<ServiceCategory, Integer> {

    ServiceCategory findByName(String name);

    @Query(nativeQuery = true, value = "select * from servicecategory where status like 'ACTIVE'")
    List<ServiceCategory> findAllByStatusLikeActive();
}
