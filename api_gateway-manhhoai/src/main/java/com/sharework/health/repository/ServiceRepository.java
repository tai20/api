package com.sharework.health.repository;

import com.sharework.health.entity.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceRepository extends JpaRepository<Service, Integer> {

    @Query(nativeQuery = true, value = "select * from service where status like 'ACTIVE'")
    List<Service> findALlByStatusLike();

    @Query(nativeQuery = true, value = "select * from service order by id desc limit 1")
    Service findServiceAfterInsert();

    @Query(nativeQuery = true, value = "select s.* from service s " +
        "join orderdetailservice ods on s.id = ods.service_id join orders o on o.id = ods.order_id join customer c on c.id = o.customer_id " +
        "where c.id= :customerId")
    List<Service> findAllByCustomerId(Integer customerId);

}
