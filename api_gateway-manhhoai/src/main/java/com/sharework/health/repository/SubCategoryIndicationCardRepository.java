package com.sharework.health.repository;

import com.sharework.health.entity.SubCategoryIndicationCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubCategoryIndicationCardRepository extends JpaRepository<SubCategoryIndicationCard, Integer> {
}
