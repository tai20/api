package com.sharework.health.repository;

import com.sharework.health.entity.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SupplierRepository extends JpaRepository<Supplier, Integer> {

    List<Supplier> findAllByName(String name);

    @Query(nativeQuery = true, value = "select * from supplier where type like 'Supplier'")
    List<Supplier> findAllByStatusLikeSupplier();

    @Query(nativeQuery = true, value = "select * from supplier where type like 'Clinic'")
    List<Supplier> findAllByStatusLikeClinic();
}
