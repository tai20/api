package com.sharework.health.repository;

import com.sharework.health.entity.TestService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestServiceRepository extends JpaRepository<TestService, Integer> {
}
