package com.sharework.health.repository;

import com.sharework.health.entity.TotalCost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TotalCostRepository extends JpaRepository<TotalCost, Integer> {
    TotalCost findById(String id);

    @Query(nativeQuery = true, value = "select m.* from totalcost m order by m.id desc limit 1 ")
    TotalCost getTotalCostAfterInsert();
}
