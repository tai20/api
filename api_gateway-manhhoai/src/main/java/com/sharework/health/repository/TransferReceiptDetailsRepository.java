package com.sharework.health.repository;

import com.sharework.health.dto.ClinicStockIdQueryDto;
import com.sharework.health.dto.GetReceivingClinicStockDto;
import com.sharework.health.dto.TransferReceiptDetailsQueryDto;
import com.sharework.health.entity.ClinicStock;
import com.sharework.health.entity.TransferReceiptDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransferReceiptDetailsRepository extends JpaRepository<TransferReceiptDetails, Integer> {

    @Query("select new com.sharework.health.dto.GetReceivingClinicStockDto(tr.clinic_receiving.id, cs.product.id, trd.number_product_transfer) " +
            "from TransferReceiptDetails trd join TransferReceipt tr on trd.transferReceipt.id=tr.id " +
            "join ClinicStock cs on trd.clinicStock.id=cs.id where trd.transferReceipt.id=:receipt_id")
    List<GetReceivingClinicStockDto> GetClinicStockReceivingId(Integer receipt_id);


    @Query("select new com.sharework.health.dto.ClinicStockIdQueryDto(cs.id) from ClinicStock cs where cs.product.id= :product_id and cs.clinic.id= :clinic_id")
    ClinicStockIdQueryDto getClinicStockByProductClinicId(Integer product_id, Integer clinic_id);


    @Query("select new com.sharework.health.dto.TransferReceiptDetailsQueryDto(trd.transferReceipt.id, trd.clinicStock.id, trd.number_product_transfer, cs.product.id, p.name)" +
            "from TransferReceiptDetails  trd join ClinicStock cs on cs.id=trd.clinicStock.id " +
            "join Product p on p.id=cs.product.id where trd.transferReceipt.id= :receipt_id")
    List<TransferReceiptDetailsQueryDto> getAllTransferReceiptDetailByReceiptid(Integer receipt_id);


    @Query(nativeQuery = true, value = "select trd.* from transferreceiptdetails trd" +
            " where trd.receipt_id= :receipt_id and trd.clinic_stock_id= :clinic_stock_id")
    List<TransferReceiptDetails> getTransferReceiptDetailsByClinicStockIdReceiptId(Integer receipt_id, Integer clinic_stock_id);
}
