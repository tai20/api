package com.sharework.health.repository;

import com.sharework.health.entity.TransferReceipt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransferReceiptRepository extends JpaRepository<TransferReceipt, Integer> {

    @Query(nativeQuery = true, value = "select tr.* from transferreceipt tr order by tr.dateexport DESC")
    List<TransferReceipt> findAllSortDateDESC();
}
