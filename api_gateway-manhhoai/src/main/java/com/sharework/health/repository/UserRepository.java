package com.sharework.health.repository;

import com.sharework.health.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    User findByEmail(String email);

    List<User> findUserByNameContains(String name);

    User findByPhoneNumber(String phoneNumber);

    User findByCmnd(String cmnd);

    @Query(nativeQuery = true, value = "select * from users where status like 'ACTIVE'")
    List<User> findAllByStatusLikeActive();
}
