package com.sharework.health.repository;

import com.sharework.health.entity.WarehouseReceiptDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WarehouseReceiptDetailRepository extends JpaRepository<WarehouseReceiptDetail, Integer> {

    @Query(nativeQuery = true, value = "select * from warehousereceipt w join warehousereceiptdetail wd on w.id = wd.receipt_id where w.id= :id")
    List<WarehouseReceiptDetail> findAllByWarehouseReceipt(Integer id);
}
