package com.sharework.health.service;

import com.sharework.health.dto.BillDto;
import com.sharework.health.dto.OrderDto;
import com.sharework.health.entity.Bill;

import java.util.List;

public interface BillService extends BaseService<BillDto, Integer>{
    boolean insert(OrderDto dto);

    List<BillDto> findBillByOrder(Integer orderID);
}
