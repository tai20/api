package com.sharework.health.service;

import java.util.List;

import com.sharework.health.dto.CategoryIndicationCardDto;


public interface CategoryIndicationCardService extends BaseService<CategoryIndicationCardDto, Integer> {
	List<CategoryIndicationCardDto> findAllBySubCategoryIndicationCardName(String subCategoryIndicationCard);
}
