package com.sharework.health.service;

import com.sharework.health.dto.ClinicStockDto;

import java.util.List;

public interface ClinicStockService extends BaseService<ClinicStockDto, Integer>{
    public List<ClinicStockDto> getClinicStockByClinicId(Integer id);

}
