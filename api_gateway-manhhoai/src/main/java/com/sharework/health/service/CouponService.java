package com.sharework.health.service;

import com.sharework.health.dto.CouponDto;
import com.sharework.health.dto.CustomerCouponDto;

import java.util.List;

public interface CouponService extends BaseService<CouponDto, Integer> {
    CouponDto searchCouponByName (String name);

    CouponDto checkCouponByCouponCode (String code);

    boolean addCouponForServiceCategory(Integer couponId, Integer serviceCategoryId);

    boolean addCouponForCustomer(Integer couponId, Integer customerId);

    List<CouponDto> getCouponsByCustomer(Integer customerId);

    List<CustomerCouponDto> findAllByCustomer(Integer customerId);

    List<CustomerCouponDto> findAllByCoupon(Integer couponId);
}
