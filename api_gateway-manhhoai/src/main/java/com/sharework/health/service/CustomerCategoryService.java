package com.sharework.health.service;

import com.sharework.health.dto.CustomerCategoryDto;

public interface CustomerCategoryService extends BaseService<CustomerCategoryDto, Integer> {
}
