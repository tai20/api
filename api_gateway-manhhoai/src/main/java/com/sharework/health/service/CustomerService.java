package com.sharework.health.service;

import com.sharework.health.dto.CustomerDto;
import com.sharework.health.dto.PasswordDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public interface CustomerService extends BaseService<CustomerDto, Integer> {
    CustomerDto findByPhoneNumber(String phoneNumber);

    List<CustomerDto> findAllByClinic();

    CustomerDto getProfile();

    String changePassword(PasswordDto dto);

//    boolean insertCustomer(CustomerDto dto, MultipartFile avatar) throws IOException;
  boolean insertCustomer(String name, String email,
                              String birthDate, String gender,
                              String address, String cmnd,
                              String phone, String skinStatus,
                              String customerResource, MultipartFile avatar,
                              Integer clinicId) throws IOException;


    List<CustomerDto> findAllByCustomerCategory(Integer customerCategoryId);

    public boolean updateAvatar(Integer id, MultipartFile avatar) throws IOException;

    boolean insertCustomerLevel(Integer customer_id, Integer level_id, BigDecimal total)throws IOException;
}
