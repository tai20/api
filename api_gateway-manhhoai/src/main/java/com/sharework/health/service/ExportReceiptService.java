package com.sharework.health.service;

import com.sharework.health.dto.ExportReceiptDetailsDto;
import com.sharework.health.dto.ExportReceiptDetailsQueryDto;
import com.sharework.health.dto.ExportReceiptDto;
import com.sharework.health.dto.ImportReceiptDetailsQueryDto;
import com.sharework.health.entity.ExportReceipt;

import java.util.List;

public interface ExportReceiptService extends BaseService<ExportReceiptDto, Integer> {
    public ExportReceiptDto insertExportReceipt(ExportReceiptDto dto);

    public ExportReceiptDetailsDto insertExportReceiptDetail(ExportReceiptDetailsDto dto);

    public List<ExportReceiptDetailsQueryDto> getAllExportReceiptDetailByReceiptid(Integer receipt_id);

    public boolean lockExportReceipt(Integer receipt_id);

    public boolean deleteExportReceipt(Integer id);

    public boolean deleteExportReceiptDetail(Integer receipt_id, Integer clinic_stock_id);

    public ExportReceiptDetailsQueryDto updateExportReceiptDetail(ExportReceiptDetailsQueryDto dto);

    public boolean restoreExportReceipt(Integer id);
}