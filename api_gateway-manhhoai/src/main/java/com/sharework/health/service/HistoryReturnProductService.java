package com.sharework.health.service;

import com.sharework.health.dto.HistoryReturnProductDetailDto;
import com.sharework.health.dto.HistoryReturnProductDto;

import java.util.List;

public interface HistoryReturnProductService extends BaseService<HistoryReturnProductDto, Integer> {

    List<HistoryReturnProductDetailDto> findAllHistoryReturnProductDetail(Integer historyReturnProductId);

    boolean addHistoryReturnProductDetail(HistoryReturnProductDetailDto dto);

    List<HistoryReturnProductDto> findAllByCustomerId(Integer customerId);

    HistoryReturnProductDto findHistoryReturnProductAfterInsert();
}
