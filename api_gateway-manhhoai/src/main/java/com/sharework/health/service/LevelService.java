package com.sharework.health.service;


import com.sharework.health.dto.LevelDto;

public interface LevelService  extends BaseService<LevelDto, Integer>{

}
