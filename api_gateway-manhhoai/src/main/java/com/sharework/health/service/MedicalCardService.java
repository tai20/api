package com.sharework.health.service;

import com.sharework.health.dto.MedicalCardDto;
import com.sharework.health.dto.MedicalCardQueryDto;
import com.sharework.health.entity.MedicalCard;

import java.util.List;

public interface MedicalCardService extends BaseService<MedicalCardDto, Integer> {

    MedicalCardDto getMedicalCardAfterInsert();

    List<MedicalCardQueryDto> getAllMedicalCard();

    MedicalCardDto getMedicalCardByScheduleExaminationId(Integer scheduleExaminationId);

    List<MedicalCardDto> getMedicalCardByCustomerId(Integer customerId);

    MedicalCardDto getMedicalCardLatelyByCustomerId(Integer customerId);

    boolean updateMedicalCardInScheduleExamination(Integer scheduleExaminationId, MedicalCardDto dto);

//    boolean updateMedicalCardByIndicationCardId(Integer medicalCardId, MedicalCardDto dto, Integer indicationCardId);
}
