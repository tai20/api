package com.sharework.health.service;

import com.sharework.health.dto.OrderDetailPackageTestDto;

import java.util.List;

public interface OrderDetailPackageTestService extends BaseService<OrderDetailPackageTestDto, Integer> {
    List<OrderDetailPackageTestDto> getOrderDetailPackageTestByOrderId(Integer customerId);

}
