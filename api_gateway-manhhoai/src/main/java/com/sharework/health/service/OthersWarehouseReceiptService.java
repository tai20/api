package com.sharework.health.service;

import com.sharework.health.dto.OtherReceiptDetailsQueryDto;
import com.sharework.health.dto.OthersWarehouseReceiptDetailsDto;
import com.sharework.health.dto.OthersWarehouseReceiptDto;

import java.util.List;

public interface OthersWarehouseReceiptService extends BaseService<OthersWarehouseReceiptDto, Integer>{

    public OthersWarehouseReceiptDto insertOtherReceipt(OthersWarehouseReceiptDto dto);
    public OthersWarehouseReceiptDetailsDto insertOtherReceiptDetail(OthersWarehouseReceiptDetailsDto dto);
    public List<OtherReceiptDetailsQueryDto> getAllOtherReceiptDetailByReceiptid(Integer receipt_id);
    public boolean deleteOtherReceipt(Integer id);
    public boolean deleteOtherReceiptDetail(Integer receipt_id, Integer clinic_stock_id);
    public OtherReceiptDetailsQueryDto updateOtherReceiptDetail(OtherReceiptDetailsQueryDto dto);
    public boolean restoreOtherReceipt(Integer id);
    public boolean lockOtherReceipt(Integer id);
}
