package com.sharework.health.service;

import com.sharework.health.dto.PackageTestDto;
import com.sharework.health.entity.PackageTest;

public interface PackageTestService extends BaseService<PackageTestDto, Integer> {

    PackageTestDto getPackageTestAfterInsert();

}
