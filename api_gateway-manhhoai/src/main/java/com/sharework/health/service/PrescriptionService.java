package com.sharework.health.service;

import com.sharework.health.dto.MedicalCardDto;
import com.sharework.health.dto.PrescriptionDetailDto;
import com.sharework.health.dto.PrescriptionDto;

import java.util.List;

public interface PrescriptionService extends BaseService<PrescriptionDto, Integer> {

    boolean addPrescriptionDetail(PrescriptionDetailDto dto);

    boolean deletePrescriptionDetail(PrescriptionDetailDto dto);

    boolean updatePrescriptionDetail(Integer prescriptionId, Integer productId,PrescriptionDetailDto dto);


    //boolean insertPrescription(Integer medicalId, PrescriptionDto dto);

    PrescriptionDto updatePrescriptionInMedicalCard(Integer medicalId, PrescriptionDto dto);

    PrescriptionDto getPrescriptionAfterInsert();

    List<PrescriptionDetailDto> getPrescriptionDetailByMedicalCard(Integer medicalId);
}
