package com.sharework.health.service;

import com.sharework.health.dto.ProductCategoryDto;

public interface ProductCategoryService extends BaseService<ProductCategoryDto, Integer> {
    ProductCategoryDto searchCategoryByCategoryName(String name);
}
