package com.sharework.health.service;

import com.sharework.health.dto.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface ProductStatisticsService{
    List<RemainingProductStatisticsQueryDto> remainingProductStatistics();
    List<StatisticsImportedProductsQueryDto> getStatisticsImportedProducts(LocalDateTime from, LocalDateTime to);
    List<StatisticsExportedProductsQueryDto> getStatisticsExportedProducts(LocalDateTime from, LocalDateTime to);
    List<StatisticsOtherProductsQueryDto> getStatisticsOtherProducts(LocalDateTime from, LocalDateTime to);
    List<StatisticsAllProductsQueryDto> getStatisticsAllProducts(LocalDateTime from, LocalDateTime to);
}
