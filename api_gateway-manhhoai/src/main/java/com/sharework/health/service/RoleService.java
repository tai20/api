package com.sharework.health.service;

import com.sharework.health.dto.RoleDto;

public interface RoleService extends BaseService<RoleDto, Integer> {
}
