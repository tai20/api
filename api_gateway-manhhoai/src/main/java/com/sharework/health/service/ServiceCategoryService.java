package com.sharework.health.service;

import com.sharework.health.dto.ServiceCategoryDto;

public interface ServiceCategoryService extends BaseService<ServiceCategoryDto, Integer> {
}
