package com.sharework.health.service;

import com.sharework.health.dto.SlipReturnProductDetailDto;
import com.sharework.health.dto.SlipReturnProductDto;

import java.util.List;

public interface SlipReturnProductService extends BaseService<SlipReturnProductDto, Integer> {

    List<SlipReturnProductDto> findAllSlipReturnProductByCustomer(Integer customerId);

    List<SlipReturnProductDetailDto> findAllSlipReturnProductDetailBySlipReturnProduct(Integer slipReturnProductId);
}
