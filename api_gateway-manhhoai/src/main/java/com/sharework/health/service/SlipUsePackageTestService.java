package com.sharework.health.service;

import com.sharework.health.dto.SlipUsePackageTestDto;

import java.util.List;

public interface SlipUsePackageTestService extends BaseService<SlipUsePackageTestDto, Integer> {
    List<SlipUsePackageTestDto> findAllByCustomer(Integer customerId);

    boolean activeSlipUsePackageTest(Integer slipusepackagetestId);

    boolean updateExtensionTime(Integer slipusepackagetestId, SlipUsePackageTestDto dto);

    SlipUsePackageTestDto getSlipUsePackageTestByMedicalCardId(Integer medicalCardId);

    boolean updateSlipUseAfterAddTotalCost(Integer medicalCardId, SlipUsePackageTestDto dto);
}
