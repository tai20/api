package com.sharework.health.service;

import com.sharework.health.dto.SlipUseDetailDto;
import com.sharework.health.dto.SlipUseDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface SlipUseService extends BaseService<SlipUseDto, Integer> {

    List<SlipUseDto> findAllByCustomer(Integer customerId);

    List<SlipUseDto> findAllByTreatmentPackage(String treatmentPackageName);

    List<SlipUseDetailDto> getAllSlipUseDetailBySlipUse(Integer slipUseId);

    List<SlipUseDetailDto> getAllSlipUserDetailByCustomerId(Integer customerId);

    boolean uploadImageBeforeSlipUse(Integer slipUseId, MultipartFile imageBefore) throws IOException;

    boolean uploadImageAfterSlipUse(Integer slipUseId, MultipartFile imageAfter) throws IOException;

    boolean activeSlipUse(Integer slipUseId);

}
