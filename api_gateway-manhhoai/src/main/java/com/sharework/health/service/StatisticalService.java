package com.sharework.health.service;


import com.sharework.health.dto.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface StatisticalService {

    List<CustomerDto> statisticalCustomerByCreatedDate(LocalDateTime startDate, LocalDateTime endDate);

    List<AdvisoryDto> statisticalAdvisoryByCreatedDate(LocalDateTime startDate, LocalDateTime endDate);

    List<ExaminationCardDto> statisticalExaminationCardByDateOfExamination(LocalDateTime startDate, LocalDateTime endDate);

    List<OrderDto> statisticalOrderByOrderDate(LocalDateTime startDate, LocalDateTime endDate);

    List<SelectCustomerIdDebtMappingDto> statisticalCustomerHaveSumDebtByMonth(Integer month, Integer year);

    List<SelectCustomerIdTotalMappingDto> statisticalCustomerHaveSumTotalByMonth(Integer month, Integer year);

    List<SelectCountExaminationQueryMappingDto> statisticalUserWithRoleDoctorAndCountExaminationByMonthAndYear(Integer month, Integer year);

    List<SelectCountExaminationQueryMappingDto> statisticalUserWithRoleTechnicianAndCountExaminationByMonthAndYear(Integer month, Integer year);

    List<SelectCountExaminationQueryMappingDto> statisticalCustomerAndCountExaminationByMonthAndYear(Integer month, Integer year);

    List<CustomerDto> statisticalCustomerHaveBirthDateByMonth(Integer month);

    //Map<ProductDto, Object[]> statisticalCountProductSoldByMonth(Integer month, Integer year);

    List<CountProductSoldByMonthDto> statisticalCountProductSoldByMonth(Integer month, Integer year);

    List<CountExaminationOfDoctorByMonthAndYearDto> statisticalCountExaminationOfDoctorByMonthAndYear(Integer month, Integer year);

    List<CountExaminationOfTechnicianByMonthAndYearDto> statisticalCountExaminationOfTechnicianByMonthAndYear(Integer month, Integer year);

    List<CountExaminationOfServiceByClinicDto> statisticalCountExaminationOfServiceByMonthAndYearAndClinic(Integer month, Integer year, Integer clinicId);

    List<CountConsumableProductByMonthAndYearDto> statisticalCountConsumableProductByMonthAndYear(Integer month, Integer year);

    List<CountCustomerOfServiceByMonthAndYearDto> statisticalCountCustomerOfServiceByMonthAndYear(Integer month, Integer year);

    List<CountServiceExaminationOfCustomerByMonthAndYearDto> statisticalCountServiceExaminationOfCustomerByMonthAndYear(Integer month, Integer year);

    List<ProductDto> findAllProductImportFromTo(LocalDate from, LocalDate to);

    List<ProductDto> findAllProductExportFromTo(LocalDate from, LocalDate to);
}
