package com.sharework.health.service;

import com.sharework.health.dto.SubCategoryIndicationCardDto;

public interface SubCategoryIndicationCardService extends BaseService<SubCategoryIndicationCardDto, Integer> {
}
