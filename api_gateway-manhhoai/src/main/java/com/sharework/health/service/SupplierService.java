package com.sharework.health.service;

import com.sharework.health.dto.SupplierDto;

import java.util.List;

public interface SupplierService extends BaseService<SupplierDto, Integer> {

    List<SupplierDto> findAllByStatusLikeSupplier();

    List<SupplierDto> findAllByStatusLikeClinic();

    List<SupplierDto> findAllActive();

}
