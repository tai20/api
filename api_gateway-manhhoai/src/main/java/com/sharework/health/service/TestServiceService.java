package com.sharework.health.service;

import com.sharework.health.dto.TestServiceDto;
import com.sharework.health.dto.TotalCostDto;

public interface TestServiceService extends BaseService<TestServiceDto, Integer> {
    TestServiceDto searchTestServiceById(String id);
}
