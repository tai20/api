package com.sharework.health.service;

import com.sharework.health.dto.WarehouseReceiptDetailDto;
import com.sharework.health.dto.WarehouseReceiptDto;

import java.time.LocalDate;
import java.util.List;

public interface WarehouseReceiptService extends BaseService<WarehouseReceiptDto, Integer> {

    boolean addWarehouseReceiptDetail(Integer id, WarehouseReceiptDetailDto dto);

    List<WarehouseReceiptDto> searchWarehouseReceiptByReceiptType(String receiptType);
    
    List<WarehouseReceiptDto> searchWarehouseReceiptByDateImport(LocalDate dateImport);
    
    List<WarehouseReceiptDto> searchWarehouseReceiptByClinic(Integer clinicId);

    List<WarehouseReceiptDetailDto> findAllByWarehouseReceipt(Integer receiptId);

    WarehouseReceiptDto findWarehouseReceiptAfterInsert();
}
