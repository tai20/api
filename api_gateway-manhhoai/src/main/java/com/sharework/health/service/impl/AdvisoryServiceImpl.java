package com.sharework.health.service.impl;

import com.sharework.health.convert.AdvisoryConvert;
import com.sharework.health.dto.AdvisoryDto;
import com.sharework.health.entity.Advisory;
import com.sharework.health.repository.AdvisoryRepository;
import com.sharework.health.service.AdvisoryService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class AdvisoryServiceImpl implements AdvisoryService {

    private AdvisoryRepository advisoryRepository;

    private AdvisoryConvert advisoryConvert;

    @Override
    public List<AdvisoryDto> findAll() {
        List<Advisory> advisories = advisoryRepository.findAll();
        List<AdvisoryDto> advisoryDtos = new ArrayList<>();
        for (Advisory entity: advisories
             ) {
            AdvisoryDto dto = advisoryConvert.entityToDto(entity);
            advisoryDtos.add(dto);
        }
        return advisoryDtos;
    }

    @Override
    public AdvisoryDto findById(Integer id) {
        return null;
    }

    @Override
    public boolean insert(AdvisoryDto dto) {
        if (dto == null){
            return false;
        }
        Advisory entity = advisoryConvert.dtoToEntity(dto);
        entity.setId(null);
        entity.setCreatedDate(LocalDateTime.now());
        advisoryRepository.save(entity);
        return true;
    }

    @Override
    public boolean update(Integer id, AdvisoryDto dto) {
        Advisory oldAdvisory = advisoryRepository.findById(id).get();
        if (oldAdvisory == null){
            return false;
        }
        if (dto == null){
            return false;
        }
        Advisory newAdvisory = new Advisory();
        newAdvisory.setContent(dto.getContent());
        advisoryRepository.save(newAdvisory);
        return true;
    }

    @Override
    public boolean delete(Integer id) {
        Advisory entity = advisoryRepository.findById(id).get();
        if (entity == null){
            return false;
        }
        advisoryRepository.deleteById(id);
        return true;
    }

    @Override
    public List<AdvisoryDto> findAllByCustomer(Integer customerId) {
        List<Advisory> advisories = advisoryRepository.findAllByCustomer(customerId);
        List<AdvisoryDto> advisoryDtos = new ArrayList<>();
        for (Advisory entity: advisories
        ) {
            AdvisoryDto dto = advisoryConvert.entityToDto(entity);
            advisoryDtos.add(dto);
        }
        return advisoryDtos;
    }
}
