package com.sharework.health.service.impl;


import com.sharework.health.convert.ClinicStockConvert;
import com.sharework.health.dto.ClinicStockDto;
import com.sharework.health.dto.ProductDto;
import com.sharework.health.entity.ClinicStock;
import com.sharework.health.entity.Product;
import com.sharework.health.entity.Clinic;
import com.sharework.health.entity.Status;
import com.sharework.health.repository.ClinicRepository;
import com.sharework.health.repository.ClinicStockRepository;
import com.sharework.health.service.ClinicStockService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class ClinicStockServiceImpl implements ClinicStockService {

    private ClinicStockRepository clinicStockRepository;

    private ClinicStockConvert clinicStockConvert;

    private ClinicRepository clinicRepository;

    @Override
    public List<ClinicStockDto> findAll() {
        List<ClinicStock> clinicStocks = clinicStockRepository.findAll();
        List<ClinicStockDto> clinicStockDtos = new ArrayList<>();
        for (ClinicStock entity: clinicStocks
        ) {
            if (entity.getProduct().getStatus() == Status.ACTIVE){
                ClinicStockDto dto = clinicStockConvert.entityToDto(entity);
                clinicStockDtos.add(dto);
            }

        }
        return clinicStockDtos;
    }

    @Override
    public ClinicStockDto findById(Integer id) {
        ClinicStock entity = clinicStockRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return clinicStockConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(ClinicStockDto clinicStockDto){
        return false;
    }

    @Override
    public boolean update(Integer id, ClinicStockDto clinicStockDto){
        ClinicStock clinicStock = clinicStockRepository.findById(id).get();
        Clinic clinic = clinicRepository.findById(clinicStockDto.getClinicDto().getId()).get();
        if(clinicStock == null){
            return false;
        }
        clinicStock.setQuantity(clinicStockDto.getQuantity());
        clinicStock.setClinic(clinic);
        clinicStockRepository.save(clinicStock);
        return true;
    }


    @Override
    public List<ClinicStockDto> getClinicStockByClinicId(Integer id){
        List<ClinicStock> clinicStocks = clinicStockRepository.getClinicStockByClinicId(id);
        List<ClinicStockDto> clinicStockDtos = new ArrayList<>();
        for (ClinicStock entity:clinicStocks){
            if (entity.getProduct().getStatus() == Status.ACTIVE){
                ClinicStockDto dto = clinicStockConvert.entityToDto(entity);
                clinicStockDtos.add(dto);
            }

        }
        return clinicStockDtos;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }
}
