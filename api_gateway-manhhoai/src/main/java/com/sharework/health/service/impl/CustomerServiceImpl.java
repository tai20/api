package com.sharework.health.service.impl;

import com.sharework.health.convert.CustomerConvert;
import com.sharework.health.dto.CustomerDto;
import com.sharework.health.dto.LevelDto;
import com.sharework.health.dto.PasswordDto;
import com.sharework.health.entity.*;
import com.sharework.health.repository.*;
import com.sharework.health.service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.Integer.parseInt;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class CustomerServiceImpl implements CustomerService {

    private static final Path CURRENT_FOLDER = Paths.get(System.getProperty("user.dir"));

    private CustomerRepository customerRepository;

    private LevelRepository levelRepository;

    private CustomerLevelRepository customerLevelRepository;
    private  OrderServiceImpl orderService;
    private  OrderRepository orderRepository;
    private LevelServiceImpl levelService;
    private CustomerConvert customerConvert;

    private CustomerCategoryRepository customerCategoryRepository;

    private RoleRepository roleRepository;

    private ClinicRepository clinicRepository;

    @Override
    public List<CustomerDto> findAll() {
        List<Customer> customers = customerRepository.findAll();
        List<CustomerDto> customerDtos = new ArrayList<>();

        for (Customer entity: customers) {
                CustomerDto dto = customerConvert.entityToDto(entity);

                customerDtos.add(dto);
        }
        return customerDtos;
    }


    @Override
    public CustomerDto findById(Integer id) {
        Customer entity = customerRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return customerConvert.entityToDto(entity);
    }




    @Override
    public boolean insert(CustomerDto dto){
        return true;
    }

//    @Override
//    public boolean insertCustomer(CustomerDto dto, MultipartFile avatar) throws IOException {
//        if (dto == null) {
//            return false;
//        }
//        if (customerRepository.findByPhoneNumber(dto.getPhoneNumber()) == null &&
//                customerRepository.findByCmnd(dto.getCmnd()) == null && customerRepository.findByEmail(dto.getEmail()) == null) {
//            Customer entity = customerConvert.dtoToEntity(dto);
//            Clinic clinic = clinicRepository.findById(dto.getClinicDto().getId()).get();
//            entity.setId(null);
//            entity.setCreatedDate(LocalDateTime.now());
//            entity.setCustomerCategory(customerCategoryRepository.findById(1).get());
//            entity.setClinic(clinic);
//            entity.setCustomerResource(dto.getCustomerResource());
//            Path staticPath = Paths.get("static");
//            Path imagePath = Paths.get("avartarCustomer");
//            if (!Files.exists(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath))) {
//                Files.createDirectories(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath));
//            }
//            if (entity.getAvatar() == null){
//                Path path = Paths.get("static/avartarCustomer/");
//                InputStream inputStream = avatar.getInputStream();
//                Files.copy(inputStream, path.resolve(avatar.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
//                entity.setAvatar(avatar.getOriginalFilename());
//
//            }
//            customerRepository.save(entity);
//            return true;
//        }
//        return false;
//    }

    @Override
    public boolean update(Integer id, CustomerDto dto) {
        Customer entity = customerRepository.findById(id).get();
        Clinic clinic = clinicRepository.findById(dto.getClinicDto().getId()).get();
        if (dto == null) {
            return false;
        }

        entity.setEmail(dto.getEmail());
        if (dto.getGender().equalsIgnoreCase("Nam")){
            entity.setGender(Gender.Nam);
        }
        if (dto.getGender().equalsIgnoreCase("Nữ")){
            entity.setGender(Gender.Nữ);
        }
        entity.setName(dto.getName());
        entity.setBirthDate(dto.getBirthDate());
        entity.setAddress(dto.getAddress());
        entity.setCmnd(dto.getCmnd());
        entity.setSkinStatus(dto.getSkinStatus());
        entity.setPhoneNumber(dto.getPhoneNumber());
        entity.setCustomerResource(dto.getCustomerResource());
        entity.setClinic(clinic);
        customerRepository.save(entity);
        return true;
    }

    @Override
    public boolean delete(Integer id) {
        Customer entity = customerRepository.findById(id).get();
        try {
            customerRepository.save(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public CustomerDto findByPhoneNumber(String phoneNumber) {
        Customer entity = customerRepository.findByPhoneNumber(phoneNumber);
        if (entity == null) {
            return null;
        }
        return customerConvert.entityToDto(entity);
    }

    @Override
    public List<CustomerDto> findAllByClinic() {
        List<Customer> customers = customerRepository.findAll();
        List<CustomerDto> customerDto = new  ArrayList<>();
        for(Customer entity: customers){
            CustomerDto dto = customerConvert.entityToDto(entity);
            if(dto.getType()== "Clinic"){
                customerDto.add(dto);
            }
        }
        return customerDto;
    }

    @Override
    public CustomerDto getProfile() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String email = ((UserDetails) principal).getUsername();

        Customer customer = customerRepository.findByEmail(email);
        return customerConvert.entityToDto(customer);
    }

    @Override
    public String changePassword(PasswordDto dto) {
        return null;
    }

    @Override
    public List<CustomerDto> findAllByCustomerCategory(Integer customerCategoryId) {
        List<Customer> customers = customerRepository.findAllByCustomerCategory(customerCategoryId);
        List<CustomerDto> customerDtos = new ArrayList<>();
        for (Customer entity: customers
        ) {
            CustomerDto dto = customerConvert.entityToDto(entity);
            customerDtos.add(dto);
        }
        return customerDtos;
    }





    @Override
    public boolean insertCustomer( String name, String email,
                                   String birthDate, String gender,
                                   String address, String cmnd,
                                   String phone, String skinStatus,
                                   String customerResource, MultipartFile avatar,
                                   Integer clinicId) throws IOException {
        Customer entity = new Customer();
//        if (name == null || phone == null || clinicId == null || cmnd == null || email == null) {
//            return false;
//        }
//        if(entity.getAvatar() == null){
//            entity.setAvatar("avatar_sample.jpeg");
//        }
//        if (customerRepository.findByPhoneNumber(phone) == null &&
//                customerRepository.findByCmnd(cmnd) == null && customerRepository.findByEmail(email) == null)
        if (customerRepository.findByPhoneNumber(phone) == null) {
            Clinic clinic = clinicRepository.findById(clinicId).get();
            entity.setId(null);
            entity.setName(name);
            entity.setGender(Gender.valueOf(gender));
            entity.setCreatedDate(LocalDateTime.now());
            entity.setCustomerCategory(customerCategoryRepository.findById(1).get());
            entity.setClinic(clinic);
            entity.setCustomerResource(customerResource);
            if(skinStatus.isEmpty()){
                entity.setSkinStatus(null);
            } else { entity.setSkinStatus(cmnd);}
            entity.setPhoneNumber(phone);
            if(cmnd.isEmpty()){
                entity.setCmnd(null);
            } else { entity.setCmnd(cmnd);}
            if(email.isEmpty()){
                entity.setEmail(null);
            } else { entity.setEmail(email);}
            if(address.isEmpty()){
                entity.setAddress(null);
            } else { entity.setAddress(address);}
            entity.setBirthDate(LocalDate.parse(birthDate));



            Path staticPath = Paths.get("static");
            Path imagePath = Paths.get("avatarCustomer");
            if (!Files.exists(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath))) {
                Files.createDirectories(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath));
            }

                Path path = Paths.get("static/avatarCustomer/");
                InputStream inputStream = avatar.getInputStream();
                //Tạo tên file ngẫu nhiên
                String fileName = avatar.getOriginalFilename();
                String suffixName=fileName.substring(fileName.lastIndexOf("."));
                fileName= UUID.randomUUID()+suffixName;
//                avatar.transferTo(dest);
                Files.copy(inputStream, path.resolve(fileName), StandardCopyOption.REPLACE_EXISTING);
                entity.setAvatar(fileName);

            customerRepository.save(entity);
            return true;
        }
        return false;
    }


    @Override
    public boolean updateAvatar(Integer id, MultipartFile avatar) throws IOException {
        Customer entity = customerRepository.findById(id).get();

        if (entity == null || avatar == null){
            return false;
        }
        else {
            Path staticPath = Paths.get("static");
            Path imagePath = Paths.get("avatarCustomer");
            if (!Files.exists(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath))) {
                Files.createDirectories(CURRENT_FOLDER.resolve(staticPath).resolve(imagePath));
            }
            {
                Path path = Paths.get("static/avatarCustomer/");
                InputStream inputStream = avatar.getInputStream();
                //Tạo tên file ngẫu nhiên
                String fileName = avatar.getOriginalFilename();
                String suffixName=fileName.substring(fileName.lastIndexOf("."));
                fileName= UUID.randomUUID()+suffixName;
//                avatar.transferTo(dest);
                Files.copy(inputStream, path.resolve(fileName), StandardCopyOption.REPLACE_EXISTING);
                entity.setAvatar(fileName);
            }
        }
        customerRepository.save(entity);
        return true;
    }

    @Override
    public boolean insertCustomerLevel(Integer customer_id, Integer level_id, BigDecimal total) throws IOException {
        return true;
    }

}




