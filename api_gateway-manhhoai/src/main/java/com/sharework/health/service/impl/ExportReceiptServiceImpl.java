package com.sharework.health.service.impl;

import com.sharework.health.convert.ExportReceiptConvert;
import com.sharework.health.convert.ExportReceiptDetailsConvert;
import com.sharework.health.dto.*;
import com.sharework.health.entity.*;
import com.sharework.health.repository.*;
import com.sharework.health.service.ExportReceiptService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class ExportReceiptServiceImpl implements ExportReceiptService {

    private ExportReceiptConvert exportReceiptConvert;

    private ExportReceiptRepository exportReceiptRepository;

    private ClinicRepository clinicRepository;

    private UserRepository userRepository;

    private OrderRepository orderRepository;

    private ClinicStockRepository clinicStockRepository;

    private ExportReceiptDetailsConvert exportReceiptDetailsConvert;

    private ExportReceiptDetailsRepository exportReceiptDetailsRepository;

    public void setSumPrice(Integer receipt_id){
        BigDecimal sumPrice = exportReceiptRepository.getSumPrice(receipt_id);
        Optional<ExportReceipt> exportReceipt = exportReceiptRepository.findById(receipt_id);
        exportReceipt.get().setSumprice(sumPrice);
        exportReceiptRepository.save(exportReceipt.get());
    }

    @Override
    public List<ExportReceiptDto> findAll(){
        List<ExportReceipt> exportReceipts = exportReceiptRepository.findAllSortDateDESC();
        List<ExportReceiptDto> exportReceiptDtos = new ArrayList<>();
        for (ExportReceipt entity:exportReceipts){
            setSumPrice(entity.getId());
            ExportReceiptDto dto = exportReceiptConvert.entityToDto(entity);
            exportReceiptDtos.add(dto);
        }
        return exportReceiptDtos;
    }

    @Override
    public ExportReceiptDto findById(Integer id){
        Optional<ExportReceipt> foundDto = exportReceiptRepository.findById(id);
        if (foundDto.isPresent()){
            ExportReceipt entity = exportReceiptRepository.findById(id).get();
            setSumPrice(entity.getId());
            return exportReceiptConvert.entityToDto(entity);
        }
        return null;
    }


    @Override
    public boolean insert(ExportReceiptDto dto){
        return false;
    }

    @Override
    public ExportReceiptDto insertExportReceipt(ExportReceiptDto dto){
        ExportReceipt entity = exportReceiptConvert.dtoToEntity(dto);
        Clinic clinic = clinicRepository.findById(dto.getClinicDto().getId()).get();
        Order order = orderRepository.findById(dto.getOrderDto().getId()).get();
        User user = userRepository.findById(dto.getUserDto().getId()).get();
        entity.setId(null);
        if (clinic == null || user == null || order == null){
            return null;
        } else {
            entity.setClinic(clinic);
            entity.setUser(user);
            entity.setOrder(order);
        }
        entity.setDateexport(LocalDateTime.now());
        entity.setReason(dto.getReason());
        entity.setSumprice(new BigDecimal(0));
        entity.setStatus(ReceiptStatus.ACTIVE);
        ExportReceipt result = exportReceiptRepository.save(entity);
        return exportReceiptConvert.entityToDto(result);
    }

    @Override
    public boolean update(Integer id, ExportReceiptDto exportReceiptDto){
        return  false;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }


    @Override
    public ExportReceiptDetailsDto insertExportReceiptDetail(ExportReceiptDetailsDto dto){
        ExportReceiptDetails entity = exportReceiptDetailsConvert.dtoToEntity(dto);
        ExportReceipt exportReceipt = exportReceiptRepository.findById(dto.getExportReceiptDto().getId()).get();
        ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinicStockDto().getId()).get();
        List<ExportReceiptDetails> exportReceiptDetails = exportReceiptDetailsRepository.getExportReceiptByClinicStockIdReceiptId(dto.getExportReceiptDto().getId(),dto.getClinicStockDto().getId());


        if (exportReceipt.getClinic().getId() != clinicStock.getClinic().getId() || dto.getNumber_product_export()<=0 || dto.getExport_price().compareTo(new BigDecimal(0))==-1) return null;
        if (clinicStock.getQuantity() < dto.getNumber_product_export() ||clinicStock == null) return null;
        if (exportReceipt == null || clinicStock == null || exportReceipt.getStatus() !=ReceiptStatus.ACTIVE){
            return null;
        } else
            //Check sp thêm vào có chưa, nếu có rồi thì cộng vào thêm
        if(exportReceiptDetails.isEmpty()){
            entity.setNumber_product_export(dto.getNumber_product_export());

        } else{
            for(ExportReceiptDetails itemSearch:exportReceiptDetails){
                entity.setNumber_product_export(dto.getNumber_product_export()+itemSearch.getNumber_product_export());
            }

        }
        clinicStock.setQuantity(clinicStock.getQuantity() - dto.getNumber_product_export());
        entity.setExportReceipt(exportReceipt);
        entity.setClinicStock(clinicStock);
        entity.setExport_price(dto.getExport_price());
        clinicStockRepository.save(clinicStock);
        ExportReceiptDetails result = exportReceiptDetailsRepository.save(entity);
        setSumPrice(entity.getExportReceipt().getId());
        return exportReceiptDetailsConvert.entityToDto(result);
    }

    @Override
    public boolean lockExportReceipt(Integer receipt_id){
        ExportReceipt exportReceipt = exportReceiptRepository.findById(receipt_id).get();
        if (exportReceipt.getStatus() == ReceiptStatus.ACTIVE){
            exportReceipt.setStatus(ReceiptStatus.LOCKED);
            return true;
        }
        return false;
    }

    @Override
    public List<ExportReceiptDetailsQueryDto> getAllExportReceiptDetailByReceiptid(Integer receipt_id){
        List<ExportReceiptDetailsQueryDto> exportReceiptDetailsQueryDtos = exportReceiptDetailsRepository.getAllExportReceiptDetailByReceiptid(receipt_id);
        return exportReceiptDetailsQueryDtos;
    }

    @Override
    public boolean deleteExportReceipt(Integer id){

        ExportReceipt exportReceipt = exportReceiptRepository.findById(id).get();
        if(exportReceipt.getStatus()!=ReceiptStatus.ACTIVE){
            return false;
        }

        List<ExportReceiptDetailsQueryDto> exportReceiptDetailsQueryDtos = exportReceiptDetailsRepository.getAllExportReceiptDetailByReceiptid(id);
        if(!exportReceiptDetailsQueryDtos.isEmpty()){
            for (ExportReceiptDetailsQueryDto dto:exportReceiptDetailsQueryDtos){
                ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinic_stock_id()).get();
                clinicStock.setQuantity(clinicStock.getQuantity() + dto.getNumber_product_export());
                clinicStockRepository.save(clinicStock);
            }
        }
        exportReceipt.setStatus(ReceiptStatus.DELETED);
        setSumPrice(id);
        exportReceiptRepository.save(exportReceipt);
        return true;
    }

    @Override
    public boolean deleteExportReceiptDetail(Integer receipt_id, Integer clinic_stock_id){
        ExportReceipt exportReceipt = exportReceiptRepository.findById(receipt_id).get();
        ClinicStock clinicStock = clinicStockRepository.findById(clinic_stock_id).get();
        if (exportReceipt.getStatus()!=ReceiptStatus.ACTIVE){
            return false;
        }
        List<ExportReceiptDetails> exportReceiptDetails = exportReceiptDetailsRepository.getExportReceiptByClinicStockIdReceiptId(receipt_id, clinic_stock_id);
        if (exportReceiptDetails.isEmpty()) return false;
        for(ExportReceiptDetails entity:exportReceiptDetails){
            clinicStock.setQuantity(clinicStock.getQuantity() + entity.getNumber_product_export());
            clinicStockRepository.save(clinicStock);
            exportReceiptDetailsRepository.delete(entity);
        }

        setSumPrice(receipt_id);
        return true;
    }

    @Override
    public ExportReceiptDetailsQueryDto updateExportReceiptDetail(ExportReceiptDetailsQueryDto dto){
        ExportReceipt exportReceipt = exportReceiptRepository.findById(dto.getReceipt_id()).get();
        List<ExportReceiptDetails> exportReceiptDetails = exportReceiptDetailsRepository.getExportReceiptByClinicStockIdReceiptId(dto.getReceipt_id(),dto.getClinic_stock_id());
        ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinic_stock_id()).get();

        if (exportReceipt.getStatus()!=ReceiptStatus.ACTIVE){
            return null;
        }
        for(ExportReceiptDetails entity:exportReceiptDetails){
            int quantityChange = dto.getNumber_product_export()-entity.getNumber_product_export();
            if (clinicStock.getQuantity()< dto.getNumber_product_export()) return null;
            clinicStock.setQuantity(clinicStock.getQuantity() - quantityChange);
            clinicStockRepository.save(clinicStock);
            //Luu lai phieu
            entity.setNumber_product_export(dto.getNumber_product_export());
            entity.setExport_price(dto.getExport_price());
            exportReceiptDetailsRepository.save(entity);
        }

        setSumPrice(exportReceipt.getId());
        return dto;
    }

    @Override
    public boolean restoreExportReceipt(Integer id){
        ExportReceipt exportReceipt = exportReceiptRepository.findById(id).get();
        if(exportReceipt.getStatus()!=ReceiptStatus.DELETED){
            return false;
        }
        List<ExportReceiptDetailsQueryDto> exportReceiptDetailsQueryDtos = exportReceiptDetailsRepository.getAllExportReceiptDetailByReceiptid(id);
        if(!exportReceiptDetailsQueryDtos.isEmpty()){
            for(ExportReceiptDetailsQueryDto dto:exportReceiptDetailsQueryDtos){
                ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinic_stock_id()).get();
                if(clinicStock.getQuantity()< dto.getNumber_product_export()) return false;
                clinicStock.setQuantity(clinicStock.getQuantity() - dto.getNumber_product_export());
                clinicStockRepository.save(clinicStock);
            }
        }
        exportReceipt.setStatus(ReceiptStatus.ACTIVE);
        exportReceiptRepository.save(exportReceipt);

        setSumPrice(id);
        return true;
    }


}
