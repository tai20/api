package com.sharework.health.service.impl;

import com.sharework.health.convert.FreeTrialConvert;
import com.sharework.health.dto.FreeTrialDto;
import com.sharework.health.entity.*;
import com.sharework.health.repository.*;
import com.sharework.health.service.FreeTrialService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(rollbackOn = Exception.class)
@AllArgsConstructor
public class FreeTrialServiceImpl implements FreeTrialService {

    private FreeTrialRepository freeTrialRepository;

    private FreeTrialConvert freeTrialConvert;

    private CustomerRepository customerRepository;

    private ServiceRepository serviceRepository;

    private SlipUseRepository slipUseRepository;

    private SlipUseDetailRepository slipUseDetailRepository;

    @Override
    public List<FreeTrialDto> findAll() {
        List<FreeTrial> freeTrials = freeTrialRepository.findAll();
        List<FreeTrialDto> freeTrialDtos = new ArrayList<>();

        for (FreeTrial entity: freeTrials
             ) {
            FreeTrialDto dto = freeTrialConvert.entityToDto(entity);
            freeTrialDtos.add(dto);
        }
        return freeTrialDtos;
    }

    @Override
    public FreeTrialDto findById(Integer id) {
        FreeTrial entity = freeTrialRepository.findById(id).get();
        if (entity == null){
            return null;
        }
        return freeTrialConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(FreeTrialDto dto) {
        if (dto == null){
            return false;
        }
        Customer customer = customerRepository.findById(dto.getCustomerDto().getId()).get();
        com.sharework.health.entity.Service service = serviceRepository.findById(dto.getServiceDto().getId()).get();
        if (customer != null && service != null){
            FreeTrial entity = freeTrialConvert.dtoToEntity(dto);
            entity.setCreatedDate(LocalDateTime.now());
            entity.setCustomer(customer);
            entity.setService(service);

            freeTrialRepository.save(entity);

            SlipUse slipUse = new SlipUse()
                    .setStatus(Status.ACTIVE)
                    .setCustomer(customer)
                    .setStartDate(LocalDate.now())
                    .setEndDate(LocalDate.now().plusYears(1))
                    .setImageBefore(null)
                    .setImageAfter(null);
            slipUseRepository.save(slipUse);

            SlipUseDetail slipUseDetail = new SlipUseDetail()
                    .setSlipUse(slipUse)
                    .setService(service)
                    .setNumberOfStock(1)
                    .setNumberOfUse(1);
            slipUseDetailRepository.save(slipUseDetail);
            return true;
        }
        return false;
    }

    @Override
    public boolean update(Integer id, FreeTrialDto dto) {

        FreeTrial oldEntity = freeTrialRepository.findById(id).get();
        if (oldEntity == null){
            return false;
        }

        Customer customer = customerRepository.findById(dto.getCustomerDto().getId()).get();
        com.sharework.health.entity.Service service = serviceRepository.findById(dto.getServiceDto().getId()).get();
        if (customer != null && service != null){
            oldEntity.setCustomer(customer);
            oldEntity.setService(service);
            freeTrialRepository.save(oldEntity);
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        FreeTrial entity = freeTrialRepository.findById(id).get();
        if (entity == null){
            return false;
        }
        freeTrialRepository.delete(entity);
        return true;
    }
}
