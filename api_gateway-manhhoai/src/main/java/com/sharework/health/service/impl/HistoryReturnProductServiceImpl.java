package com.sharework.health.service.impl;

import com.sharework.health.convert.HistoryReturnProductConvert;
import com.sharework.health.convert.HistoryReturnProductDetailConvert;
import com.sharework.health.dto.HistoryReturnProductDetailDto;
import com.sharework.health.dto.HistoryReturnProductDto;
import com.sharework.health.entity.*;
import com.sharework.health.repository.*;
import com.sharework.health.service.HistoryReturnProductService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(rollbackOn = Exception.class)
@AllArgsConstructor
public class HistoryReturnProductServiceImpl implements HistoryReturnProductService {

    private HistoryReturnProductRepository historyReturnProductRepository;

    private HistoryReturnProductDetailRepository historyReturnProductDetailRepository;

    private HistoryReturnProductConvert historyReturnProductConvert;

    private HistoryReturnProductDetailConvert historyReturnProductDetailConvert;

    private UserRepository userRepository;

    private SlipReturnProductRepository slipReturnProductRepository;

    private SlipReturnProductDetailRepository slipReturnProductDetailRepository;

    private ProductRepository productRepository;

    @Override
    public List<HistoryReturnProductDto> findAll() {
        List<HistoryReturnProduct> historyReturnProducts = historyReturnProductRepository.findAll();
        List<HistoryReturnProductDto> historyReturnProductDtos = new ArrayList<>();

        for (HistoryReturnProduct entity: historyReturnProducts
             ) {
            HistoryReturnProductDto dto = historyReturnProductConvert.entityToDto(entity);
            historyReturnProductDtos.add(dto);
        }
        return historyReturnProductDtos;
    }

    @Override
    public HistoryReturnProductDto findById(Integer id) {
        return null;
    }

    @Override
    public boolean insert(HistoryReturnProductDto dto) {
        if (dto == null){
            return false;
        }
        User user = userRepository.findById(dto.getUserDto().getId()).get();
        SlipReturnProduct slipReturnProduct = slipReturnProductRepository.findById(dto.getSlipReturnProductDto().getId()).get();

        if (user != null && slipReturnProduct != null){
            HistoryReturnProduct entity = historyReturnProductConvert.dtoToEntity(dto);
            entity.setCreatedDate(LocalDateTime.now());
            entity.setUser(user);
            entity.setSlipReturnProduct(slipReturnProduct);
            historyReturnProductRepository.save(entity);
            return true;
        }
        return false;
    }

    @Override
    public boolean update(Integer id, HistoryReturnProductDto dto) {
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public List<HistoryReturnProductDetailDto> findAllHistoryReturnProductDetail(Integer historyReturnProductId) {
        List<HistoryReturnProductDetail> historyReturnProductDetails = historyReturnProductDetailRepository.findAllByHistoryReturnProductId(historyReturnProductId);
        List<HistoryReturnProductDetailDto> historyReturnProductDetailDtos = new ArrayList<>();

        for (HistoryReturnProductDetail entity: historyReturnProductDetails
             ) {
            HistoryReturnProductDetailDto dto = historyReturnProductDetailConvert.entityToDto(entity);
            historyReturnProductDetailDtos.add(dto);
        }
        return historyReturnProductDetailDtos;
    }

    @Override
    public boolean addHistoryReturnProductDetail(HistoryReturnProductDetailDto dto) {
        if (dto.getHistoryReturnProductDto().getId() == null || dto.getProductDto().getId() == null){
            return false;
        }
        HistoryReturnProduct entity = historyReturnProductRepository.findById(dto.getHistoryReturnProductDto().getId()).get();
        if (entity == null){
            return false;
        }
        Product product = productRepository.findById(dto.getProductDto().getId()).get();
        HistoryReturnProductDetail historyReturnProductDetail = historyReturnProductDetailConvert.dtoToEntity(dto);
        historyReturnProductDetail.setProduct(product);
        historyReturnProductDetail.setQuantity(dto.getQuantity());
        historyReturnProductDetailRepository.save(historyReturnProductDetail);

        product.setQuantity(product.getQuantity() - dto.getQuantity());
        productRepository.save(product);

        SlipReturnProduct slipReturnProduct = slipReturnProductRepository.findById(entity.getSlipReturnProduct().getId()).get();
        List<SlipReturnProductDetail> slipReturnProductDetails = slipReturnProductDetailRepository.findAllBySlipReturnProductId(slipReturnProduct.getId());

        for (SlipReturnProductDetail slipReturnProductDetail: slipReturnProductDetails
             ) {
            if (slipReturnProductDetail.getProduct().getId() == product.getId()){
                slipReturnProductDetail.setQuantityStock(slipReturnProductDetail.getQuantityStock() - dto.getQuantity());
                slipReturnProductDetailRepository.save(slipReturnProductDetail);
                return true;
            }
        }
        return false;
    }

    @Override
    public List<HistoryReturnProductDto> findAllByCustomerId(Integer customerId) {
        List<HistoryReturnProduct> historyReturnProducts = historyReturnProductRepository.findAllByCustomerId(customerId);
        List<HistoryReturnProductDto> historyReturnProductDtos = new ArrayList<>();

        for (HistoryReturnProduct entity: historyReturnProducts
             ) {
            HistoryReturnProductDto dto = historyReturnProductConvert.entityToDto(entity);
            historyReturnProductDtos.add(dto);
        }
        return historyReturnProductDtos;
    }

    @Override
    public HistoryReturnProductDto findHistoryReturnProductAfterInsert() {
        HistoryReturnProduct entity = historyReturnProductRepository.getHistoryReturnProductAfterInsert();
        if (entity == null){
            return null;
        }
        return historyReturnProductConvert.entityToDto(entity);
    }
}
