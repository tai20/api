package com.sharework.health.service.impl;

import com.sharework.health.convert.ClinicStockConvert;
import com.sharework.health.convert.ImportReceiptConvert;
import com.sharework.health.convert.ImportReceiptDetailsConvert;
import com.sharework.health.dto.*;
import com.sharework.health.entity.*;
import com.sharework.health.repository.*;
import com.sharework.health.service.ImportReceiptService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class ImportReceiptServiceImpl implements ImportReceiptService {


    private ImportReceiptConvert importReceiptConvert;

    private ImportReceiptRepository importReceiptRepository;

    private ClinicRepository clinicRepository;

    private ClinicStockRepository clinicStockRepository;

    private SupplierRepository supplierRepository;

    private UserRepository userRepository;

    private ImportReceiptDetailsRepository importReceiptDetailsRepository;

    private ImportReceiptDetailsConvert importReceiptDetailsConvert;

    private ClinicStockConvert clinicStockConvert;


    public synchronized void setSumPrice(Integer receipt_id){

        BigDecimal sumPrice = importReceiptRepository.getSumPrice(receipt_id);
        Optional<ImportReceipt> importReceipt = importReceiptRepository.findById(receipt_id);
        importReceipt.get().setSumprice(sumPrice);
        importReceiptRepository.save(importReceipt.get());
        System.out.println(sumPrice);
    }

    @Override
    public List<ImportReceiptDto> findAll(){
        List<ImportReceipt> importReceipts = importReceiptRepository.findAllSortDateDESC();
        List<ImportReceiptDto> importReceiptDtos = new ArrayList<>();
        for (ImportReceipt entity:importReceipts){
                setSumPrice(entity.getId());
                ImportReceiptDto dto = importReceiptConvert.entityToDto(entity);
                importReceiptDtos.add(dto);
        }

        return importReceiptDtos;
    }



    @Override
    public ImportReceiptDto findById(Integer id){
        Optional<ImportReceipt> foundDto = importReceiptRepository.findById(id);
        if (foundDto.isPresent()){
            ImportReceipt entity = importReceiptRepository.findById(id).get();
            setSumPrice(entity.getId());
            return importReceiptConvert.entityToDto(entity);
        }
        return null;
    }


    @Override
    public boolean insert(ImportReceiptDto dto){
        return false;
    }



    @Override
    public ImportReceiptDto insertImportReceipt(ImportReceiptDto dto) {
        ImportReceipt entity = importReceiptConvert.dtoToEntity(dto);
        Clinic clinic = clinicRepository.findById(dto.getClinicDto().getId()).get();
        Supplier supplier = supplierRepository.findById(dto.getSupplierDto().getId()).get();
        User user = userRepository.findById(dto.getUserDto().getId()).get();

        entity.setId(null);
        if (clinic == null || user == null || supplier == null){
            return null;
        } else {
            entity.setClinic(clinic);
            entity.setUser(user);
            entity.setSupplier(supplier);
        }
        entity.setDateimport(LocalDateTime.now());
        entity.setSumprice(new BigDecimal(0));
        entity.setStatus(ReceiptStatus.ACTIVE);
        ImportReceipt result = importReceiptRepository.save(entity);
        return importReceiptConvert.entityToDto(result);
    }

    @Override
    public boolean update(Integer id, ImportReceiptDto importReceiptDto){
        return  false;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }



    @Override
    public ImportReceiptDetailsDto insertImportReceiptDetail(ImportReceiptDetailsDto dto){
        ImportReceiptDetails entity = importReceiptDetailsConvert.dtoToEntity(dto);
        ImportReceipt importReceipt = importReceiptRepository.findById(dto.getImportReceiptDto().getId()).get();
        ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinicStockDto().getId()).get();
        List<ImportReceiptDetails> importReceiptDetails = importReceiptDetailsRepository.getImportReceiptByClinicStockIdReceiptId(dto.getImportReceiptDto().getId(), dto.getClinicStockDto().getId());

        if (importReceipt.getClinic().getId() != clinicStock.getClinic().getId() || dto.getNumber_product_import()<=0 || dto.getImport_price().compareTo(new BigDecimal(0))==-1) return null;
        if (importReceipt == null || clinicStock == null ||importReceipt.getStatus() != ReceiptStatus.ACTIVE || clinicStock.getQuantity()<0){
            return  null;
        } else
        if (importReceiptDetails.isEmpty()){
            entity.setNumber_product_import(dto.getNumber_product_import());
        } else{
            for(ImportReceiptDetails itemSearch:importReceiptDetails){
                entity.setNumber_product_import(dto.getNumber_product_import() + itemSearch.getNumber_product_import());
            }
        }
        clinicStock.setQuantity(dto.getNumber_product_import() + clinicStock.getQuantity());
        entity.setImportReceipt(importReceipt);
        entity.setClinicStock(clinicStock);
        entity.setImport_price(dto.getImport_price());
        clinicStockRepository.save(clinicStock);
        ImportReceiptDetails result = importReceiptDetailsRepository.save(entity);
        setSumPrice(entity.getImportReceipt().getId());
        return importReceiptDetailsConvert.entityToDto(result);
    }


    @Override
    public List<ImportReceiptDetailsQueryDto> getAllImportReceiptDetailByReceiptid(Integer receipt_id){
        List<ImportReceiptDetailsQueryDto> importReceiptDetailsQueryDtos = importReceiptDetailsRepository.getAllImportReceiptDetailByReceiptid(receipt_id);
        return importReceiptDetailsQueryDtos;
    }


//    Chỉ chuyển trạng thái sang DELETED, không xóa detail của phiếu
    @Override
    public boolean deleteImportReceipt(Integer id){
        ImportReceipt importReceipt = importReceiptRepository.findById(id).get();


        if (importReceipt.getStatus()!=ReceiptStatus.ACTIVE ){
            return false;
        }
        List<ImportReceiptDetailsQueryDto> importReceiptDetailsQueryDtos = importReceiptDetailsRepository.getAllImportReceiptDetailByReceiptid(id);
        if (!importReceiptDetailsQueryDtos.isEmpty()) {
            for (ImportReceiptDetailsQueryDto dto : importReceiptDetailsQueryDtos) {
                ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinic_stock_id()).get();
                if(clinicStock.getQuantity()<dto.getNumber_product_import()) return false;
                clinicStock.setQuantity(clinicStock.getQuantity() - dto.getNumber_product_import());
                clinicStockRepository.save(clinicStock);
            }
        }
        importReceipt.setStatus(ReceiptStatus.DELETED);
        setSumPrice(id);
        importReceiptRepository.save(importReceipt);
        return true;
    }



    @Override
    public boolean deleteImportReceiptDetail(Integer receipt_id, Integer clinic_stock_id){
        ImportReceipt importReceipt = importReceiptRepository.findById(receipt_id).get();
        ClinicStock clinicStock = clinicStockRepository.findById(clinic_stock_id).get();


        if (importReceipt.getStatus()!=ReceiptStatus.ACTIVE){
            return false;
        }
        List<ImportReceiptDetails> importReceiptDetails = importReceiptDetailsRepository.getImportReceiptByClinicStockIdReceiptId(receipt_id, clinic_stock_id);
        if (importReceiptDetails.isEmpty())
        {return false;}
        for(ImportReceiptDetails entity:importReceiptDetails){
            clinicStock.setQuantity(clinicStock.getQuantity()- entity.getNumber_product_import());
            clinicStockRepository.save(clinicStock);
            importReceiptDetailsRepository.delete(entity);
        }
        setSumPrice(receipt_id);
        return true;

    }


    @Override
    public ImportReceiptDetailsQueryDto updateImportReceiptDetail(ImportReceiptDetailsQueryDto dto){
        ImportReceipt importReceipt = importReceiptRepository.findById(dto.getReceipt_id()).get();
        List<ImportReceiptDetails> importReceiptDetails = importReceiptDetailsRepository.getImportReceiptByClinicStockIdReceiptId(dto.getReceipt_id(), dto.getClinic_stock_id());
        ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinic_stock_id()).get();
        LocalDateTime date_present = LocalDateTime.now();



        if (importReceipt.getStatus()!=ReceiptStatus.ACTIVE){
            return null;
        }

        for(ImportReceiptDetails entity:importReceiptDetails){
            int quantityChange = dto.getNumber_product_import() - entity.getNumber_product_import();

            clinicStock.setQuantity(clinicStock.getQuantity()+quantityChange);
            clinicStockRepository.save(clinicStock);
            //Lưu lại phiếu
            entity.setNumber_product_import(dto.getNumber_product_import());
            entity.setImport_price(dto.getImport_price());
            importReceiptDetailsRepository.save(entity);

        }

        setSumPrice(importReceipt.getId());
        return dto;
    }

    @Override
    public boolean restoreImportReceipt(Integer id){
        ImportReceipt importReceipt = importReceiptRepository.findById(id).get();

        if (importReceipt.getStatus()!=ReceiptStatus.DELETED){
            return false;
        }
        List<ImportReceiptDetailsQueryDto> importReceiptDetailsQueryDtos = importReceiptDetailsRepository.getAllImportReceiptDetailByReceiptid(id);
        if (!importReceiptDetailsQueryDtos.isEmpty()) {
            for (ImportReceiptDetailsQueryDto dto : importReceiptDetailsQueryDtos) {
                ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinic_stock_id()).get();
                clinicStock.setQuantity(clinicStock.getQuantity() + dto.getNumber_product_import());
                clinicStockRepository.save(clinicStock);
            }
        }
        importReceipt.setStatus(ReceiptStatus.ACTIVE);
        importReceiptRepository.save(importReceipt);
        setSumPrice(id);
        return true;
    }



    @Override
    public boolean lockImportReceipt(Integer id){
        ImportReceipt importReceipt = importReceiptRepository.findById(id).get();
        if (importReceipt.getStatus() !=ReceiptStatus.ACTIVE){
            return false;
        }
        importReceipt.setStatus(ReceiptStatus.LOCKED);
        importReceiptRepository.save(importReceipt);
        return true;
    }




}








