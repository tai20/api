package com.sharework.health.service.impl;

import com.sharework.health.convert.*;
import com.sharework.health.dto.*;
import com.sharework.health.entity.*;
import com.sharework.health.repository.*;
import com.sharework.health.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class OrderServiceImpl implements OrderService {
    private BillRepository billRepository;

    private BillConvert billConvert;

    private OrderRepository orderRepository;

    private OrderConvert orderConvert;

    private  LevelRepository levelRepository;
    private ProductRepository productRepository;

    private CustomerRepository customerRepository;

    private CustomerCategoryRepository customerCategoryRepository;

    private UserRepository userRepository;

    private OrderDetailProductRepository orderDetailProductRepository;

    private OrderDetailProductConvert orderDetailProductConvert;

    private OrderDetailTreatmentPackageConvert orderDetailTreatmentPackageConvert;

    private OrderDetailServiceConvert orderDetailServiceConvert;

    private SlipUseRepository slipUseRepository;

    private OrderDetailServiceRepository orderDetailServiceRepository;

    private OrderDetailTreatmentPackageRepository orderDetailTreatmentPackageRepository;

    private SlipUseDetailRepository slipUseDetailRepository;

    private TreatmentPackageRepository treatmentPackageRepository;

    private TreatmentPackageServiceRepository treatmentPackageServiceRepository;

    private ServiceRepository serviceRepository;

    private SlipReturnProductRepository slipReturnProductRepository;

    private SlipReturnProductDetailRepository slipReturnProductDetailRepository;

    private ClinicRepository clinicRepository;

    private ClinicConvert clinicConvert;

    private OrderDetailPackageTestRepository orderDetailPackageTestRepository;

    private PackageTestRepository packageTestRepository;

    private SlipUsePackageTestRepository slipUsePackageTestRepository;

    private PackageTestServiceDetailRepository packageTestServiceDetailRepository;

    private SlipUsePackageTestDetailRepository slipUsePackageTestDetailRepository;

    @Override
    public List<OrderDto> findAll() {
        List<Order> orders = orderRepository.findAll();
        List<OrderDto> orderDtos = new ArrayList<>();

        for (Order entity : orders
        ) {
            if (orderRepository.countAllByCustomer(entity.getCustomer().getId()) > 0 && entity.getCustomer().getCustomerCategory().getId() == 1) {
                Customer customer = customerRepository.findById(entity.getCustomer().getId()).get();
                //customer.setCustomerCategory(customerCategoryRepository.findById(2).get());
                customer.setCustomerCategory(customerCategoryRepository.findByName("Khách hàng thường xuyên"));
                customerRepository.save(customer);
            }
            if (orderRepository.countAllByCustomer(entity.getCustomer().getId()) >= 20) {
                Customer customer = customerRepository.findById(entity.getCustomer().getId()).get();
                //customer.setCustomerCategory(customerCategoryRepository.findById(3).get());
                customer.setCustomerCategory(customerCategoryRepository.findByName("Khách hàng có nhiều hóa đơn"));
                customerRepository.save(customer);
            }
            if (orderRepository.sumOrderTotalOfCustomer(entity.getCustomer().getId()).compareTo(new BigDecimal(200000000)) >= 0) {
                Customer customer = customerRepository.findById(entity.getCustomer().getId()).get();
                //customer.setCustomerCategory(customerCategoryRepository.findById(4).get());
                customer.setCustomerCategory(customerCategoryRepository.findByName("Khách hàng VIP"));
                customerRepository.save(customer);
            }


            OrderDto dto = orderConvert.entityToDto(entity);
            orderDtos.add(dto);
        }
        return orderDtos;
    }

    public BigDecimal totalCustomer(Integer customer_id){
        return orderRepository.totalbyCustomerId(customer_id);
    }

    @Override
    public List<OrderDto> getAllOrderSortDateDESC(){
        List<Order> orders = orderRepository.getAllOrderSortDateDESC();
        List<OrderDto> orderDtos = new ArrayList<>();
        for (Order entity:orders){
            orderDtos.add(orderConvert.entityToDto(entity));
        }
        return orderDtos;
    }

    @Override
    public OrderDto findById(Integer id) {
        Order entity = orderRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return orderConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(OrderDto dto) {
        if (dto == null) {
            return false;
        }
        Customer customer = customerRepository.findById(dto.getCustomerDto().getId()).get();
        if (customer == null) {
            return false;
        }
        User user = userRepository.findById(dto.getUserDto().getId()).get();
        if (user == null) {
            return false;
        }
        Order entity = orderConvert.dtoToEntity(dto);
        entity.setId(null);
        entity.setCustomer(customer);
        entity.setUser(user);
        entity.setStatus(OrderStatus.Processing);
        entity.setOrderDate(LocalDateTime.now());
        entity.setDiscount(dto.getDiscount());
        entity.setTotal(dto.getTotal());
        entity.setDebtMoney(dto.getTotal());
        if (dto.getType().equals("2")) {
            entity.setType(OrderType.Spa);
        } else {
            entity.setType(OrderType.Clinic);
        }
        orderRepository.save(entity);

        return true;
    }


    @Override
    public boolean update(Integer id, OrderDto dto) {
        Order order = orderRepository.findById(id).get();
        if (order == null) {
            return false;
        }
        order.setDebtMoney(dto.getDebtMoney());
        order.setOrderDate(dto.getOrderDate());
        order.setPaymentMethod(dto.getPaymentMethod());
        order.setDiscount(dto.getDiscount());
        //order.setDateOfExamination(dto.getDateOfExamination());
        //order.setService(dto.getService());
        if (order.getDebtMoney().compareTo(new BigDecimal(0)) == 0 && order.getStatus() == OrderStatus.Paying) {
            order.setStatus(OrderStatus.Paid);
        }
        orderRepository.save(order);
        return true;
    }

    @Override
    public boolean delete(Integer id) {
        Order order = orderRepository.findById(id).get();
        if(order == null){
            return false;
        }
        List<Bill> bills = billRepository.findAll();
        for(Bill bill: bills){
            if(id == bill.getOrder().getId()){
                billRepository.delete(bill);
            }
        }
        orderRepository.delete(order);
        return true;
    }

//    @Override
//    public boolean deleteOrderDetailProduct(Integer orderId){
//        List<OrderDetailProduct> orderDetailProducts = orderDetailProductRepository.findByOrder(orderId);
//        for(OrderDetailProduct entity: orderDetailProducts){
//            orderDetailProductRepository.delete(entity);
//        }
//        return true;
//    }
//
//    @Override
//    public boolean deleteOrderDetailService(Integer orderId){
//        List<OrderDetailService> orderDetailServices = orderDetailServiceRepository.findAllByOrder(orderId);
//        for(OrderDetailService entity: orderDetailServices){
//            orderDetailServiceRepository.delete(entity);
//        }
//        return true;
//    }
//
//    @Override
//    public boolean deleteOrderDetailTreatmentPackage(Integer orderId){
//        List<OrderDetailTreatmentPackage> orderDetailTreatmentPackages = orderDetailTreatmentPackageRepository.findAllByOrder(orderId);
//        for(OrderDetailTreatmentPackage entity: orderDetailTreatmentPackages){
//            orderDetailTreatmentPackageRepository.delete(entity);
//        }
//        return true;
//    }

    @Override
    public boolean deleteByOrderId(Integer orderId){
        List<OrderDetailProduct> orderDetailProducts = orderDetailProductRepository.findByOrder(orderId);
        List<OrderDetailService> orderDetailServices = orderDetailServiceRepository.findAllByOrder(orderId);
        List<OrderDetailTreatmentPackage> orderDetailTreatmentPackages = orderDetailTreatmentPackageRepository.findAllByOrder(orderId);
        for(OrderDetailProduct entity: orderDetailProducts){
            orderDetailProductRepository.delete(entity);
        }
        for(OrderDetailService entity: orderDetailServices){
            orderDetailServiceRepository.delete(entity);
        }
        for(OrderDetailTreatmentPackage entity: orderDetailTreatmentPackages){
            orderDetailTreatmentPackageRepository.delete(entity);
        }
        return true;
    }

    @Override
    public boolean addOrderDetailProduct(OrderDetailProductDto dto) {
        Order order = orderRepository.findById(dto.getOrderDto().getId()).get();
        if (order == null) {
            return false;
        }
        Product product = productRepository.findById(dto.getProductDto().getId()).get();
        if (product == null) {
            return false;
        }
        OrderDetailProduct orderDetailProduct = new OrderDetailProduct()
                .setOrder(order)
                .setProduct(product)
                .setQuantity(dto.getQuantity())
                .setPrice(dto.getPrice());
        orderDetailProductRepository.save(orderDetailProduct);


        //product.setQuantity(product.getQuantity() - dto.getQuantity());

        productRepository.save(product);
        return true;
    }

    @Override
    public boolean addOrderDetailTreatmentPackage(OrderDetailTreatmentPackageDto dto) {
        Order order = orderRepository.findById(dto.getOrderDto().getId()).get();
        if (order == null) {
            return false;
        }
        TreatmentPackage treatmentPackage = treatmentPackageRepository.findById(dto.getTreatmentPackageDto().getId()).get();
        if (treatmentPackage == null) {
            return false;
        }
        OrderDetailTreatmentPackage orderDetailTreatmentPackage = new OrderDetailTreatmentPackage()
                .setOrder(order)
                .setTreatmentPackage(treatmentPackage)
                .setQuantity(dto.getQuantity())
                .setPrice(dto.getPrice());

        orderDetailTreatmentPackageRepository.save(orderDetailTreatmentPackage);
        return true;
    }

    @Override
    public boolean addOrderDetailService(OrderDetailServiceDto dto) {
        Order order = orderRepository.findById(dto.getOrderDto().getId()).get();
        if (order == null) {
            return false;
        }
        com.sharework.health.entity.Service service = serviceRepository.findById(dto.getServiceDto().getId()).get();
        if (service == null) {
            return false;
        }
        OrderDetailService orderDetailService = new OrderDetailService()
                .setOrder(order)
                .setService(service)
                .setQuantity(dto.getQuantity())
                .setPrice(dto.getPrice());

        orderDetailServiceRepository.save(orderDetailService);
        return true;
    }

    @Override
    public boolean updateOrderDetailProduct(OrderDetailProductDto dto) {
        if (dto == null) {
            return false;
        }

        return false;
    }

    @Override
    public boolean updateTotalAndDebt(Integer orderId, MoneyDto dto){
       Order order = orderRepository.findById(orderId).get();
       if(dto == null){
           return false;
       }
       order.setDebtMoney(dto.getDebtMoney());
       if (order.getDebtMoney().compareTo(new BigDecimal(0)) == 0 && order.getStatus() == OrderStatus.Paying) {
           order.setStatus(OrderStatus.Paid);
       }
       order.setTotal(dto.getTotal());
       orderRepository.save(order);
       return true;

    }

    @Override
    public boolean updateOrderDetailService(Integer orderId, Integer serviceId, OrderDetailServiceDto dto) {
        //List<OrderDetailService> orderDetailServices = orderDetailServiceRepository.findAllByOrder(orderId);
        OrderDetailService entity = orderDetailServiceRepository.findByOrderService(orderId, serviceId);
        com.sharework.health.entity.Service service = serviceRepository.findById(dto.getServiceDto().getId()).get();
        if(entity == null){
            return false;
        }
        //entity.setOrder(orderRepository.findById(orderId).get());
        //entity.setService(service);
        entity.setPrice(dto.getPrice());
        entity.setQuantity(dto.getQuantity());
        orderDetailServiceRepository.save(entity);
        return true;

    }



    @Override
    public List<OrderDto> getOrderByOrderDate(LocalDateTime orderDate) {
        List<OrderDto> orderDtos = new ArrayList<>();
        List<Order> orders = orderRepository.findByOrderDate(orderDate);
        for (Order entity : orders) {
            OrderDto dto = orderConvert.entityToDto(entity);
            orderDtos.add(dto);
        }
        return orderDtos;
    }

    @Override
    public List<OrderDto> getOrderByCustomer(Integer customerId) {
        Customer customer = customerRepository.findById(customerId).get();
        if (customer == null) {
            return null;
        }
        List<OrderDto> orderDtos = new ArrayList<OrderDto>();
        List<Order> orders = orderRepository.findByCustomer(customer);
        if (orders == null || orders.isEmpty()) {
            return null;
        }
        for (Order entity : orders) {
            OrderDto dto = orderConvert.entityToDto(entity);
            orderDtos.add(dto);
        }
        return orderDtos;
    }

    @Override
    public List<OrderDto> getOrderByUser(Integer userId) {
        User user = userRepository.findById(userId).get();
        if (user == null) {
            return null;
        }
        List<OrderDto> orderDtos = new ArrayList<OrderDto>();
        List<Order> orders = orderRepository.findByUser(user);
        if (orders == null || orders.isEmpty()) {
            return null;
        }
        for (Order entity : orders) {
            OrderDto dto = orderConvert.entityToDto(entity);
            orderDtos.add(dto);
        }
        return orderDtos;
    }

    @Override
    public List<OrderDetailProductDto> findAllOrderDetailProductWithOrder(Integer orderId) {
        List<OrderDetailProduct> orderDetailProducts = orderDetailProductRepository.findByOrder(orderId);
        List<OrderDetailProductDto> orderDetailProductDtos = new ArrayList<>();
        for (OrderDetailProduct entity : orderDetailProducts
        ) {
            OrderDetailProductDto dto = orderDetailProductConvert.entityToDto(entity);
            orderDetailProductDtos.add(dto);
        }
        return orderDetailProductDtos;
    }

    @Override
    public List<OrderDetailTreatmentPackageDto> findAllOrderDetailTreatmentPackageWithOrder(Integer orderId) {
        List<OrderDetailTreatmentPackage> orderDetailTreatmentPackages = orderDetailTreatmentPackageRepository.findAllByOrder(orderId);
        List<OrderDetailTreatmentPackageDto> orderDetailTreatmentPackageDtos = new ArrayList<>();
        for (OrderDetailTreatmentPackage entity : orderDetailTreatmentPackages
        ) {
            OrderDetailTreatmentPackageDto dto = orderDetailTreatmentPackageConvert.entityToDto(entity);
            orderDetailTreatmentPackageDtos.add(dto);
        }
        return orderDetailTreatmentPackageDtos;
    }

    @Override
    public List<OrderDetailServiceDto> findAllOrderDetailServiceWithOrder(Integer orderId) {
        List<OrderDetailService> orderDetailServices = orderDetailServiceRepository.findAllByOrder(orderId);
        List<OrderDetailServiceDto> orderDetailServiceDtos = new ArrayList<>();
        for (OrderDetailService entity : orderDetailServices
        ) {
            OrderDetailServiceDto dto = orderDetailServiceConvert.entityToDto(entity);
            orderDetailServiceDtos.add(dto);
        }
        return orderDetailServiceDtos;
    }

    @Override
    public List<OrderDto> findAllOrderByClinic(Integer clinicId) {
        List<Order> orders = orderRepository.findByClinic(clinicId);
        List<OrderDto> orderDtos = new ArrayList<>();
        for (Order entity : orders
        ) {
            OrderDto dto = orderConvert.entityToDto(entity);
            orderDtos.add(dto);
        }
        return orderDtos;
    }

    @Override
    public List<OrderDto> findAllOrderWithExamination() {
        List<OrderDto> orderDtos = new ArrayList<>();
        List<Order> orders = orderRepository.findAllOrderWithExamination();
        for (Order entity : orders
        ) {
            OrderDto dto = orderConvert.entityToDto(entity);
            orderDtos.add(dto);
        }
        return orderDtos;
    }

    @Override
    public List<OrderDto> findAllOrderWithOrderDetails() {
        List<OrderDto> orderDtos = new ArrayList<>();
        List<Order> orders = orderRepository.findAllOrderWithOrderDetails();
        for (Order entity : orders
        ) {
            OrderDto dto = orderConvert.entityToDto(entity);
            orderDtos.add(dto);
        }
        return orderDtos;
    }

    @Override
    public List<OrderDto> findByOrderDateBetween(LocalDateTime startDate, LocalDateTime endDate) {
        List<Order> orders = orderRepository.statisticalOrderByOrderDateIsBetween(startDate, endDate);
        List<OrderDto> orderDtos = new ArrayList<>();

        for (Order entity : orders
        ) {
            OrderDto dto = orderConvert.entityToDto(entity);
            orderDtos.add(dto);
        }
        return orderDtos;
    }

    @Override
    public boolean confirmOrderBeforePayment(Integer orderId) {
        Order order = orderRepository.findById(orderId).get();
        if (order == null) {
            return false;
        }
        if (order.getStatus() == OrderStatus.Processing) {
            order.setStatus(OrderStatus.Confirmed);
            orderRepository.save(order);
            return true;
        }
        return false;
    }

    @Override
    public boolean paymentOrder(Integer orderId, PaymentDto dto) {
        Order entity = orderRepository.findById(orderId).get();
        Bill bill = new Bill();

        if (entity == null) {
            return false;
        }
        if (entity.getStatus() == OrderStatus.Confirmed) {
            entity.setPaymentMethod(dto.getPaymentMethod());
            BigDecimal finalmoney = entity.getDebtMoney().subtract(dto.getMoneyPayment());
            if (finalmoney.compareTo(new BigDecimal(0)) < 0) {
                finalmoney = new BigDecimal(0);
            }
            entity.setDebtMoney(finalmoney);
            if (entity.getDebtMoney().compareTo(new BigDecimal(0)) > 0) {
                entity.setStatus(OrderStatus.Paying);
                orderRepository.save(entity);
            }
            if (entity.getDebtMoney().compareTo(new BigDecimal(0)) <= 0) {
                entity.setStatus(OrderStatus.Paid);
                orderRepository.save(entity);
            }

            //set Bill
            bill.setOrder(entity);
            bill.setPaymentDate(entity.getOrderDate());
            bill.setPaymentMethod(entity.getPaymentMethod());
            bill.setPaymentAmount(dto.getMoneyPayment());
            billRepository.save(bill);

            List<OrderDetailProduct> orderDetailProducts = orderDetailProductRepository.findByOrder(orderId);
            if (!orderDetailProducts.isEmpty()) {
                SlipReturnProduct slipReturnProduct = new SlipReturnProduct()
                        .setCreatedDate(LocalDateTime.now())
                        .setStatus(Status.ACTIVE)
                        .setCustomer(customerRepository.findById(entity.getCustomer().getId()).get());
                slipReturnProductRepository.save(slipReturnProduct);

                for (OrderDetailProduct orderDetailProduct : orderDetailProducts
                ) {
                    SlipReturnProductDetail slipReturnProductDetail = new SlipReturnProductDetail()
                            .setSlipReturnProduct(slipReturnProduct)
                            .setProduct(orderDetailProduct.getProduct())
                            .setQuantityStock(orderDetailProduct.getQuantity())
                            .setQuantity(orderDetailProduct.getQuantity());
                    slipReturnProductDetailRepository.save(slipReturnProductDetail);
                }
            }

            List<OrderDetailService> orderDetailServices = orderDetailServiceRepository.findAllByOrder(orderId);
            if (!orderDetailServices.isEmpty()) {
                SlipUse slipUse = new SlipUse()
                        .setStartDate(LocalDate.now())
                        .setEndDate(LocalDate.now().plusYears(1))
                        .setImageBefore(null)
                        .setImageAfter(null)
                        .setCustomer(customerRepository.findById(entity.getCustomer().getId()).get());
                if (entity.getDebtMoney().compareTo(new BigDecimal(0)) > 0) {
                    slipUse.setStatus(Status.DEACTIVE);
                } else {
                    slipUse.setStatus(Status.ACTIVE);
                }
                slipUseRepository.save(slipUse);
                for (OrderDetailService orderDetailService : orderDetailServices
                ) {
                    SlipUseDetail slipUseDetail = new SlipUseDetail()
                            .setSlipUse(slipUse)
                            .setService(orderDetailService.getService())
                            .setNumberOfUse(orderDetailService.getQuantity())
                            .setNumberOfStock(orderDetailService.getQuantity());
                    slipUseDetailRepository.save(slipUseDetail);
                }
            }

            List<OrderDetailTreatmentPackage> orderDetailTreatmentPackages = orderDetailTreatmentPackageRepository.findAllByOrder(orderId);
            if (!orderDetailTreatmentPackages.isEmpty()) {
                for (OrderDetailTreatmentPackage orderDetailTreatmentPackage : orderDetailTreatmentPackages
                ) {
                    TreatmentPackage treatmentPackage = treatmentPackageRepository.findById(orderDetailTreatmentPackage.getTreatmentPackage().getId()).get();
                    for (int i = 0; i < orderDetailTreatmentPackage.getQuantity(); i++) {
                        SlipUse slipUse = new SlipUse()
                                .setStartDate(LocalDate.now())
                                .setEndDate(LocalDate.now().plusYears(1))
                                .setImageBefore(null)
                                .setImageAfter(null)
                                .setCustomer(customerRepository.findById(entity.getCustomer().getId()).get())
                                .setTreatmentPackage(treatmentPackage);

                        if (entity.getDebtMoney().compareTo(new BigDecimal(0)) > 0) {
                            slipUse.setStatus(Status.DEACTIVE);
                        } else {
                            slipUse.setStatus(Status.ACTIVE);
                        }
                        slipUseRepository.save(slipUse);
                        List<TreatmentPackageService> treatmentPackageServices = treatmentPackageServiceRepository.findAllByTreatmentPackage(treatmentPackage.getId());
                        for (TreatmentPackageService treatmentPackageService : treatmentPackageServices
                        ) {
                            SlipUseDetail slipUseDetail = new SlipUseDetail()
                                    .setSlipUse(slipUse)
                                    .setService(treatmentPackageService.getService())
                                    .setNumberOfUse(treatmentPackageService.getNumberOfUse())
                                    .setNumberOfStock(treatmentPackageService.getNumberOfUse());
                            slipUseDetailRepository.save(slipUseDetail);
                        }
                    }
                }
            }

            return true;
        }
        return false;
    }

    @Override
    public boolean paymentRest(Integer orderId, PaymentDto dto) {
        Order entity = orderRepository.findById(orderId).get();
        Bill bill = new Bill();

        //set Bill
        bill.setOrder(entity);
        bill.setPaymentDate(entity.getOrderDate());
        bill.setPaymentMethod(entity.getPaymentMethod());
        bill.setPaymentAmount(dto.getMoneyPayment());
        billRepository.save(bill);

        if (entity == null) {
            return false;
        }
        if (entity.getDebtMoney().compareTo(new BigDecimal(0)) > 0) {
            BigDecimal finalmoney = entity.getDebtMoney().subtract(dto.getMoneyPayment());
            if (finalmoney.compareTo(new BigDecimal(0)) < 0) {
                finalmoney = new BigDecimal(0);
            }
            entity.setDebtMoney(finalmoney);
            if (entity.getDebtMoney().compareTo(new BigDecimal(0)) > 0) {
                entity.setStatus(OrderStatus.Paying);
                orderRepository.save(entity);
                return true;
            }
            if (entity.getDebtMoney().compareTo(new BigDecimal(0)) <= 0) {
                entity.setStatus(OrderStatus.Paid);
                orderRepository.save(entity);
                return true;
            }

            return true;
        }
        return false;
    }

    @Override
    public OrderDto getOrderAfterInsert() {
        Order entity = orderRepository.getOrderAfterInsert();
        if (entity == null) {
            return null;
        }
        return orderConvert.entityToDto(entity);
    }

    @Override
    public boolean paymentClinic(Integer orderId, PaymentDto dto) {
        Order entity = orderRepository.findById(orderId).get();
        if (entity == null) {
            return false;
        }
        if (entity.getStatus() == OrderStatus.Confirmed) {
            entity.setPaymentMethod(dto.getPaymentMethod());
            BigDecimal finalmoney = entity.getDebtMoney().subtract(dto.getMoneyPayment());
            if (finalmoney.compareTo(new BigDecimal(0)) < 0) {
                finalmoney = new BigDecimal(0);
            }
            entity.setDebtMoney(finalmoney);
            if (entity.getDebtMoney().compareTo(new BigDecimal(0)) > 0) {
                entity.setStatus(OrderStatus.Paying);
                orderRepository.save(entity);
            }
            if (entity.getDebtMoney().compareTo(new BigDecimal(0)) <= 0) {
                entity.setStatus(OrderStatus.Paid);
                orderRepository.save(entity);
            }
            List<OrderDetailPackageTest> orderDetailPackageTests = orderDetailPackageTestRepository.getOrderDetailPackageTestByOrderId(orderId);
            if (!orderDetailPackageTests.isEmpty()) {
                for (OrderDetailPackageTest orderDetailPackageTest : orderDetailPackageTests
                ) {
                    PackageTest packageTest = packageTestRepository.findById(orderDetailPackageTest.getPackageTest().getPackagetest_id()).get();
                    for (int i = 0; i < orderDetailPackageTest.getQuantity(); i++) {
                        SlipUsePackageTest slipUsePackageTest = new SlipUsePackageTest()
                                .setStartdate(LocalDate.now())
                                .setEnddate(LocalDate.now().plusYears(1))
                                .setCustomer(customerRepository.findById(entity.getCustomer().getId()).get())
                                .setName(packageTest.getPackagetestname())
                                .setTestService(packageTest.getTestService());
                        if (packageTest.getNumberofuse() != null) {
                            slipUsePackageTest.setPrice(new BigDecimal(0));
                            slipUsePackageTest.setNumberofuse(packageTest.getNumberofuse());
                        } else {
                            slipUsePackageTest.setPrice(packageTest.getPrice());
                        }
                        if (entity.getDebtMoney().compareTo(new BigDecimal(0)) > 0) {
                            slipUsePackageTest.setStatus(Status.DEACTIVE);
                        } else {
                            slipUsePackageTest.setStatus(Status.ACTIVE);
                        }
                        slipUsePackageTestRepository.save(slipUsePackageTest);
                        List<PackageTestServiceDetail> packageTestServiceDetails = packageTestServiceDetailRepository.getPackageTestDetalByPackageId(packageTest.getPackagetest_id());
                        for (PackageTestServiceDetail packageTestServiceDetail : packageTestServiceDetails
                        ) {
                            SlipUsePackageTestDetail slipUsePackageTestDetail = new SlipUsePackageTestDetail()
                                    .setSlipusePackageTest(slipUsePackageTest)
                                    .setCategoryIndicationCard(packageTestServiceDetail.getCategoryIndicationCard())
                                    .setNumberofuse(packageTestServiceDetail.getNumberofuse());
                            slipUsePackageTestDetailRepository.save(slipUsePackageTestDetail);
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }
}
