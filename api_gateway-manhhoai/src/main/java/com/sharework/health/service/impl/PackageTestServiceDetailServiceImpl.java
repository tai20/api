package com.sharework.health.service.impl;

import com.sharework.health.convert.PackageTestServiceDetailConvert;
import com.sharework.health.dto.PackageTestServiceDetailDto;
import com.sharework.health.entity.CategoryIndicationCard;
import com.sharework.health.entity.PackageTest;
import com.sharework.health.entity.PackageTestServiceDetail;
import com.sharework.health.entity.Status;
import com.sharework.health.repository.CategoryIndicationCardRepository;
import com.sharework.health.repository.PackageTestRepository;
import com.sharework.health.repository.PackageTestServiceDetailRepository;
import com.sharework.health.service.PackageTestServiceDetailService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class PackageTestServiceDetailServiceImpl implements PackageTestServiceDetailService {

    private PackageTestServiceDetailRepository packageTestServiceDetailRepository;

    private PackageTestRepository packageTestRepository;

    private CategoryIndicationCardRepository categoryIndicationCardRepository;

    private PackageTestServiceDetailConvert packageTestServiceDetailConvert;

    @Override
    public List<PackageTestServiceDetailDto> findAll() {
        List<PackageTestServiceDetail> packageTestServiceDetails = packageTestServiceDetailRepository.findAll();
        List<PackageTestServiceDetailDto> packageTestServiceDetailDtos = new ArrayList<>();
        for (PackageTestServiceDetail entity : packageTestServiceDetails
        ) {
            if (entity.getPackageTest().getStatus().equals(Status.ACTIVE)) {
                PackageTestServiceDetailDto dto = packageTestServiceDetailConvert.entityToDto(entity);
                packageTestServiceDetailDtos.add(dto);
            }
        }
        return packageTestServiceDetailDtos;
    }

    @Override
    public PackageTestServiceDetailDto findById(Integer id) {
        PackageTestServiceDetail entity = packageTestServiceDetailRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        if (entity.getPackageTest().getStatus().equals(Status.DEACTIVE)) {
            return null;
        }
        return packageTestServiceDetailConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(PackageTestServiceDetailDto dto) {
        if (dto == null) {
            return false;
        }
        PackageTest packageTest = packageTestRepository.findById(dto.getPackageTestDto()
                .getPackagetest_id()).get();

        CategoryIndicationCard categoryIndicationCard = categoryIndicationCardRepository.findById(dto.getCategoryIndicationCardDto()
                .getId()).get();

        if (packageTest != null && categoryIndicationCard != null) {
            PackageTestServiceDetail packageTestServiceDetail = packageTestServiceDetailConvert.dtoToEntity(dto);
            packageTestServiceDetail.setId(null);
            packageTestServiceDetail.setPackageTest(packageTest);
            packageTestServiceDetail.setCategoryIndicationCard(categoryIndicationCard);
            packageTestServiceDetailRepository.save(packageTestServiceDetail);
            return true;
        }
        return false;
    }

    @Override
    public boolean update(Integer id, PackageTestServiceDetailDto dto) {
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public List<PackageTestServiceDetailDto> getPackageTestDetalByPackageId(Integer packagetest_id) {
        List<PackageTestServiceDetail> packageTestServiceDetails = packageTestServiceDetailRepository.getPackageTestDetalByPackageId(packagetest_id);
        List<PackageTestServiceDetailDto> packageTestServiceDetailDtos = new ArrayList<>();
        for (PackageTestServiceDetail entity : packageTestServiceDetails
        ) {
            PackageTestServiceDetailDto dto = packageTestServiceDetailConvert.entityToDto(entity);
            packageTestServiceDetailDtos.add(dto);
        }
        return packageTestServiceDetailDtos;
    }
}
