package com.sharework.health.service.impl;

import com.sharework.health.convert.PackageTestConvert;
import com.sharework.health.dto.PackageTestDto;
import com.sharework.health.dto.TestServiceDto;
import com.sharework.health.entity.*;
import com.sharework.health.repository.PackageTestRepository;
import com.sharework.health.repository.TestServiceRepository;
import com.sharework.health.service.PackageTestService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class PackageTestServiceImpl implements PackageTestService {

    private PackageTestRepository packageTestRepository;

    private PackageTestConvert packageTestConvert;

    private TestServiceRepository testServiceRepository;

    @Override
    public List<PackageTestDto> findAll() {
        List<PackageTest> packageTests = packageTestRepository.findAll();
        List<PackageTestDto> packageTestDtos = new ArrayList<>();
        for (PackageTest entity : packageTests
        ) {
            if (entity.getStatus().equals(Status.ACTIVE)) {
                PackageTestDto dto = packageTestConvert.entityToDto(entity);
                packageTestDtos.add(dto);
            }
        }
        return packageTestDtos;

    }

    @Override
    public PackageTestDto findById(Integer id) {
        PackageTest entity = packageTestRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        if (entity.getStatus().equals(Status.DEACTIVE)) {
            return null;
        }
        return packageTestConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(PackageTestDto dto) {
        if (dto == null) {
            return false;
        }
        TestService testService = null;
        if (dto.getTestServiceDto().getTestservice_id() != null) {
            testService = testServiceRepository.findById(dto.getTestServiceDto().getTestservice_id()).get();
        }

        PackageTest packageTest = packageTestConvert.dtoToEntity(dto);
        packageTest.setPackagetest_id(null);
        if (testService == null && packageTest.getNumberofuse() == null) {
            packageTest.setStatus(Status.ACTIVE);
            packageTest.setTestService(null);

        } else {
            packageTest.setStatus(Status.ACTIVE);
            packageTest.setTestService(testService);
        }

        packageTestRepository.save(packageTest);
        return true;
    }

    @Override
    public boolean update(Integer id, PackageTestDto dto) {
        if (dto == null) {
            return false;
        }
        TestService testService = null;
        if (dto.getTestServiceDto().getTestservice_id() != null) {
            testService = testServiceRepository.findById(dto.getTestServiceDto().getTestservice_id()).get();
        }

        PackageTest packageTest = packageTestRepository.findById(id).get();
        if (testService == null && packageTest.getNumberofuse() == null) {
            packageTest.setTestService(null);
            packageTest.setNumberofuse(null);
            packageTest.setPackagetestname(dto.getPackagetestname());
            packageTest.setPrice(dto.getPrice());

        } else {
            packageTest.setTestService(testService);
            packageTest.setNumberofuse(dto.getNumberofuse());
            packageTest.setPackagetestname(dto.getPackagetestname());
            packageTest.setPrice(dto.getPrice());
        }

        packageTestRepository.save(packageTest);
        return true;
    }

    @Override
    public boolean delete(Integer id) {
        PackageTest entity = packageTestRepository.findById(id).get();
        if (entity == null) {
            return false;
        }
        entity.setStatus(Status.DEACTIVE);
        packageTestRepository.save(entity);
        return true;
    }

    @Override
    public PackageTestDto getPackageTestAfterInsert() {
        PackageTest entity = packageTestRepository.getPackageTestAfterInsert();
        if (entity == null) {
            return null;
        }
        return packageTestConvert.entityToDto(entity);
    }
}
