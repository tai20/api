package com.sharework.health.service.impl;

import com.sharework.health.convert.PrescriptionConvert;
import com.sharework.health.convert.PrescriptionDetailConvert;
import com.sharework.health.dto.MedicalCardDto;
import com.sharework.health.dto.PrescriptionDetailDto;
import com.sharework.health.dto.PrescriptionDto;
import com.sharework.health.entity.MedicalCard;
import com.sharework.health.entity.Prescription;
import com.sharework.health.entity.PrescriptionDetail;
import com.sharework.health.entity.Product;
import com.sharework.health.repository.MedicalCardRepository;
import com.sharework.health.repository.PrescriptionDetailRepository;
import com.sharework.health.repository.PrescriptionRepository;
import com.sharework.health.repository.ProductRepository;
import com.sharework.health.service.PrescriptionService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class PrescriptionServiceImpl implements PrescriptionService {

    private PrescriptionRepository prescriptionRepository;

    private PrescriptionDetailRepository prescriptionDetailRepository;

    private MedicalCardRepository medicalCardRepository;

    private ProductRepository productRepository;

    private PrescriptionConvert prescriptionConvert;

    private PrescriptionDetailConvert prescriptionDetailConvert;

    @Override
    public List<PrescriptionDto> findAll() {
        List<Prescription> prescriptions = prescriptionRepository.findAll();
        List<PrescriptionDto> prescriptionDtos = new ArrayList<>();

        for (Prescription entity : prescriptions
        ) {
            PrescriptionDto dto = prescriptionConvert.entityToDto(entity);
            prescriptionDtos.add(dto);
        }
        return prescriptionDtos;
    }

    @Override
    public PrescriptionDto findById(Integer id) {
        Prescription entity = prescriptionRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return prescriptionConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(PrescriptionDto dto) {
        if (dto == null) {
            return false;
        }

        Prescription entity = prescriptionConvert.dtoToEntity(dto);
        entity.setId(null);
        entity.setCreatedDate(LocalDateTime.now());
        prescriptionRepository.save(entity);
        return true;
    }

    //    @Override
//    public boolean insertPrescription(Integer medicalId,PrescriptionDto dto) {
//        if (dto == null) {
//            return false;
//        }
//        Prescription entity = prescriptionConvert.dtoToEntity(dto);
//        entity.setId(null);
//        entity.setCreatedDate(LocalDateTime.now());
//        prescriptionRepository.save(entity);
//        return true;
//    }
    @Override
    public PrescriptionDto updatePrescriptionInMedicalCard(Integer medicalId, PrescriptionDto dto) {
//        if(dto ==null){
//            return false;
//        }
        MedicalCard medicalCard = medicalCardRepository.findById(medicalId).get();
        Prescription prescription = prescriptionRepository.findById(dto.getId()).get();
        if (medicalCard != null && prescription != null) {
            medicalCard.setPrescription(prescription);
            medicalCardRepository.save(medicalCard);
            return prescriptionConvert.entityToDto(prescription);
        }
        return null;
    }

    @Override
    public boolean update(Integer id, PrescriptionDto dto) {
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public boolean addPrescriptionDetail(PrescriptionDetailDto dto) {
        if (dto == null) {
            return false;
        }
        Prescription prescription = prescriptionRepository.findById(dto.getPrescriptionDto().getId()).get();
        Product product = productRepository.findById(dto.getProductDto().getId()).get();

        if (prescription != null && product != null) {
            PrescriptionDetail entity = prescriptionDetailConvert.dtoToEntity(dto);
            entity.setPrescription(prescription);
            entity.setProduct(product);
            entity.setQuantity(dto.getQuantity());
            entity.setTutorial(dto.getTutorial());
            prescriptionDetailRepository.save(entity);
            return true;
        }
        return false;
    }

    @Override
    public boolean deletePrescriptionDetail(PrescriptionDetailDto dto) {
        if (dto == null) {
            return false;
        }
        List<PrescriptionDetail> prescriptionDetailList = prescriptionDetailRepository.findAll();

        for (PrescriptionDetail prescriptionDetail : prescriptionDetailList) {
            if (prescriptionDetail.getPrescription().getId().equals(dto.getPrescriptionDto().getId())
                    && prescriptionDetail.getProduct().getId().equals(dto.getProductDto().getId())) {
                prescriptionDetailRepository.delete(prescriptionDetail);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean updatePrescriptionDetail(Integer prescriptionId, Integer productId, PrescriptionDetailDto dto) {
        if (dto == null) {
            return false;
        }

        List<PrescriptionDetail> prescriptionDetailList = prescriptionDetailRepository.findAll();

        for (PrescriptionDetail prescriptionDetail : prescriptionDetailList) {
            if (prescriptionDetail.getPrescription().getId().equals(prescriptionId)
                    && prescriptionDetail.getProduct().getId().equals(productId)) {
                prescriptionDetail.setTutorial(dto.getTutorial());
                prescriptionDetail.setQuantity(dto.getQuantity());

                prescriptionDetailRepository.save(prescriptionDetail);
                return true;
            }
        }
        return false;
    }


    @Override
    public PrescriptionDto getPrescriptionAfterInsert() {
        Prescription entity = prescriptionRepository.getPrescriptionAfterInsert();
        if (entity == null) {
            return null;
        }
        return prescriptionConvert.entityToDto(entity);
    }

    @Override
    public List<PrescriptionDetailDto> getPrescriptionDetailByMedicalCard(Integer medicalId) {
        List<PrescriptionDetail> prescriptionDetails = prescriptionDetailRepository.getPrescriptionDetailByMedicalCard(medicalId);
        List<PrescriptionDetailDto> prescriptionDetailDtos = new ArrayList<>();
//        MedicalCard medicalCard = medicalCardRepository.findById(medicalId).get();
        for (PrescriptionDetail entity : prescriptionDetails
        ) {
//            if(entity.getPrescription().getId().equals(medicalCard.getPrescription().getId())){
            PrescriptionDetailDto dto = prescriptionDetailConvert.entityToDto(entity);
            prescriptionDetailDtos.add(dto);
        }
        return prescriptionDetailDtos;
    }

}
