package com.sharework.health.service.impl;

import com.sharework.health.convert.ServiceCategoryConvert;
import com.sharework.health.dto.ServiceCategoryDto;
import com.sharework.health.entity.ServiceCategory;
import com.sharework.health.entity.Status;
import com.sharework.health.repository.ServiceCategoryRepository;
import com.sharework.health.service.ServiceCategoryService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class ServiceCategoryServiceImpl implements ServiceCategoryService {

    private ServiceCategoryRepository serviceCategoryRepository;

    private ServiceCategoryConvert serviceCategoryConvert;

    @Override
    public List<ServiceCategoryDto> findAll() {
        List<ServiceCategory> serviceCategories = serviceCategoryRepository.findAllByStatusLikeActive();
        List<ServiceCategoryDto> serviceCategoryDtos = new ArrayList<>();

        for (ServiceCategory entity: serviceCategories
             ) {
            ServiceCategoryDto dto = serviceCategoryConvert.entityToDto(entity);
            serviceCategoryDtos.add(dto);
        }
        return serviceCategoryDtos;
    }

    @Override
    public ServiceCategoryDto findById(Integer id) {
        ServiceCategory entity = serviceCategoryRepository.findById(id).get();
        if (entity == null){
            return null;
        }
        return serviceCategoryConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(ServiceCategoryDto dto) {
        if (dto == null){
            return false;
        }
        ServiceCategory entity = serviceCategoryConvert.dtoToEntity(dto);
        serviceCategoryRepository.save(entity);
        return true;
    }

    @Override
    public boolean update(Integer id, ServiceCategoryDto dto) {
        ServiceCategory entity = serviceCategoryRepository.findById(id).get();
        if (entity == null){
            return false;
        }
        entity.setName(dto.getName());
        return true;
    }

    @Override
    public boolean delete(Integer id) {
        ServiceCategory entity = serviceCategoryRepository.findById(id).get();
        if (entity == null){
            return false;
        }
        entity.setStatus(Status.DEACTIVE);
        return true;
    }
}
