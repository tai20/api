package com.sharework.health.service.impl;

import com.sharework.health.convert.SlipReturnProductConvert;
import com.sharework.health.convert.SlipReturnProductDetailConvert;
import com.sharework.health.dto.SlipReturnProductDetailDto;
import com.sharework.health.dto.SlipReturnProductDto;
import com.sharework.health.entity.Customer;
import com.sharework.health.entity.SlipReturnProduct;
import com.sharework.health.entity.SlipReturnProductDetail;
import com.sharework.health.repository.CustomerRepository;
import com.sharework.health.repository.SlipReturnProductDetailRepository;
import com.sharework.health.repository.SlipReturnProductRepository;
import com.sharework.health.service.SlipReturnProductService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(rollbackOn = Exception.class)
@AllArgsConstructor
public class SlipReturnProductServiceImpl implements SlipReturnProductService  {

    private SlipReturnProductRepository slipReturnProductRepository;

    private SlipReturnProductDetailRepository slipReturnProductDetailRepository;

    private SlipReturnProductConvert slipReturnProductConvert;

    private SlipReturnProductDetailConvert slipReturnProductDetailConvert;

    private CustomerRepository customerRepository;

    @Override
    public List<SlipReturnProductDto> findAll() {
        List<SlipReturnProduct> slipReturnProducts = slipReturnProductRepository.findAllByStatusLike();
        List<SlipReturnProductDto> slipReturnProductDtos = new ArrayList<>();

        for (SlipReturnProduct entity: slipReturnProducts
             ) {
            SlipReturnProductDto dto = slipReturnProductConvert.entityToDto(entity);
            slipReturnProductDtos.add(dto);
        }
        return slipReturnProductDtos;
    }

    @Override
    public SlipReturnProductDto findById(Integer id) {
        SlipReturnProduct entity = slipReturnProductRepository.findById(id).get();
        if (entity == null){
            return null;
        }
        return slipReturnProductConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(SlipReturnProductDto dto) {
        if (dto == null){
            return false;
        }

        Customer customer = customerRepository.findById(dto.getCustomerDto().getId()).get();
        if (customer != null){
            SlipReturnProduct entity = slipReturnProductConvert.dtoToEntity(dto);
            entity.setCreatedDate(LocalDateTime.now());
            entity.setCustomer(customer);
            slipReturnProductRepository.save(entity);
            return true;
        }
        return false;
    }

    @Override
    public boolean update(Integer id, SlipReturnProductDto dto) {
        SlipReturnProduct entity = slipReturnProductRepository.findById(id).get();
        if (entity == null){
            return false;
        }
        Customer customer = customerRepository.findById(dto.getCustomerDto().getId()).get();
        if (customer != null){
            entity.setCustomer(customer);
            slipReturnProductRepository.save(entity);
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public List<SlipReturnProductDto> findAllSlipReturnProductByCustomer(Integer customerId) {
        List<SlipReturnProduct> slipReturnProducts = slipReturnProductRepository.findAllByCustomerId(customerId);
        List<SlipReturnProductDto> slipReturnProductDtos = new ArrayList<>();

        for (SlipReturnProduct entity: slipReturnProducts
             ) {
            SlipReturnProductDto dto = slipReturnProductConvert.entityToDto(entity);
            slipReturnProductDtos.add(dto);
        }
        return slipReturnProductDtos;
    }

    @Override
    public List<SlipReturnProductDetailDto> findAllSlipReturnProductDetailBySlipReturnProduct(Integer slipReturnProductId) {
        List<SlipReturnProductDetail> slipReturnProductDetails = slipReturnProductDetailRepository.findAllBySlipReturnProductId(slipReturnProductId);
        List<SlipReturnProductDetailDto> slipReturnProductDetailDtos = new ArrayList<>();

        for (SlipReturnProductDetail entity: slipReturnProductDetails
             ) {
                SlipReturnProductDetailDto dto = slipReturnProductDetailConvert.entityToDto(entity);
                slipReturnProductDetailDtos.add(dto);
        }
        return slipReturnProductDetailDtos;
    }
}
