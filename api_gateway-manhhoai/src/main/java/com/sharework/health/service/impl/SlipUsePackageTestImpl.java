package com.sharework.health.service.impl;

import com.sharework.health.convert.SlipUsePackageTestConvert;
import com.sharework.health.dto.IndicationCardDto;
import com.sharework.health.dto.SlipUsePackageTestDto;
import com.sharework.health.entity.*;
import com.sharework.health.repository.CustomerRepository;
import com.sharework.health.repository.MedicalCardRepository;
import com.sharework.health.repository.SlipUsePackageTestRepository;
import com.sharework.health.repository.TestServiceRepository;
import com.sharework.health.service.SlipUsePackageTestService;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class SlipUsePackageTestImpl implements SlipUsePackageTestService {

    private SlipUsePackageTestRepository slipUsePackageTestRepository;

    private CustomerRepository customerRepository;

    private TestServiceRepository testServiceRepository;

    private SlipUsePackageTestConvert slipUsePackageTestConvert;

    private MedicalCardRepository medicalCardRepository;


    @Override
    public List<SlipUsePackageTestDto> findAll() {
        List<SlipUsePackageTest> slipusePackageTests = slipUsePackageTestRepository.findAll();
        List<SlipUsePackageTestDto> slipUsePackageTestDtos = new ArrayList<>();
        for (SlipUsePackageTest entity : slipusePackageTests
        ) {
            if (entity.getEnddate().compareTo(LocalDate.now()) < 0) {
                entity.setStatus(Status.DEACTIVE);
                slipUsePackageTestRepository.save(entity);
            }
            SlipUsePackageTestDto dto = slipUsePackageTestConvert.entityToDto(entity);
            slipUsePackageTestDtos.add(dto);
        }
        return slipUsePackageTestDtos;
    }

    @Override
    public SlipUsePackageTestDto findById(Integer id) {
        SlipUsePackageTest entity = slipUsePackageTestRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return slipUsePackageTestConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(SlipUsePackageTestDto dto) {
        if (dto == null) {
            return false;
        }
        TestService testService = testServiceRepository.findById(dto.getTestServiceDto().getTestservice_id()).get();

        Customer customer = customerRepository.findById(dto.getCustomerDto().getId()).get();

        if (testService != null && customer != null) {
            SlipUsePackageTest slipusePackageTest = slipUsePackageTestConvert.dtoToEntity(dto);
            slipusePackageTest.setId(null);
            slipusePackageTest.setTestService(testService);
            slipusePackageTest.setCustomer(customer);
            slipusePackageTest.setStartdate(LocalDate.now());
            slipusePackageTest.setEnddate(LocalDate.now().plusYears(1));
            slipUsePackageTestRepository.save(slipusePackageTest);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean update(Integer id, SlipUsePackageTestDto dto) {
        if (dto == null) {
            return false;
        }

        SlipUsePackageTest slipUsePackageTest = slipUsePackageTestRepository.findById(id).get();

        Customer customer = customerRepository.findById(dto.getCustomerDto().getId()).get();

        TestService testService = testServiceRepository.findById(dto.getTestServiceDto().getTestservice_id()).get();

        if (testService != null && customer != null) {
            if (slipUsePackageTest != null) {
                slipUsePackageTest.setEnddate(dto.getEnddate());
                slipUsePackageTest.setNumberofuse(dto.getNumberofuse());
                slipUsePackageTest.setPrice(dto.getPrice());
                slipUsePackageTestRepository.save(slipUsePackageTest);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        SlipUsePackageTest entity = slipUsePackageTestRepository.findById(id).get();
        if (entity == null) {
            return false;
        }
        slipUsePackageTestRepository.deleteById(id);
        return true;
    }

    @Override
    public List<SlipUsePackageTestDto> findAllByCustomer(Integer customerId) {
        List<SlipUsePackageTest> slipusePackageTests = slipUsePackageTestRepository.findAllByCustomer(customerId);
        List<SlipUsePackageTestDto> slipUsePackageTestDtos = new ArrayList<>();
        for (SlipUsePackageTest entity : slipusePackageTests
        ) {
            SlipUsePackageTestDto dto = slipUsePackageTestConvert.entityToDto(entity);
            slipUsePackageTestDtos.add(dto);
        }
        return slipUsePackageTestDtos;
    }

    @Override
    public boolean activeSlipUsePackageTest(Integer slipusepackagetestId) {
        SlipUsePackageTest slipUsePackageTest = slipUsePackageTestRepository.findById(slipusepackagetestId).get();

        if (slipUsePackageTest == null) {
            return false;
        }
        slipUsePackageTest.setStatus(Status.ACTIVE);
        slipUsePackageTestRepository.save(slipUsePackageTest);
        return true;
    }

    @Override
    public boolean updateExtensionTime(Integer slipusepackagetestId, SlipUsePackageTestDto dto) {
        if (dto == null) {
            return false;
        }
        SlipUsePackageTest slipUsePackageTest = slipUsePackageTestRepository.findById(slipusepackagetestId).get();
        try {
            if (dto.getEnddate() != null) {
                slipUsePackageTest.setEnddate(dto.getEnddate());
            }
            slipUsePackageTestRepository.save(slipUsePackageTest);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public SlipUsePackageTestDto getSlipUsePackageTestByMedicalCardId(Integer medicalCardId) {
        SlipUsePackageTest entity = slipUsePackageTestRepository.getSlipUsePackageTestByMedicalCardId(medicalCardId);
        if (entity == null) {
            return null;
        }
        return slipUsePackageTestConvert.entityToDto(entity);
    }

    @Override
    public boolean updateSlipUseAfterAddTotalCost(Integer medicalCardId, SlipUsePackageTestDto dto) {
        if (dto == null) {
            return false;
        }
        MedicalCard medicalCard = medicalCardRepository.findById(medicalCardId).get();

        SlipUsePackageTest slipUsePackageTest = null;
        if(medicalCard.getSlipUsePackageTest()!=null){
            slipUsePackageTest = slipUsePackageTestRepository.findById(medicalCard.getSlipUsePackageTest().getId()).get();
        }else{
            slipUsePackageTest = null;
        }

        if (slipUsePackageTest != null && slipUsePackageTest.getNumberofuse() == null) {
            BigDecimal finalmoney = slipUsePackageTest.getPrice().subtract(medicalCard.getTestService().getPrice());
            slipUsePackageTest.setPrice(finalmoney);
            slipUsePackageTestRepository.save(slipUsePackageTest);
            return true;
        }
        return false;
    }
}
