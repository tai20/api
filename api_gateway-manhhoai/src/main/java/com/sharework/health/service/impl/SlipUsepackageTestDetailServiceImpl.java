package com.sharework.health.service.impl;

import com.sharework.health.convert.SlipUsepackageTestDetailConvert;
import com.sharework.health.dto.SlipUsePackageTestDetailDto;
import com.sharework.health.entity.*;
import com.sharework.health.repository.CategoryIndicationCardRepository;
import com.sharework.health.repository.SlipUsePackageTestDetailRepository;
import com.sharework.health.repository.SlipUsePackageTestRepository;
import com.sharework.health.service.SlipUsePackageTestDetailService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class SlipUsepackageTestDetailServiceImpl implements SlipUsePackageTestDetailService {

    private SlipUsePackageTestDetailRepository slipUsePackageTestDetailRepository;

    private SlipUsePackageTestRepository slipUsePackageTestRepository;

    private CategoryIndicationCardRepository categoryIndicationCardRepository;

    private SlipUsepackageTestDetailConvert slipUsepackageTestDetailConvert;


    @Override
    public List<SlipUsePackageTestDetailDto> findAll() {
        List<SlipUsePackageTestDetail> slipusePackageTestDetails = slipUsePackageTestDetailRepository.findAll();
        List<SlipUsePackageTestDetailDto> slipUsePackageTestDetailDtos = new ArrayList<>();
        for (SlipUsePackageTestDetail entity : slipusePackageTestDetails
        ) {
            SlipUsePackageTestDetailDto dto = slipUsepackageTestDetailConvert.entityToDto(entity);
            slipUsePackageTestDetailDtos.add(dto);
        }
        return slipUsePackageTestDetailDtos;
    }

    @Override
    public SlipUsePackageTestDetailDto findById(Integer id) {
        SlipUsePackageTestDetail entity = slipUsePackageTestDetailRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return slipUsepackageTestDetailConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(SlipUsePackageTestDetailDto dto) {
        if (dto == null) {
            return false;
        }
        SlipUsePackageTest slipUsePackageTest = slipUsePackageTestRepository.findById(dto.getSlipusePackageTestDto()
                .getId()).get();

        CategoryIndicationCard categoryIndicationCard = categoryIndicationCardRepository.findById(dto.getCategoryIndicationCardDto()
                .getId()).get();

        if (slipUsePackageTest != null && categoryIndicationCard != null) {
            SlipUsePackageTestDetail slipusePackageTestDetail = slipUsepackageTestDetailConvert.dtoToEntity(dto);
            slipusePackageTestDetail.setId(null);
            slipusePackageTestDetail.setSlipusePackageTest(slipUsePackageTest);
            slipusePackageTestDetail.setCategoryIndicationCard(categoryIndicationCard);
            slipUsePackageTestDetailRepository.save(slipusePackageTestDetail);
            return true;
        }
        return false;
    }

    @Override
    public boolean update(Integer id, SlipUsePackageTestDetailDto dto) {
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }


    @Override
    public List<SlipUsePackageTestDetailDto> getSlipUsePackageTestDetailBySlipUsePackageId(Integer slipUsePackageTestId) {
        List<SlipUsePackageTestDetail> slipUsePackageTestDetails = slipUsePackageTestDetailRepository.getSlipUsePackageTestDetailBySlipUsePackageId(slipUsePackageTestId);
        List<SlipUsePackageTestDetailDto> slipUsePackageTestDetailDtos = new ArrayList<>();

        for (SlipUsePackageTestDetail entity : slipUsePackageTestDetails
        ) {
            SlipUsePackageTestDetailDto dto = slipUsepackageTestDetailConvert.entityToDto(entity);
            slipUsePackageTestDetailDtos.add(dto);
        }
        return slipUsePackageTestDetailDtos;
    }
}
