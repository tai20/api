package com.sharework.health.service.impl;

import com.sharework.health.convert.*;
import com.sharework.health.dto.*;
import com.sharework.health.entity.*;
import com.sharework.health.repository.*;
import com.sharework.health.service.StatisticalService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Service
@Transactional(rollbackOn = Exception.class)
@AllArgsConstructor
public class StatisticalServiceImpl implements StatisticalService {

    private CustomerRepository customerRepository;

    private CustomerConvert customerConvert;

    private AdvisoryRepository advisoryRepository;

    private AdvisoryConvert advisoryConvert;

    private ExaminationCardRepository examinationCardRepository;

    private ExaminationCardDetailRepository examinationCardDetailRepository;

    private UserRepository userRepository;

    private ExaminationCardConvert examinationCardConvert;

    private OrderRepository orderRepository;

    private OrderDetailProductRepository orderDetailProductRepository;

    private OrderConvert orderConvert;

    private ProductRepository productRepository;

    private ProductConvert productConvert;

    @Override
    public List<CustomerDto> statisticalCustomerByCreatedDate(LocalDateTime startDate, LocalDateTime endDate) {
        List<Customer> customers = customerRepository.statisticalCustomerByCreatedDateIsBetween(startDate,endDate);
        List<CustomerDto> customerDtos = new ArrayList<>();
        for (Customer entity: customers
             ) {
            CustomerDto dto = customerConvert.entityToDto(entity);
            customerDtos.add(dto);
        }
        return customerDtos;
    }

    @Override
    public List<AdvisoryDto> statisticalAdvisoryByCreatedDate(LocalDateTime startDate, LocalDateTime endDate) {
        List<Advisory> advisories = advisoryRepository.statisticalAdvisoryByCreatedDateIsBetween(startDate, endDate);
        List<AdvisoryDto> advisoryDtos = new ArrayList<>();
        for (Advisory entity: advisories
             ) {
            AdvisoryDto dto = advisoryConvert.entityToDto(entity);
            advisoryDtos.add(dto);
        }
        return advisoryDtos;
    }

    @Override
    public List<ExaminationCardDto> statisticalExaminationCardByDateOfExamination(LocalDateTime startDate, LocalDateTime endDate) {
        List<ExaminationCard> examinationCards = examinationCardRepository.statisticalExaminationCardByDateOfExaminationIsBetween(startDate,endDate);
        List<ExaminationCardDto> examinationCardDtos = new ArrayList<>();
        for (ExaminationCard entity: examinationCards
             ) {
            ExaminationCardDto dto = examinationCardConvert.entityToDto(entity);
            examinationCardDtos.add(dto);
        }
        return examinationCardDtos;
    }

    @Override
    public List<OrderDto> statisticalOrderByOrderDate(LocalDateTime startDate, LocalDateTime endDate) {
        List<Order> orders = orderRepository.statisticalOrderByOrderDateIsBetween(startDate, endDate);
        List<OrderDto> orderDtos = new ArrayList<>();
        for (Order entity: orders
        ) {
            OrderDto dto = orderConvert.entityToDto(entity);
            orderDtos.add(dto);
        }
        return orderDtos;
    }

    @Override
    public List<SelectCustomerIdDebtMappingDto> statisticalCustomerHaveSumDebtByMonth(Integer month, Integer year) {
        if (month == null || year == null){
            return null;
        }
        List<SelectCustomerIdDebtDto> list = orderRepository.findAllCustomerHaveDebtByMonthOfOrderDate(month, year);
        List<SelectCustomerIdDebtMappingDto> dtos = new ArrayList<>();

        for (SelectCustomerIdDebtDto entity: list
        ) {
            Customer customer = customerRepository.findById(entity.getId()).get();
            SelectCustomerIdDebtMappingDto dto = new SelectCustomerIdDebtMappingDto()
                    .setName(customer.getName())
                    .setPhoneNumber(customer.getPhoneNumber())
                    .setAddress(customer.getAddress())
                    .setDebtMoney(entity.getDebtMoney());
            dtos.add(dto);
        }
        return dtos;
    }

    @Override
    public List<SelectCustomerIdTotalMappingDto> statisticalCustomerHaveSumTotalByMonth(Integer month, Integer year) {
        if (month == null || year == null){
            return null;
        }
        List<SelectCustomerIdTotalDto> list = orderRepository.findAllCustomerHaveTotalByMonthOfOrderDate(month,year);
        List<SelectCustomerIdTotalMappingDto> dtos = new ArrayList<>();

        for (SelectCustomerIdTotalDto entity: list
        ) {
            Customer customer = customerRepository.findById(entity.getId()).get();
            SelectCustomerIdTotalMappingDto dto = new SelectCustomerIdTotalMappingDto()
                    .setName(customer.getName())
                    .setPhoneNumber(customer.getPhoneNumber())
                    .setAddress(customer.getAddress())
                    .setTotal(entity.getTotal());
            dtos.add(dto);
        }
        return dtos;
    }



    @Override
    public List<SelectCountExaminationQueryMappingDto> statisticalUserWithRoleDoctorAndCountExaminationByMonthAndYear(Integer month, Integer year) {
        List<SelectCountExaminationUserDto> list = examinationCardDetailRepository.findAllUserWithRoleDoctorAndCountExaminationByMonthAndYear(month, year);
        List<SelectCountExaminationQueryMappingDto> dtos = new ArrayList<>();
        for (SelectCountExaminationUserDto entity: list
             ) {
            User user = userRepository.findById(entity.getId()).get();
            SelectCountExaminationQueryMappingDto dto = new SelectCountExaminationQueryMappingDto()
                    .setName(user.getName())
                    .setPhoneNumber(user.getPhoneNumber())
                    .setAddress(user.getAddress())
                    .setClinicName(user.getClinic().getName())
                    .setCountExamination(entity.getCountExamination());
            dtos.add(dto);
        }
        return dtos;
    }

    @Override
    public List<SelectCountExaminationQueryMappingDto> statisticalUserWithRoleTechnicianAndCountExaminationByMonthAndYear(Integer month, Integer year) {
        List<SelectCountExaminationUserDto> list = examinationCardDetailRepository.findAllUserWithRoleTechnicianAndCountExaminationByMonthAndYear(month, year);
        List<SelectCountExaminationQueryMappingDto> dtos = new ArrayList<>();
        for (SelectCountExaminationUserDto entity: list
        ) {
            User user = userRepository.findById(entity.getId()).get();
            SelectCountExaminationQueryMappingDto dto = new SelectCountExaminationQueryMappingDto()
                    .setName(user.getName())
                    .setPhoneNumber(user.getPhoneNumber())
                    .setAddress(user.getAddress())
                    .setClinicName(user.getClinic().getName())
                    .setCountExamination(entity.getCountExamination());
            dtos.add(dto);
        }
        return dtos;
    }

    @Override
    public List<SelectCountExaminationQueryMappingDto> statisticalCustomerAndCountExaminationByMonthAndYear(Integer month, Integer year) {
//        List<SelectCountExaminationCustomerDto> list = examinationCardRepository.findAllCustomerAndCountExaminationByMonthAndYear(month,year);
//        List<SelectCountExaminationQueryMappingDto> dtos = new ArrayList<>();
//        for (SelectCountExaminationCustomerDto entity: list
//        ) {
//            User user = userRepository.findById(entity.getId()).get();
//            SelectCountExaminationQueryMappingDto dto = new SelectCountExaminationQueryMappingDto()
//                    .setName(user.getName())
//                    .setPhoneNumber(user.getPhoneNumber())
//                    .setAddress(user.getAddress())
//                    .setClinicName(user.getClinic().getName())
//                    .setCountExamination(entity.getCountExamination());
//            dtos.add(dto);
//        }
//        return dtos;
        List<?> list = examinationCardRepository.findAllCustomerAndCountExaminationByMonthAndYear(month,year);
        List<SelectCountExaminationQueryMappingDto> dtos = new ArrayList<>();
        for (Object o: list
        ) {
            Object[] temp = (Object[]) o;
            //User user = userRepository.findById(entity.getId()).get();
            SelectCountExaminationQueryMappingDto dto = new SelectCountExaminationQueryMappingDto()
                    .setName(temp[1].toString())
                    .setPhoneNumber(temp[2].toString())
                    .setAddress(temp[3].toString())
                    .setClinicName(temp[5].toString())
                    .setCountExamination(Long.parseLong(temp[6].toString()));
            dtos.add(dto);
        }
        return dtos;
    }

    @Override
    public List<CustomerDto> statisticalCustomerHaveBirthDateByMonth(Integer month) {
        List<Customer> customers = customerRepository.findAllByBirthDateWithMonth(month);
        List<CustomerDto> customerDtos = new ArrayList<>();

        for (Customer entity: customers
             ) {
            CustomerDto dto = customerConvert.entityToDto(entity);
            customerDtos.add(dto);
        }
        return customerDtos;
    }

    @Override
    public List<CountProductSoldByMonthDto> statisticalCountProductSoldByMonth(Integer month, Integer year) {
        List<?> list = orderDetailProductRepository.CountProductSoldByMonth(month,year);
//        Map<Product, Object[]> map = new TreeMap<>(new Comparator<Product>() {
//            @Override
//            public int compare(Product o1, Product o2) {
//                return o1.getName().compareToIgnoreCase(o2.getName());
//            }
//        });
        Map<ProductDto, Object[]> map = new HashMap<>();
        List<CountProductSoldByMonthDto> dtos = new ArrayList<>();
        //List<CountProductSoldByMonthDto> listDto = new ArrayList<>();
        for (Object o: list
             ) {
            Object[] temp = (Object[]) o;
            //Object[] value = new Object[] {temp[1]};
//            map.put(productConvert.entityToDto(productRepository.findById(Integer.parseInt(temp[0].toString())).get()), value);
            CountProductSoldByMonthDto dto = new CountProductSoldByMonthDto()
                    .setProductDto(productConvert.entityToDto(productRepository.findById(Integer.parseInt(temp[0].toString())).get()))
                    .setCountProduct(Long.parseLong(temp[1].toString()));
            dtos.add(dto);
        }
        return dtos;
    }

    @Override
    public List<CountExaminationOfDoctorByMonthAndYearDto> statisticalCountExaminationOfDoctorByMonthAndYear(Integer month, Integer year) {
        //List<CountExaminationOfDoctorByMonthAndYearDto> dtos = new ArrayList<>();
        List<?> list = examinationCardRepository.statisticalCountExaminationOfDoctorByMonthAndYear(month, year);
        List<CountExaminationOfDoctorByMonthAndYearDto> dtos = new ArrayList<>();
        if (list.isEmpty() || list == null){
            return null;
        }
        for (Object o: list
             ) {
            Object[] temp = (Object[]) o;
            CountExaminationOfDoctorByMonthAndYearDto dto = new CountExaminationOfDoctorByMonthAndYearDto();
            dto.setUserId(Integer.parseInt(temp[0].toString()));
            dto.setUserName(temp[1].toString());
            dto.setClinicId(Integer.parseInt(temp[2].toString()));
            dto.setClinicName(temp[3].toString());
            dto.setServiceId(Integer.parseInt(temp[4].toString()));
            dto.setServiceName(temp[5].toString());
            dto.setCustomerId(Integer.parseInt(temp[6].toString()));
            dto.setCustomerName(temp[7].toString());
            //dto.setDateOfExamination(LocalDate.parse(temp[8].toString()));
            dto.setCountExamination(Long.parseLong(temp[8].toString()));
            dtos.add(dto);
        }
        return dtos;
    }

    @Override
    public List<CountExaminationOfTechnicianByMonthAndYearDto> statisticalCountExaminationOfTechnicianByMonthAndYear(Integer month, Integer year) {
        List<?> list = examinationCardRepository.statisticalCountExaminationOfTechnicianByMonthAndYear(month, year);
        List<CountExaminationOfTechnicianByMonthAndYearDto> dtos = new ArrayList<>();
        if (list.isEmpty() || list == null){
            return null;
        }
        for (Object o: list
        ) {
            Object[] temp = (Object[]) o;
            CountExaminationOfTechnicianByMonthAndYearDto dto = new CountExaminationOfTechnicianByMonthAndYearDto();
            dto.setUserId(Integer.parseInt(temp[0].toString()));
            dto.setUserName(temp[1].toString());
            dto.setClinicId(Integer.parseInt(temp[2].toString()));
            dto.setClinicName(temp[3].toString());
            dto.setServiceId(Integer.parseInt(temp[4].toString()));
            dto.setServiceName(temp[5].toString());
            dto.setCustomerId(Integer.parseInt(temp[6].toString()));
            dto.setCustomerName(temp[7].toString());
            dto.setCountExamination(Long.parseLong(temp[8].toString()));
            dtos.add(dto);
        }
        return dtos;
    }

    @Override
    public List<CountExaminationOfServiceByClinicDto> statisticalCountExaminationOfServiceByMonthAndYearAndClinic(Integer month, Integer year, Integer clinicId) {
        List<?> list = examinationCardRepository.statisticalCountExaminationOfServiceByMonthAndYearAndClinic(month, year, clinicId);
        List<CountExaminationOfServiceByClinicDto> dtos = new ArrayList<>();
        if (list.isEmpty() || list == null){
            return null;
        }
        for (Object o: list
        ) {
            Object[] temp = (Object[]) o;
            CountExaminationOfServiceByClinicDto dto = new CountExaminationOfServiceByClinicDto();
            dto.setServiceId(Integer.parseInt(temp[0].toString()));
            dto.setServiceName(temp[1].toString());
            dto.setClinicId(Integer.parseInt(temp[2].toString()));
            dto.setClinicName(temp[3].toString());
            dto.setCountExamination(Long.parseLong(temp[4].toString()));
            dtos.add(dto);
        }
        return dtos;
    }

    @Override
    public List<CountConsumableProductByMonthAndYearDto> statisticalCountConsumableProductByMonthAndYear(Integer month, Integer year) {
        List<?> list = examinationCardRepository.statisticalCountConsumableProductByMonthAndYear(month, year);
        List<CountConsumableProductByMonthAndYearDto> dtos = new ArrayList<>();
        if (list.isEmpty() || list == null){
            return null;
        }
        for (Object o: list
        ) {
            Object[] temp = (Object[]) o;

            CountConsumableProductByMonthAndYearDto dto = new CountConsumableProductByMonthAndYearDto();
            dto.setServiceId(Integer.parseInt(temp[0].toString()));
            dto.setProductId(Integer.parseInt(temp[1].toString()));
            Product product = productRepository.findById(Integer.parseInt(temp[1].toString())).get();
            dto.setProductName(temp[2].toString());
            dto.setUnit(temp[3].toString());
            dto.setClinicId(Integer.parseInt(temp[4].toString()));
            dto.setClinicName(temp[5].toString());
            if (product.getCapacity() <= 0){
                dto.setCountConsumableProduct(Long.parseLong(temp[6].toString()));
            }else {
                int countConsumableProduct = Integer.parseInt(temp[6].toString()) / product.getCapacity();
                dto.setCountConsumableProduct(Long.parseLong(String.valueOf(countConsumableProduct)));
            }
            dtos.add(dto);
        }
        return dtos;
    }

    @Override
    public List<CountCustomerOfServiceByMonthAndYearDto> statisticalCountCustomerOfServiceByMonthAndYear(Integer month, Integer year) {
        List<?> list = examinationCardRepository.statisticalCountCustomerOfServiceByMonthAndYear(month, year);
        List<CountCustomerOfServiceByMonthAndYearDto> dtos = new ArrayList<>();
        if (list.isEmpty() || list == null){
            return null;
        }
        for (Object o: list
        ) {
            Object[] temp = (Object[]) o;
            CountCustomerOfServiceByMonthAndYearDto dto = new CountCustomerOfServiceByMonthAndYearDto();
            dto.setServiceId(Integer.parseInt(temp[0].toString()));
            dto.setServiceName(temp[1].toString());
            dto.setClinicId(Integer.parseInt(temp[2].toString()));
            dto.setClinicName(temp[3].toString());
            dto.setCountCustomer(Long.parseLong(temp[4].toString()));
            dtos.add(dto);
        }
        return dtos;
    }

    @Override
    public List<CountServiceExaminationOfCustomerByMonthAndYearDto> statisticalCountServiceExaminationOfCustomerByMonthAndYear(Integer month, Integer year) {
        List<?> list = examinationCardRepository.statisticalCountServiceExaminationOfCustomerByMonthAndYear(month, year);
        List<CountServiceExaminationOfCustomerByMonthAndYearDto> dtos = new ArrayList<>();
        if (list.isEmpty() || list == null){
            return null;
        }
        for (Object o: list
        ) {
            Object[] temp = (Object[]) o;
            CountServiceExaminationOfCustomerByMonthAndYearDto dto = new CountServiceExaminationOfCustomerByMonthAndYearDto();
            dto.setCustomerId(Integer.parseInt(temp[0].toString()));
            dto.setCustomerName(temp[1].toString());
            dto.setServiceId(Integer.parseInt(temp[2].toString()));
            dto.setServiceName(temp[3].toString());
            dto.setClinicId(Integer.parseInt(temp[4].toString()));
            dto.setClinicName(temp[5].toString());
            dto.setCountService(Long.parseLong(temp[6].toString()));
            dtos.add(dto);
        }
        return dtos;
    }

    @Override
    public List<ProductDto> findAllProductImportFromTo(LocalDate from, LocalDate to) {
        List<Product> products = productRepository.findAllProductImportFromTo(from,to);
        List<ProductDto> productDtos = new ArrayList<>();

        for (Product entity: products
             ) {
            ProductDto dto = productConvert.entityToDto(entity);
            productDtos.add(dto);
        }
        return productDtos;
    }

    @Override
    public List<ProductDto> findAllProductExportFromTo(LocalDate from, LocalDate to) {
        List<Product> products = productRepository.findAllProductExportFromTo(from,to);
        List<ProductDto> productDtos = new ArrayList<>();

        for (Product entity: products
        ) {
            ProductDto dto = productConvert.entityToDto(entity);
            productDtos.add(dto);
        }
        return productDtos;
    }
}
