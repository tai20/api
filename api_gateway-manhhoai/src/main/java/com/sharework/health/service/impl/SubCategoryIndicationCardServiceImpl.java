package com.sharework.health.service.impl;

import com.sharework.health.convert.SubCategoryIndicationCardConvert;
import com.sharework.health.dto.SubCategoryIndicationCardDto;
import com.sharework.health.entity.SubCategoryIndicationCard;
import com.sharework.health.repository.SubCategoryIndicationCardRepository;
import com.sharework.health.service.SubCategoryIndicationCardService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class SubCategoryIndicationCardServiceImpl implements SubCategoryIndicationCardService {

    private SubCategoryIndicationCardRepository subCategoryIndicationCardRepository;

    private SubCategoryIndicationCardConvert subCategoryIndicationCardConvert;

    @Override
    public List<SubCategoryIndicationCardDto> findAll() {
        List<SubCategoryIndicationCard> subCategoryIndicationCards = subCategoryIndicationCardRepository.findAll();
        List<SubCategoryIndicationCardDto> subCategoryIndicationCardDtos = new ArrayList<>();
        for (SubCategoryIndicationCard entity : subCategoryIndicationCards
        ) {
            SubCategoryIndicationCardDto dto = subCategoryIndicationCardConvert.entityToDto(entity);
            subCategoryIndicationCardDtos.add(dto);
        }
        return subCategoryIndicationCardDtos;
    }

    @Override
    public SubCategoryIndicationCardDto findById(Integer id) {
        SubCategoryIndicationCard entity = subCategoryIndicationCardRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return subCategoryIndicationCardConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(SubCategoryIndicationCardDto dto) {
        if (dto == null) {
            return false;
        }
        try {
            SubCategoryIndicationCard entity = subCategoryIndicationCardConvert.dtoToEntity(dto);
            entity.setId(null);
            subCategoryIndicationCardRepository.save(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean update(Integer id, SubCategoryIndicationCardDto dto) {
        SubCategoryIndicationCard entity = subCategoryIndicationCardRepository.findById(id).get();
        if (entity == null) {
            return false;
        }
        try {
            entity.setName(dto.getName());
            entity.setDescription(dto.getDescription());
            subCategoryIndicationCardRepository.save(entity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        try {
            SubCategoryIndicationCard entity = subCategoryIndicationCardRepository.findById(id).get();
            if (entity != null) {
                subCategoryIndicationCardRepository.deleteById(id);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
