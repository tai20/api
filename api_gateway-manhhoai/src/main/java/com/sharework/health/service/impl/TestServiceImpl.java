package com.sharework.health.service.impl;

import com.sharework.health.convert.TestServiceConvert;
import com.sharework.health.dto.TestServiceDto;
import com.sharework.health.entity.TestService;
import com.sharework.health.repository.TestServiceRepository;
import com.sharework.health.service.TestServiceService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class TestServiceImpl implements TestServiceService {

    private TestServiceRepository testServiceRepository;

    private TestServiceConvert testServiceConvert;

    @Override
    public List<TestServiceDto> findAll() {
        List<TestService> testServices = testServiceRepository.findAll();
        List<TestServiceDto> testServiceDtos = new ArrayList<>();
        for (TestService entity : testServices
        ) {
            TestServiceDto dto = testServiceConvert.entityToDto(entity);
            testServiceDtos.add(dto);
        }
        return testServiceDtos;
    }

    @Override
    public TestServiceDto findById(Integer id) {
        TestService entity = testServiceRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return testServiceConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(TestServiceDto dto) {
        if (dto == null) {
            return false;
        }
        TestService testService = testServiceConvert.dtoToEntity(dto);
        testService.setTestservice_id(null);
        testServiceRepository.save(testService);
        return true;
    }

    @Override
    public boolean update(Integer id, TestServiceDto dto) {
        TestService entity = testServiceRepository.findById(id).get();
        if (entity == null) {
            return false;
        }
        entity.setNameservice(dto.getNameservice());
        entity.setPrice(dto.getPrice());
        testServiceRepository.save(entity);
        return true;
    }

    @Override
    public boolean delete(Integer id) {
        TestService entity = testServiceRepository.findById(id).get();
        if (entity == null) {
            return false;
        }
        testServiceRepository.deleteById(id);
        return true;
    }

    @Override
    public TestServiceDto searchTestServiceById(String id) {
        return null;
    }
}
