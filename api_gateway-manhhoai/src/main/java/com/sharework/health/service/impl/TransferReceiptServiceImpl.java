package com.sharework.health.service.impl;

import com.sharework.health.convert.ClinicStockConvert;
import com.sharework.health.convert.TransferReceiptConvert;
import com.sharework.health.convert.TransferReceiptDetailsConvert;
import com.sharework.health.dto.*;
import com.sharework.health.entity.*;
import com.sharework.health.repository.*;
import com.sharework.health.service.TransferReceiptService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class TransferReceiptServiceImpl implements TransferReceiptService {

    private TransferReceiptConvert transferReceiptConvert;

    private TransferReceiptRepository transferReceiptRepository;

    private TransferReceiptDetailsConvert transferReceiptDetailsConvert;

    private ClinicStockRepository clinicStockRepository;

    private ClinicStockConvert clinicStockConvert;

    private ClinicRepository clinicRepository;

    private UserRepository userRepository;

    private TransferReceiptDetailsRepository transferReceiptDetailsRepository;



    @Override
    public List<TransferReceiptDto> findAll(){

        List<TransferReceipt> transferReceipts = transferReceiptRepository.findAllSortDateDESC();
        List<TransferReceiptDto> transferReceiptDtos = new ArrayList<>();
        for (TransferReceipt entity:transferReceipts){
            TransferReceiptDto dto = transferReceiptConvert.entityToDto(entity);
            transferReceiptDtos.add(dto);
        }

        return transferReceiptDtos;
    }


    @Override
    public TransferReceiptDto findById(Integer id){
        Optional<TransferReceipt> founDto = transferReceiptRepository.findById(id);
        if (founDto.isPresent()){
            TransferReceipt entity = transferReceiptRepository.findById(id).get();
            return transferReceiptConvert.entityToDto(entity);
        }
        return null;
    }


    @Override
    public TransferReceiptDto insertTransferReceipt(TransferReceiptDto dto){
        TransferReceipt entity = transferReceiptConvert.dtoToEntity(dto);
        User user = userRepository.findById(dto.getUserDto().getId()).get();
        Clinic clinic_sending = clinicRepository.findById(dto.getClinicSendingDto().getId()).get();
        Clinic clinic_receiving = clinicRepository.findById(dto.getClinicRecevingDto().getId()).get();

        if (clinic_receiving == null || clinic_sending == null || user == null){
            return null;
        } else{
            entity.setId(null);
            entity.setUser(user);
            entity.setClinic_sending(clinic_sending);
            entity.setClinic_receiving(clinic_receiving);
            entity.setDateexport(LocalDateTime.now());
            entity.setDateimport(null);
            entity.setStatus(TransferStatus.MOVING);
        }
        TransferReceipt result = transferReceiptRepository.save(entity);
        return transferReceiptConvert.entityToDto(result);
    }


    @Override
    public TransferReceiptDetailsDto insertTransferReceiptDetail(TransferReceiptDetailsDto dto){

        TransferReceiptDetails entity = transferReceiptDetailsConvert.dtoToEntity(dto);
        TransferReceipt transferReceipt = transferReceiptRepository.findById(dto.getTransferReceiptDto().getId()).get();
        ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinicStockDto().getId()).get();
        List<TransferReceiptDetails> transferReceiptDetails = transferReceiptDetailsRepository.getTransferReceiptDetailsByClinicStockIdReceiptId(dto.getTransferReceiptDto().getId(), dto.getClinicStockDto().getId());

        if (transferReceipt.getClinic_sending().getId() != clinicStock.getClinic().getId() || dto.getNumber_product_transfer()<=0) return null;
        if (transferReceipt == null || clinicStock == null || transferReceipt.getStatus() != TransferStatus.MOVING || clinicStock.getQuantity()<dto.getNumber_product_transfer()){
            return null;
        } else
        if (transferReceiptDetails.isEmpty()){
            entity.setNumber_product_transfer(dto.getNumber_product_transfer());
        } else{
            for (TransferReceiptDetails itemSearch:transferReceiptDetails){
                entity.setNumber_product_transfer(dto.getNumber_product_transfer() + dto.getNumber_product_transfer());
            }
        }
        entity.setTransferReceipt(transferReceipt);
        entity.setClinicStock(clinicStock);
        clinicStock.setQuantity(clinicStock.getQuantity() - dto.getNumber_product_transfer());
        clinicStockRepository.save(clinicStock);
        TransferReceiptDetails result = transferReceiptDetailsRepository.save(entity);
        return transferReceiptDetailsConvert.entityToDto(result);

    }


    @Override
    public TransferReceiptDto confirmRecevied(Integer id){

        //Thay đổi trạng thái phiếu
        TransferReceipt entity = transferReceiptRepository.findById(id).get();
        if (entity == null || entity.getStatus() != TransferStatus.MOVING){
            return  null;
        } else {
            entity.setDateimport(LocalDateTime.now());
            entity.setStatus(TransferStatus.RECEIVED);
            transferReceiptRepository.save(entity);
        }

        List<GetReceivingClinicStockDto> getReceivingClinicStockDtoList = transferReceiptDetailsRepository.GetClinicStockReceivingId(id);

        for(GetReceivingClinicStockDto dto:getReceivingClinicStockDtoList){
            ClinicStockIdQueryDto clinicId = transferReceiptDetailsRepository.getClinicStockByProductClinicId(dto.getProduct_id(), dto.getClinic_receving());

            ClinicStock entitydetail = clinicStockRepository.findById(clinicId.getClinic_stock_id()).get();
            entitydetail.setQuantity(entitydetail.getQuantity()+ dto.getNumber_product_transfer());
            clinicStockRepository.save(entitydetail);
        }

        return transferReceiptConvert.entityToDto(entity);
    }


    @Override
    public List<TransferReceiptDetailsQueryDto> getAllTransferReceiptDetailByReceiptid(Integer id){
        List<TransferReceiptDetailsQueryDto> transferReceiptDetailsQueryDtos = transferReceiptDetailsRepository.getAllTransferReceiptDetailByReceiptid(id);
        return transferReceiptDetailsQueryDtos;
    }


    @Override
    public boolean deleteTransferReceipt(Integer id){
        TransferReceipt transferReceipt = transferReceiptRepository.findById(id).get();

        if (transferReceipt.getStatus() != TransferStatus.MOVING){
            return false;
        }
        List<TransferReceiptDetailsQueryDto> transferReceiptDetailsQueryDtos = transferReceiptDetailsRepository.getAllTransferReceiptDetailByReceiptid(id);
        if (!transferReceiptDetailsQueryDtos.isEmpty()) {
            for (TransferReceiptDetailsQueryDto dto : transferReceiptDetailsQueryDtos) {
                ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinic_stock_id()).get();
                clinicStock.setQuantity(clinicStock.getQuantity() + dto.getNumber_product_transfer());
                clinicStockRepository.save(clinicStock);
            }
        }
        transferReceipt.setStatus(TransferStatus.DELETED);
        transferReceiptRepository.save(transferReceipt);
        return true;
    }
    @Override
    public boolean deleteTransferReceiptDetail(Integer receipt_id, Integer clinic_stock_id){

        TransferReceipt transferReceipt = transferReceiptRepository.findById(receipt_id).get();
        ClinicStock clinicStock = clinicStockRepository.findById(clinic_stock_id).get();

        if (transferReceipt.getStatus() != TransferStatus.MOVING){
            return false;
        }
        List<TransferReceiptDetails> transferReceiptDetails = transferReceiptDetailsRepository.getTransferReceiptDetailsByClinicStockIdReceiptId(receipt_id, clinic_stock_id);
        if (transferReceiptDetails.isEmpty()) return false;
        for(TransferReceiptDetails entity:transferReceiptDetails){
            clinicStock.setQuantity(clinicStock.getQuantity() + entity.getNumber_product_transfer());
            clinicStockRepository.save(clinicStock);
            transferReceiptDetailsRepository.delete(entity);
        }
        return true;

    }

    @Override
    public TransferReceiptDetailsQueryDto updateTransferReceiptDetail(TransferReceiptDetailsQueryDto dto){
        TransferReceipt transferReceipt = transferReceiptRepository.findById(dto.getReceipt_id()).get();
        List<TransferReceiptDetails> transferReceiptDetails = transferReceiptDetailsRepository.getTransferReceiptDetailsByClinicStockIdReceiptId(dto.getReceipt_id(), dto.getClinic_stock_id());
        ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinic_stock_id()).get();

        if (transferReceipt.getStatus() != TransferStatus.MOVING){
            return null;
        }
        for(TransferReceiptDetails entity:transferReceiptDetails){
            int quantityChange = dto.getNumber_product_transfer() - entity.getNumber_product_transfer();
            clinicStock.setQuantity(clinicStock.getQuantity() - quantityChange);
            clinicStockRepository.save(clinicStock);

            entity.setNumber_product_transfer(dto.getNumber_product_transfer());
            transferReceiptDetailsRepository.save(entity);
        }

        return dto;
    }

    @Override
    public boolean restoreTransferReceipt(Integer id){
        TransferReceipt transferReceipt = transferReceiptRepository.findById(id).get();

        if(transferReceipt.getStatus() != TransferStatus.DELETED){
            return false;
        }
        List<TransferReceiptDetailsQueryDto> transferReceiptDetailsQueryDtos = transferReceiptDetailsRepository.getAllTransferReceiptDetailByReceiptid(id);
        if (!transferReceiptDetailsQueryDtos.isEmpty()) {
            for (TransferReceiptDetailsQueryDto dto : transferReceiptDetailsQueryDtos) {
                ClinicStock clinicStock = clinicStockRepository.findById(dto.getClinic_stock_id()).get();
                clinicStock.setQuantity(clinicStock.getQuantity() - dto.getNumber_product_transfer());
                clinicStockRepository.save(clinicStock);
            }
        }
        transferReceipt.setStatus(TransferStatus.MOVING);
        transferReceiptRepository.save(transferReceipt);
        return true;
    }





    @Override
    public boolean insert(TransferReceiptDto dto){
        return false;
    }

    @Override
    public boolean update(Integer id, TransferReceiptDto transferReceiptDto){
        return  false;
    }

    @Override
    public boolean delete(Integer id) { return false;}

}
