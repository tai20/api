package com.sharework.health.service.impl;

import com.sharework.health.convert.ProductConvert;
import com.sharework.health.convert.WarehouseReceiptConvert;
import com.sharework.health.convert.WarehouseReceiptDetailConvert;
import com.sharework.health.dto.WarehouseReceiptDetailDto;
import com.sharework.health.dto.WarehouseReceiptDto;
import com.sharework.health.entity.*;
import com.sharework.health.repository.*;
import com.sharework.health.service.WarehouseReceiptService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional(rollbackOn = Exception.class)
public class WarehouseReceiptServiceImpl implements WarehouseReceiptService {

    private WarehouseReceiptRepository warehouseReceiptRepository;

    private WarehouseReceiptConvert warehouseReceiptConvert;

    private ClinicRepository clinicRepository;

    private SupplierRepository supplierRepository;

    private UserRepository userRepository;

    private ProductRepository productRepository;

    private WarehouseReceiptDetailRepository warehouseReceiptDetailRepository;

    private WarehouseReceiptDetailConvert warehouseReceiptDetailConvert;

    private ProductConvert productConvert;

    @Override
    public List<WarehouseReceiptDto> findAll() {
        List<WarehouseReceipt> warehouseReceipts = warehouseReceiptRepository.findAll();
        List<WarehouseReceiptDto> warehouseReceiptDtos = new ArrayList<>();
        for (WarehouseReceipt entity: warehouseReceipts
             ) {
            WarehouseReceiptDto dto = warehouseReceiptConvert.entityToDto(entity);
            warehouseReceiptDtos.add(dto);
        }
        return warehouseReceiptDtos;
    }

    @Override
    public WarehouseReceiptDto findById(Integer id) {
        WarehouseReceipt entity = warehouseReceiptRepository.findById(id).get();
        if (entity == null) {
            return null;
        }
        return warehouseReceiptConvert.entityToDto(entity);
    }

    @Override
    public boolean insert(WarehouseReceiptDto dto) {
        if (dto == null){
            return false;
        }

            Clinic clinic = clinicRepository.findById(dto.getClinicDto().getId()).get();
            Supplier supplier = supplierRepository.findById(dto.getSupplierDto().getId()).get();
            User user = userRepository.findById(dto.getUserDto().getId()).get();

            WarehouseReceipt entity = warehouseReceiptConvert.dtoToEntity(dto);
            entity.setId(null);
            if (clinic == null){
                return false;
            }
            entity.setClinic(clinic);
            if (supplier == null){
                return false;
            }
            entity.setSupplier(supplier);
            if (user == null){
                return false;
            }
            entity.setUser(user);
            entity.setStatus(Status.ACTIVE);
            warehouseReceiptRepository.save(entity);
            return true;

    }

    @Override
    public boolean update(Integer id, WarehouseReceiptDto dto) {
        WarehouseReceipt entity = warehouseReceiptRepository.findById(id).get();
        if (entity == null){
            return false;
        }

            Clinic clinic = clinicRepository.findById(dto.getClinicDto().getId()).get();
            Supplier supplier = supplierRepository.findById(dto.getSupplierDto().getId()).get();
            User user = userRepository.findById(dto.getUserDto().getId()).get();

            entity.setDateimport(dto.getDateimport());
            entity.setReceiptType(dto.getReceiptType());
            //entity.setTotalPayment(dto.getTotalPayment());

            if (clinic == null){
                return false;
            }
            entity.setClinic(clinic);
            if (supplier == null){
                return false;
            }
            entity.setSupplier(supplier);
            if (user == null){
                return false;
            }
            entity.setUser(user);
            warehouseReceiptRepository.save(entity);
            return true;

    }

    @Override
    public boolean delete(Integer id) {
        WarehouseReceipt entity = warehouseReceiptRepository.findById(id).get();
        if (entity == null){
            return false;
        }
            entity.setStatus(Status.DEACTIVE);
            warehouseReceiptRepository.save(entity);
            return true;

    }

    @Override
    public boolean addWarehouseReceiptDetail(Integer id, WarehouseReceiptDetailDto dto) {
        WarehouseReceipt entity = warehouseReceiptRepository.findById(id).get();
        Product product = productRepository.findById(dto.getProductDto().getId()).get();
        if (entity == null){
            return false;
        }
        if (entity.getReceiptType().equalsIgnoreCase("Nhập hàng")){
            WarehouseReceiptDetail warehouseReceiptDetail = new WarehouseReceiptDetail()
                    .setWarehouseReceipt(entity)
                    .setProduct(product)
                    .setNumberProductImport(dto.getNumberProductImport());
            warehouseReceiptDetailRepository.save(warehouseReceiptDetail);

            // Kho được nhập
            List<Product> products = productRepository.findAllByClinic(entity.getClinic().getId());
            for (Product product1: products
            ) {
                if (product1.getId() == product.getId()){
                    if (product1.getCapacity() <= 0) {
                        product1.setQuantity(product1.getQuantity() + dto.getNumberProductImport());
                        productRepository.save(product1);
                        return true;
                    }else {
                        int imp = dto.getNumberProductImport() * product1.getCapacity();
                        product1.setQuantity(product1.getQuantity() + dto.getNumberProductImport());
                        product1.setTotalCapacity(imp);
                        productRepository.save(product1);
                        return true;
                    }

                }
            }
        }
        if (entity.getReceiptType().equalsIgnoreCase("Chuyển kho")){
            WarehouseReceiptDetail warehouseReceiptDetail = new WarehouseReceiptDetail()
                    .setWarehouseReceipt(entity)
                    .setProduct(product)
                    .setNumberProductImport(dto.getNumberProductImport());
            warehouseReceiptDetailRepository.save(warehouseReceiptDetail);

            // Kho tổng
            Clinic clinic = clinicRepository.findByName(entity.getSupplier().getName());
            List<Product> productSuppliers = productRepository.findAllByClinic(clinic.getId());
            for (Product product1: productSuppliers
            ) {
                if (product1.getName() == product.getName()){
                    if (product1.getQuantity() - dto.getNumberProductImport() >=0){
                        product1.setQuantity(product1.getQuantity() - dto.getNumberProductImport());
                        productRepository.save(product1);
                    }else {
                        return false;
                    }
                }
            }
            // Kho được nhập
            List<Product> productClinics = productRepository.findAllByClinic(entity.getClinic().getId());
            int dem = 0;
            for (Product product1: productClinics
            ) {
                if (product1.getId() == product.getId()){
                    product1.setQuantity(product1.getQuantity() + dto.getNumberProductImport());
                    productRepository.save(product1);
                    dem ++;
                    break;
                }
            }
            if (dem == 0){
                Product productNew = new Product()
                        .setName(product.getName())
                        .setStatus(Status.ACTIVE)
                        .setQuantity(dto.getNumberProductImport())
                        .setWholesalePrice(product.getWholesalePrice())
                        .setUnit(product.getUnit())
                        .setPrice(product.getPrice())
                        .setProductCategory(product.getProductCategory());
                        //.setClinic(clinicRepository.findById(entity.getClinic().getId()).get());

                productRepository.save(productNew);
            }
            return true;
        }
        return false;
    }

    @Override
    public List<WarehouseReceiptDto> searchWarehouseReceiptByReceiptType(String receiptType) {
        List<WarehouseReceipt> warehouseReceipts = warehouseReceiptRepository.findAllByReceiptType(receiptType);
        List<WarehouseReceiptDto> warehouseReceiptDtos = new ArrayList<>();
        for (WarehouseReceipt entity: warehouseReceipts
             ) {
            WarehouseReceiptDto dto = warehouseReceiptConvert.entityToDto(entity);
            warehouseReceiptDtos.add(dto);
        }
        return warehouseReceiptDtos;
    }

	@Override
	public List<WarehouseReceiptDto> searchWarehouseReceiptByDateImport(LocalDate dateImport) {
		List<WarehouseReceipt> warehouseReceipts = warehouseReceiptRepository.findByDateimport(dateImport);
        List<WarehouseReceiptDto> warehouseReceiptDtos = new ArrayList<>();
        for (WarehouseReceipt entity: warehouseReceipts
             ) {
            WarehouseReceiptDto dto = warehouseReceiptConvert.entityToDto(entity);
            warehouseReceiptDtos.add(dto);
        }
        return warehouseReceiptDtos;
	}

	@Override
	public List<WarehouseReceiptDto> searchWarehouseReceiptByClinic(Integer clinicId) {
		List<WarehouseReceipt> warehouseReceipts = warehouseReceiptRepository.findByClinic(clinicId);
        List<WarehouseReceiptDto> warehouseReceiptDtos = new ArrayList<>();
        for (WarehouseReceipt entity: warehouseReceipts
             ) {
            WarehouseReceiptDto dto = warehouseReceiptConvert.entityToDto(entity);
            warehouseReceiptDtos.add(dto);
        }
        return warehouseReceiptDtos;
	}

    @Override
    public List<WarehouseReceiptDetailDto> findAllByWarehouseReceipt(Integer receiptId) {
        List<WarehouseReceiptDetail> warehouseReceiptDetails = warehouseReceiptDetailRepository.findAllByWarehouseReceipt(receiptId);
        List<WarehouseReceiptDetailDto> warehouseReceiptDetailDtos = new ArrayList<>();

        for (WarehouseReceiptDetail entity: warehouseReceiptDetails
             ) {
            WarehouseReceiptDetailDto dto = warehouseReceiptDetailConvert.entityToDto(entity);
            warehouseReceiptDetailDtos.add(dto);
        }
        return warehouseReceiptDetailDtos;
    }

    @Override
    public WarehouseReceiptDto findWarehouseReceiptAfterInsert() {
        WarehouseReceipt entity = warehouseReceiptRepository.findWarehouseReceiptAfterInsert();
        if (entity == null){
            return null;
        }
        return warehouseReceiptConvert.entityToDto(entity);
    }
}
